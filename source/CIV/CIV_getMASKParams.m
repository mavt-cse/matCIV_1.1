function params = CIV_getMASKParams(folder)

% SCRATCH2L_GETDEFAULTPARAMS:
%   gets default parameters for SCRATCH2L
%
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS:
%   looks for CIV_params.mat in current directory
%   if no CIV_params.mat is found, uses default params
%   
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS(DEFAULT):
%   with DEFAULT=1 forces use of default parameters

if nargin == 1
    try
        P = load(fullfile(folder,'CIV_paramsMASK.mat'));
    catch ME, % catch and ignore all errors
    end
    if exist('P','var') && isfield(P,'mod')
        params=P;
        return
    end
end

% MASK parameters
params = struct(...
    'mod',0, ...            % masking mode [doppler/intensity]
    ...
    'usePrev',1, ...        % use information of previous mask
    'd1',15,  ...           % distance 1
    'd2',30,  ...           % distance 2
    'd3',15,  ...           % distance 3
    ...
    'corrShift',1, ...      % do automatic shift correction
    'ssth',0.001, ...       % shift mask size threshold
    'sfth',0.3, ...         % shift fraction threshold
    'frac',0.5, ...          % fraction sth..
    ...                     % INTENSITY BASED PARAMS
    'I_th1',0, ...          % threshold 1
    'I_th2',0, ...          % threshold 2
    'I_op',0,  ...          % open
    'I_er',0,  ...          % errode
    'I_cl',0,  ...          % close
    'I_dil',0, ...          % dilate
    ...                     % DOPPLER BASED
    'D_th1',0.15, ...        % threshold 1
    'D_th2',0.1,  ...       % thres
    'D_cl1',10,  ...        % close 1
    'D_cl2',40,  ...        % close 2
    'D_a',30, ...           % areal
    'D_dil',3, ...
    'useFilter1',1, ...     % filter images before diff calculation
    'f1_name','log', ...   % filter name
    'f1_params','', ...     % filter params
    'f1','', ...            % filter 
    'useFilter2',0, ...     % filter diff image
    'f2_name','disk', ...   % filter name
    'f2_params','', ...     % filter params
    'f2','');               % filter

params.f1_params={20,4};
params.f2_params={1};
params.f1=fspecial('log',20,4);
params.f2=fspecial('disk',1);