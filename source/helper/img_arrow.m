function [ax ay]=img_arrow(angle,scale,x,y)
% returns outline path of arrow oriented according to "angle"
% origin at [x y]
% scale: scales the arrow

% ax: x-coordinates of arrow path
% ay: y-coordinates of arrow path

[th,r]=cart2pol([0  10  10 20 10 10 0  0],[-5 -5 -10  0 10  5 5 -5]);
[a1 a2]=pol2cart(th+angle,r*scale);
ax=a1+x;
ay=a2+y;