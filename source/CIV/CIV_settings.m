function varargout = CIV_settings(varargin)
% CIV_SETTINGS MATLAB code for CIV_settings.fig
%      CIV_SETTINGS, by itself, creates a new CIV_SETTINGS or raises the existing
%      singleton*.
%
%      H = CIV_SETTINGS returns the handle to a new CIV_SETTINGS or the handle to
%      the existing singleton*.
%
%      CIV_SETTINGS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CIV_SETTINGS.M with the given input arguments.
%
%      CIV_SETTINGS('Property','Value',...) creates a new CIV_SETTINGS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CIV_settings_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CIV_settings_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CIV_settings

% Last Modified by GUIDE v2.5 20-Aug-2012 12:15:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CIV_settings_OpeningFcn, ...
                   'gui_OutputFcn',  @CIV_settings_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CIV_settings is made visible.
function CIV_settings_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CIV_settings (see VARARGIN)

% Choose default command line output for CIV_settings
handles.output = hObject;
if nargin > 3
    handles.CBudParams=varargin{1};
    handles.CBstParams=varargin{2};
    handles.CBdlData=varargin{3};
    handles.CBhandles = varargin{4};
    handles.params = handles.CBhandles.params;
else
    handles.CBudParams=0;
    handles.CBstParams=0;
    handles.CBdlData=0;
    handles.CBhandles = 0;
    handles.params = CIV_getDefaultParams();
end

% assign tabs to panesl
handles.tabs=[handles.PB_general handles.UIP_general; ...
    handles.PB_piv handles.UIP_piv; ...
    handles.PB_mask handles.UIP_mask; ...
    handles.PB_statistics handles.UIP_statistics; ...
    handles.PB_tracking handles.UIP_tracking];
handles.curTab=1;
PB_tab_Callback(handles.PB_general, [], handles);

%set values
setfieldvalues(hObject, handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CIV_settings wait for user response (see UIRESUME)
uiwait(handles.CIV_settings);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Outputs from this function are returned to the command line.
function varargout = CIV_settings_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if ~ishandle(hObject),  % if figure was already deleted
    varargout{1} = {[]};
else
    varargout{1} = handles.output;
end
if ishandle(hObject)
    delete(handles.CIV_settings);
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function setfieldvalues(hObject, handles)
choice={'off','on'};
% general
set(handles.E_generalFolder,'String',handles.params.general.folder);
set(handles.E_dt,'String',num2str(handles.params.general.dt));
set(handles.S_dt,'Value',handles.params.general.dt);
set(handles.E_resolution,'String',num2str(handles.params.general.resolution));
set(handles.S_resolution,'Value',handles.params.general.resolution);
set(handles.E_scaleFactor,'String',num2str(handles.params.general.scaleFactor));
set(handles.S_scaleFactor,'Value',handles.params.general.scaleFactor);
% piv
set(handles.E_pivFolder,'String',handles.params.general.folderPIV);
set(handles.CB_lnf,'Value',handles.params.piv.lnf);
set(handles.S_lnfWSize,'Value',handles.params.piv.lnf_winsize);
set(handles.E_lnfWSize,'String',num2str(handles.params.piv.lnf_winsize));
set(handles.CB_crvlt,'Value',handles.params.piv.crvlt);
set(handles.E_crvltPctg,'String',handles.params.piv.crvlt_pctg);
set(handles.E_crvltLevels,'String',mat2strSelf(handles.params.piv.crvlt_levels));
set(handles.CB_crvltAbs,'Value',handles.params.piv.crvlt_abs);
if(handles.params.piv.lnf)
    %turn off
    handles.params.piv.crvlt=0;
    set(handles.E_crvltPctg,'Visible','off');
    set(handles.T_crvltPctg,'Visible','off');
    set(handles.T_crvltU1,'Visible','off');
    set(handles.E_crvltLevels,'Visible','off');
    set(handles.T_crvltLevels,'Visible','off');
    set(handles.CB_crvltAbs,'Visible','off');
    %turn on
    set(handles.T_lnfWSize','Visible','on');
    set(handles.S_lnfWSize','Visible','on');
    set(handles.E_lnfWSize','Visible','on');
    set(handles.T_lnfU1','Visible','on');
elseif(handles.params.piv.crvlt)
    %turn off
    set(handles.T_lnfWSize','Visible','off');
    set(handles.S_lnfWSize','Visible','off');
    set(handles.E_lnfWSize','Visible','off');
    set(handles.T_lnfU1','Visible','off');
    %turn on
    set(handles.E_crvltPctg,'Visible','on');
    set(handles.T_crvltPctg,'Visible','on');
    set(handles.T_crvltU1,'Visible','on');
    set(handles.E_crvltLevels,'Visible','on');
    set(handles.T_crvltLevels,'Visible','on');
    set(handles.CB_crvltAbs,'Visible','on');
end
c=handles.params.piv.crvlt+1;
set(handles.E_crvltPctg,'Enable',choice{c});
set(handles.T_crvltPctg,'Enable',choice{c});
set(handles.T_crvltU1,'Enable',choice{c});
set(handles.E_crvltLevels,'Enable',choice{c});
set(handles.T_crvltLevels,'Enable',choice{c});
set(handles.CB_crvltAbs,'Enable',choice{c});
    
    
choice={'off','on'};
c=handles.params.piv.lnf+1;
set(handles.T_lnfWSize,'Enable',choice{c});
set(handles.S_lnfWSize,'Enable',choice{c});
set(handles.E_lnfWSize,'Enable',choice{c});
set(handles.T_lnfU1','Enable',choice{c});


set(handles.E_wSize,'String',mat2str(handles.params.piv.winsize));
set(handles.S_overlap,'Value',handles.params.piv.overlap);
set(handles.E_overlap,'String',num2str(handles.params.piv.overlap));
switch handles.params.piv.method
    case 'single'
        set(handles.PM_method,'Value',1)
    case 'multi'
        set(handles.PM_method,'Value',2)
    case 'multin'
        set(handles.PM_method,'Value',3)
    case 'mqd'
        set(handles.PM_method,'Value',4)
    case 'norm'
        set(handles.PM_method,'Value',5)
end
set(handles.PM_units,'Value',handles.params.piv.units+1);

set(handles.CB_sft,'Value',handles.params.piv.sft);
set(handles.E_sftTh,'String',handles.params.piv.sftTh);

set(handles.CB_snrF,'Value',handles.params.piv.snrF);
set(handles.E_snrTh,'String',handles.params.piv.snrTh);
c=handles.params.piv.snrF+1;
set(handles.E_snrTh,'Enable',choice{c});

set(handles.CB_glbF,'Value',handles.params.piv.glbF);
set(handles.E_glbTh,'String',handles.params.piv.glbTh);
c=handles.params.piv.glbF+1;
set(handles.E_glbTh,'Enable',choice{c});

set(handles.CB_locF,'Value',handles.params.piv.locF);
set(handles.E_locTh,'String',handles.params.piv.locTh);
switch handles.params.piv.locKs
    case 'median'
        set(handles.PM_locKs,'Value',1)
    case 'mean'
        set(handles.PM_locKs,'Value',2)
end
switch handles.params.piv.locMtd
    case 'median'
        set(handles.PM_locMtd,'Value',1)
    case 'mean'
        set(handles.PM_locMtd,'Value',2)
end
c=handles.params.piv.locF+1;
set(handles.E_locTh,'Enable',choice{c});
set(handles.PM_locKs,'Enable',choice{c});
set(handles.PM_locMtd,'Enable',choice{c});

% mask
set(handles.E_maskFolder,'String',handles.params.general.folderMASK);
switch handles.params.mask.mod
    case 0
        set(handles.UIP_maskMethod,'SelectedObject',handles.RB_difference);
        set(handles.UIP_difference,'Visible','on');
        set(handles.UIP_intensity,'Visible','off');
        
    case 1
        set(handles.UIP_maskMethod,'SelectedObject',handles.RB_intensity);
        set(handles.UIP_difference,'Visible','off');
        set(handles.UIP_intensity,'Visible','on');
end
set(handles.S_D_th1,'Value',handles.params.mask.D_th1);
set(handles.E_D_th1,'String',handles.params.mask.D_th1);
set(handles.S_D_th2,'Value',handles.params.mask.D_th2);
set(handles.E_D_th2,'String',handles.params.mask.D_th2);
set(handles.S_D_cl1,'Value',handles.params.mask.D_cl1);
set(handles.E_D_cl1,'String',handles.params.mask.D_cl1);
set(handles.S_D_a,'Value',handles.params.mask.D_a);
set(handles.E_D_a,'String',handles.params.mask.D_a);
set(handles.S_D_cl2,'Value',handles.params.mask.D_cl2);
set(handles.E_D_cl2,'String',handles.params.mask.D_cl2);
set(handles.S_D_dil,'Value',handles.params.mask.D_dil);
set(handles.E_D_dil,'String',handles.params.mask.D_dil);
% use filter 1
set(handles.CB_F1,'Value',handles.params.mask.useFilter1);
set(handles.E_fName1,'String',handles.params.mask.f1_name);
set(handles.E_fParams1,'String',cell2str(handles.params.mask.f1_params));
c=handles.params.mask.useFilter1+1;
set(handles.T_fName1,'Enable',choice{c});
set(handles.E_fName1,'Enable',choice{c});
set(handles.T_fParams1,'Enable',choice{c});
set(handles.E_fParams1,'Enable',choice{c});
% use filter 2
set(handles.CB_F2,'Value',handles.params.mask.useFilter2);
set(handles.E_fName2,'String',handles.params.mask.f2_name);
set(handles.E_fParams2,'String',cell2str(handles.params.mask.f2_params));
c=handles.params.mask.useFilter2+1;
set(handles.T_fName2,'Enable',choice{c});
set(handles.E_fName2,'Enable',choice{c});
set(handles.T_fParams2,'Enable',choice{c});
set(handles.E_fParams2,'Enable',choice{c});
% use prev tab
set(handles.CB_prev,'Value',handles.params.mask.usePrev);
set(handles.S_d1,'Value',handles.params.mask.d1);
set(handles.E_d1,'String',num2str(handles.params.mask.d1));
set(handles.S_d2,'Value',handles.params.mask.d2);
set(handles.E_d2,'String',num2str(handles.params.mask.d2));
set(handles.S_d3,'Value',handles.params.mask.d3);
set(handles.E_d3,'String',num2str(handles.params.mask.d3));
c=handles.params.mask.usePrev+1;
set(handles.T_d1,'Enable',choice{c});
set(handles.S_d1,'Enable',choice{c});
set(handles.E_d1,'Enable',choice{c});
set(handles.T_d2,'Enable',choice{c});
set(handles.S_d2,'Enable',choice{c});
set(handles.E_d2,'Enable',choice{c});
set(handles.T_d3,'Enable',choice{c});
set(handles.S_d3,'Enable',choice{c});
set(handles.E_d3,'Enable',choice{c});
% shift tab
set(handles.CB_shift,'Value',handles.params.mask.corrShift);
set(handles.S_ssth,'Value',handles.params.mask.ssth);
set(handles.E_ssth,'String',num2str(handles.params.mask.ssth));
set(handles.S_sfth,'Value',handles.params.mask.sfth);
set(handles.E_sfth,'String',num2str(handles.params.mask.sfth));
set(handles.S_frac,'Value',handles.params.mask.frac);
set(handles.E_frac,'String',num2str(handles.params.mask.frac));
c=handles.params.mask.corrShift+1;
set(handles.T_ssth,'Enable',choice{c});
set(handles.S_ssth,'Enable',choice{c});
set(handles.E_ssth,'Enable',choice{c});
set(handles.T_sfth,'Enable',choice{c});
set(handles.S_sfth,'Enable',choice{c});
set(handles.E_sfth,'Enable',choice{c});
set(handles.T_frac,'Enable',choice{c});
set(handles.S_frac,'Enable',choice{c});
set(handles.E_frac,'Enable',choice{c});
% statistics
set(handles.E_sectors,'String',num2str(handles.params.stats.sectors));
set(handles.S_sectors,'Value',handles.params.stats.sectors);
c=handles.params.stats.confReg+1;
set(handles.CB_confReg,'Value',handles.params.stats.confReg);
set(handles.E_regExt,'String',num2str(handles.params.stats.regExt));
set(handles.E_regExt,'Enable',choice{c});
set(handles.T_regExt,'Enable',choice{c});
set(handles.T_regExtu,'Enable',choice{c});
c=handles.params.stats.space+1;
set(handles.CB_space,'Value',handles.params.stats.space);
set(handles.E_distBinSS,'String',num2str(handles.params.stats.distBinSS));
set(handles.E_distBinSS,'Enable',choice{c});
set(handles.T_distBinSS,'Enable',choice{c});
set(handles.T_distBinSSu,'Enable',choice{c});
set(handles.E_distBinOL,'String',handles.params.stats.distBinOL);
set(handles.E_distBinOL,'Enable',choice{c});
set(handles.T_distBinOL,'Enable',choice{c});
set(handles.E_corrDist,'String',handles.params.stats.corrDist);
set(handles.E_corrDist,'Enable',choice{c});
set(handles.T_corrDist,'Enable',choice{c});
set(handles.T_corrDistu,'Enable',choice{c});
set(handles.E_tempBinSS,'String',num2str(handles.params.stats.tempBinSS));
set(handles.E_tempBinOL,'String',handles.params.stats.tempBinOL);

guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [val, ok]=getEditValue(preval, E, min, max)
val=str2double(get(E,'String'));
ok=1;
if(isnan(val) || (val<min) || (val>max)),
    val=preval;
    ok=0;
    errordlg(sprintf('Enter value between %0.2f and %0.2f!', min, max));
end
    
    
%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [val, ok]=getEditValueOl(preval, E, min, max, ws)
val=str2double(get(E,'String'));
ok=1;
if(sum(sum(ws*val-round(ws*val)))~=0 || isnan(val) || (val<min) || (val>max)),
    val=preval;
    ok=0;
    errordlg(sprintf('Enter value between %0.2f and %0.2f and such that overlap*winsize = INT!', min, max));
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [val, ok]=getEditInt(preval, E, min, max)
val=round(str2double(get(E,'String')));
ok=1;
if(isnan(val) || (val<min) || (val>max)),
    val=preval;
    ok=0;
    errordlg(sprintf('Enter value between %0.2f and %0.2f!', min, max));
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% vector, integer, unique
function [val, ok]=getEditIUMat(preval, E, minV, maxV)
ok=1;
try
    val=round(eval(get(E,'String')));
catch
    ok=0;
end
srt=0;
if length(val)>1
    srt=min(diff(val))<1;
end

if(~ok || size(val,1)~=1 || isnan(sum(val)) || (min(val)<minV) || (max(val)>maxV) || srt),
    val=preval;
    ok=0;
    errordlg(sprintf('Enter vector with increasing integer values between %0.0f and %0.0f', minV, maxV));
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [val, ok]=getEditI2Mat(preval, E, minV, maxV)
ok=1;
try
    val=round(eval(get(E,'String')));
catch
    ok=0;
end

if(~ok || size(val,2)~=2 || isnan(sum(sum(val))) || (min(min(val))<minV) || (max(max(val))>maxV)),
    val=preval;
    ok=0;
    errordlg(sprintf('Enter 2*n Matrix with values between %0.2f and %0.2f, usually decreasing squares of size 128, 64, 32 or 16', minV, maxV));
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function str=mat2strSelf(mat)
if length(mat)==1
    str=sprintf('[%s]',mat2str(mat));
else
    str=mat2str(mat);
end


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| GUI VIEW STUFF |||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_general.
function PB_tab_Callback(hObject, eventdata, handles)
% hObject    handle to PB_general (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++++++++++
% - switch on visibility of the correct tab
% - highlight the current tab
% hObject is the tab that has been clicked
%+++++++++++++
% lable clicked tab
set(handles.tabs(handles.curTab,1),'Enable','on');
set(handles.tabs(handles.curTab,2),'Visible','off');
handles.curTab = find(handles.tabs == hObject);
set(handles.tabs(handles.curTab,1),'Enable','off');
set(handles.tabs(handles.curTab,2),'Visible','on');
% Update handles structure
guidata(hObject, handles);


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| BUTTON ROW |||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_update.
function PB_update_Callback(hObject, eventdata, handles)
% hObject    handle to PB_update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.CBhandles=handles.CBudParams(handles.CBhandles,handles.params);
figure(handles.CIV_settings);
guidata(hObject, handles);
%uiresume(handles.CIV_settings);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_store.
function PB_store_Callback(hObject, eventdata, handles)
% hObject    handle to PB_store (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.CBhandles=handles.CBudParams(handles.CBhandles,handles.params);
handles.CBstParams(handles.CBhandles);
figure(handles.CIV_settings);
guidata(hObject,handles);



%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_reset.
function PB_reset_Callback(hObject, eventdata, handles)
% hObject    handle to PB_rollback (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.params=CIV_getDefaultParams();
setfieldvalues(hObject, handles);
handles.CBhandles=handles.CBudParams(handles.CBhandles,handles.params);
figure(handles.CIV_settings);
guidata(hObject, handles);
%uiresume(handles.CIV_settings);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_rollback.
function PB_rollback_Callback(hObject, eventdata, handles)
% hObject    handle to PB_rollback (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ppath=fullfile(handles.CBhandles.batchInfo.folderpath,handles.params.general.folder);
handles.params=CIV_getDefaultParams(ppath);
setfieldvalues(hObject, handles);
handles.CBhandles=handles.CBudParams(handles.CBhandles,handles.params);
figure(handles.CIV_settings);
guidata(hObject, handles);
%uiresume(handles.CIV_settings);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_cancel.
function PB_cancel_Callback(hObject, eventdata, handles)
% hObject    handle to PB_rollback (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guidata(hObject, handles);
uiresume(handles.CIV_settings);


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| GENERAL PANEL ||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_generalFolder_Callback(hObject, eventdata, handles)
% hObject    handle to E_generalFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_generalFolder as text
%        str2double(get(hObject,'String')) returns contents of E_generalFolder as a double
s=get(hObject,'String');
pthGEN=fullfile(handles.CBhandles.batchInfo.folderpath,s);
if(exist(pthGEN,'file'))
    ans=questdlg('This Folder already exists.. do you want to load this data?','WARNING','ok','cancel','ok');
    handles.params=CIV_getDefaultParams(fullfile(handles.CBhandles.batchInfo.folderpath,s));
    handles.params.general.folder=s;
    setfieldvalues(hObject, handles);
else
    handles.params.general.folder=s;
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_generalFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_generalFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
%
% todo: if existing folder, ask load
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_dt_Callback(hObject, eventdata, handles)
% hObject    handle to S_dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.general.dt=get(hObject,'Value');
set(handles.E_dt,'String',num2str(handles.params.general.dt));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_dt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_dt_Callback(hObject, eventdata, handles)
% hObject    handle to E_dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_dt as text
%        str2double(get(hObject,'String')) returns contents of E_dt as a double
[handles.params.general.dt, ok] = getEditValue(handles.params.general.dt,hObject,1,90);
if ~ok
    set(hObject,'String',num2str(handles.params.general.dt));
else
    set(handles.S_dt,'Value',handles.params.general.dt);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_dt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_dt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_resolution_Callback(hObject, eventdata, handles)
% hObject    handle to S_resolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.general.resolution=get(hObject,'Value');
set(handles.E_resolution,'String',num2str(handles.params.general.resolution));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_resolution_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_resolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_resolution_Callback(hObject, eventdata, handles)
% hObject    handle to E_resolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_resolution as text
%        str2double(get(hObject,'String')) returns contents of E_resolution as a double
[handles.params.general.resolution, ok] = getEditValue(handles.params.general.resolution,hObject,0.01,10);
if ~ok
    set(hObject,'String',num2str(handles.params.general.resolution));
else
    set(handles.S_resolution,'Value',handles.params.general.resolution);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_resolution_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_resolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_scaleFactor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_scaleFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_scaleFactor_Callback(hObject, eventdata, handles)
% hObject    handle to E_scaleFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_scaleFactor as text
%        str2double(get(hObject,'String')) returns contents of E_scaleFactor as a double
ans=questdlg('Changing this setting will invalidate previously conducted data. All data will be deleted','WARNING','ok','cancel','ok');
if strcmp(ans,'ok')
    ans=handles.CBdlData(handles.CBhandles,handles.params);
end
if strcmp(ans,'cancel')
    set(hObject,'String',num2str(handles.params.general.scaleFactor));
    guidata(hObject,handles);
    return;
end
[handles.params.general.scaleFactor, ok] = getEditValue(handles.params.general.scaleFactor,hObject,0,1);
if ~ok
    set(hObject,'String',num2str(handles.params.general.scaleFactor));
else
    
    set(handles.S_scaleFactor,'Value',handles.params.general.scaleFactor);
end
guidata(hObject,handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_scaleFactor_Callback(hObject, eventdata, handles)
% hObject    handle to S_scaleFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
ans=questdlg('Changing this setting will invalidate previously conducted data. All data will be deleted','WARNING','ok','cancel','ok')
if strcmp(ans,'ok')
    ans=handles.CBdlData(handles.CBhandles,handles.params);
end
if strcmp(ans,'cancel')
    set(hObject,'Value',handles.params.general.scaleFactor);
    return;
end
handles.params.general.scaleFactor=get(hObject,'Value');
set(handles.E_scaleFactor,'String',num2str(handles.params.general.scaleFactor));
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_scaleFactor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_scaleFactor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| PIV PANEL ||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_pivFolder_Callback(hObject, eventdata, handles)
% hObject    handle to E_pivFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_pivFolder as text
%        str2double(get(hObject,'String')) returns contents of E_pivFolder as a double
pthGEN=fullfile(handles.CBhandles.batchInfo.folderpath,handles.params.general.folder);
s=get(hObject,'String');
pthPIV=fullfile(pthGEN,s);
if(exist(fullfile(pthPIV),'file'))
    ans=questdlg('This Folder already exists.. do you want to load this data?','WARNING','ok','cancel','ok');
    handles.params.piv=CIV_getPIVParams(pthPIV);
    handles.params.general.folderPIV=s;
    setfieldvalues(hObject, handles);
else
    handles.params.general.folderPIV=s;
end
guidata(hObject,handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_pivFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_pivFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_lnf.
function CB_crvlt_Callback(hObject, eventdata, handles)
% hObject    handle to CB_lnf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_lnf
% ++++++
% update enable lnf stuff
handles.params.piv.crvlt=get(hObject,'Value');
if handles.params.piv.lnf
    handles.params.piv.lnf=0;
end
setfieldvalues(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_crvltPctg_Callback(hObject, eventdata, handles)
% hObject    handle to E_crvltPctg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[handles.params.piv.crvlt_pctg ok] = getEditValue(handles.params.piv.crvlt_pctg,hObject,0,100);
if ~ok,
    set(hObject,'String',num2str(handles.params.piv.crvlt_pctg));
end
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_crvltPctg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_crvltPctg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_crvltLevels_Callback(hObject, eventdata, handles)
% hObject    handle to E_crvltLevels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++++
% returns a matrix
% make sure that overlap times min(winsize) is integer
%+++++++
[handles.params.piv.crvlt_levels ok] = getEditIUMat(handles.params.piv.crvlt_levels,hObject,1,10);
if ~ok,
    set(hObject,'String',mat2strSelf(handles.params.piv.crvlt_levels));
end
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_crvltLevels_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_crvltLevels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_crvltAbs.
function CB_crvltAbs_Callback(hObject, eventdata, handles)
% hObject    handle to CB_crvltAbs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.params.piv.crvlt_abs=get(hObject,'Value');
setfieldvalues(hObject, handles);











%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_lnf.
function CB_lnf_Callback(hObject, eventdata, handles)
% hObject    handle to CB_lnf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_lnf
% ++++++
% update enable lnf stuff
handles.params.piv.lnf=get(hObject,'Value');
if handles.params.piv.crvlt
    handles.params.piv.crvlt=0;
end
setfieldvalues(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_lnfWSize_Callback(hObject, eventdata, handles)
% hObject    handle to E_lnfWSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_lnfWSize as text
%        str2double(get(hObject,'String')) returns contents of E_lnfWSize as a double
%++++++++++++
% get value
% round to integer
% adjust slider
%++++++++++++
[handles.params.piv.lnf_winsize ok] = getEditInt(handles.params.piv.lnf_winsize,hObject,1,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.piv.lnf_winsize));
else
    set(handles.S_lnfWSize,'Value',handles.params.piv.lnf_winsize);
end
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_lnfWSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_lnfWSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_lnfWSize_Callback(hObject, eventdata, handles)
% hObject    handle to S_lnfWSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.piv.lnf_winsize=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.piv.lnf_winsize);
set(handles.E_lnfWSize,'String',num2str(handles.params.piv.lnf_winsize));
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_lnfWSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_lnfWSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_wSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_wSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_wSize_Callback(hObject, eventdata, handles)
% hObject    handle to E_wSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_wSize as text
%        str2double(get(hObject,'String')) returns contents of E_wSize as a double
%+++++++
% returns a matrix
% make sure that overlap times min(winsize) is integer
%+++++++
[handles.params.piv.winsize ok] = getEditI2Mat(handles.params.piv.winsize,hObject,2,256);
if ~ok,
    set(hObject,'String',mat2str(handles.params.piv.winsize));
end
%check overlap*window is integer
check=handles.params.piv.winsize*handles.params.piv.overlap;
if(sum(sum(check-round(check)))>0)
    % check this
    errordlg(sprintf('overlap * window has to be an integer value. overlap is reset to 0'));
    handles.params.piv.overlap=0;
    set(handles.S_overlap,'Value',handles.params.piv.overlap);
    set(handles.E_overlap,'String',num2str(handles.params.piv.overlap));
end
guidata(hObject, handles);



%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_wSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_wSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_overlap_Callback(hObject, eventdata, handles)
% hObject    handle to S_overlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
%+++++
% find the closest overlap value to slider position
% that satisfies overlap*winsize=INTEGER
%++++++


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_overlap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_overlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_overlap_Callback(hObject, eventdata, handles)
% hObject    handle to E_overlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_overlap as text
%        str2double(get(hObject,'String')) returns contents of E_overlap as a double
%@@@ check this
[handles.params.piv.overlap, ok] = getEditValueOl(handles.params.piv.overlap,hObject,0,1,handles.params.piv.winsize);
if ~ok
    set(hObject,'String',num2str(handles.params.piv.overlap));
else
    set(handles.S_overlap,'Value',handles.params.piv.overlap);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_overlap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_overlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on selection change in PM_method.
function PM_method_Callback(hObject, eventdata, handles)
% hObject    handle to PM_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns PM_method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_method
s=get(hObject,'String');
handles.params.piv.method=s{get(hObject,'Value')};
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function PM_method_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in PM_units.
function PM_units_Callback(hObject, eventdata, handles)
% hObject    handle to PM_units (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns PM_units contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_units
handles.params.piv.units=get(hObject,'Value')-1;
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function PM_units_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_units (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%||||||||||||||||||||||||||||||||||||||||||||||
% POST PROCESSING: SHIFTING AND FILTERING
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_sft.
function CB_sft_Callback(hObject, eventdata, handles)
% hObject    handle to CB_sft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% update enable shift stuff
handles.params.piv.sft=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_sftTh_Callback(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%++++++++++++
% get value
% round to integer
% adjust slider
%++++++++++++
[handles.params.piv.sftTh ok] = getEditValue(handles.params.piv.sftTh,hObject,-1,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.piv.sftTh));
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_sftTh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_sft.
function CB_snrF_Callback(hObject, eventdata, handles)
% hObject    handle to CB_sft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% update enable shift stuff
handles.params.piv.snrF=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_snrTh_Callback(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%++++++++++++
% get value
% round to integer
% adjust slider
%++++++++++++
[handles.params.piv.snrTh ok] = getEditValue(handles.params.piv.snrTh,hObject,0,100);
if ~ok,
    set(hObject,'String',num2str(handles.params.piv.snrTh));
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_snrTh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_sft.
function CB_glbF_Callback(hObject, eventdata, handles)
% hObject    handle to CB_sft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% update enable shift stuff
handles.params.piv.glbF=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_glbTh_Callback(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%++++++++++++
% get value
% round to integer
% adjust slider
%++++++++++++
[handles.params.piv.glbTh ok] = getEditValue(handles.params.piv.glbTh,hObject,0,100);
if ~ok,
    set(hObject,'String',num2str(handles.params.piv.glbTh));
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_glbTh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_sft.
function CB_locF_Callback(hObject, eventdata, handles)
% hObject    handle to CB_sft (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% update enable shift stuff
handles.params.piv.locF=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_locTh_Callback(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%++++++++++++
% get value
% round to integer
% adjust slider
%++++++++++++
[handles.params.piv.locTh ok] = getEditValue(handles.params.piv.locTh,hObject,0,100);
if ~ok,
    set(hObject,'String',num2str(handles.params.piv.locTh));
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_locTh_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_sftTh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on selection change in PM_method.
function PM_locKs_Callback(hObject, eventdata, handles)
% hObject    handle to PM_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns PM_method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_method
s=get(hObject,'String');
handles.params.piv.locKs=s{get(hObject,'Value')};
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function PM_locKs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on selection change in PM_method.
function PM_locMtd_Callback(hObject, eventdata, handles)
% hObject    handle to PM_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns PM_method contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PM_method
s=get(hObject,'String');
handles.params.piv.locMtd=s{get(hObject,'Value')};
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function PM_locMtd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PM_method (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| MASK PANEL |||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_maskFolder_Callback(hObject, eventdata, handles)
% hObject    handle to E_maskFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_maskFolder as text
%        str2double(get(hObject,'String')) returns contents of E_maskFolder as a double
pthGEN=fullfile(handles.CBhandles.batchInfo.folderpath,handles.params.general.folder);
s=get(hObject,'String');
pthMASK=fullfile(pthGEN,s);
if(exist(fullfile(pthMASK),'file'))
    ans=questdlg('This Folder already exists.. do you want to load this data?','WARNING','ok','cancel','ok');
    handles.params.mask=CIV_getMASKParams(pthMASK);
    handles.params.general.folderMASK=s;
    setfieldvalues(hObject, handles);
else
    handles.params.general.folderMASK=s;
end
guidata(hObject,handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_maskFolder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_maskFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function UIP_maskMethod_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to UIP_maskMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch get(eventdata.NewValue,'Tag') % Get Tag of selected object.
    case 'RB_difference'
        handles.params.mask.mod=0;
    case 'RB_intensity'
        handles.params.mask.mod=1;
    otherwise
        % do nothing
end
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_shift.
function CB_F1_Callback(hObject, eventdata, handles)
% hObject    handle to CB_shift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_shift
% ++++++
% update enable shift detection stuff
handles.params.mask.useFilter1=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_shift.
function CB_F2_Callback(hObject, eventdata, handles)
% hObject    handle to CB_shift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_shift
% ++++++
% update enable shift detection stuff
handles.params.mask.useFilter2=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_prev.
function CB_prev_Callback(hObject, eventdata, handles)
% hObject    handle to CB_prev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_prev
% ++++++
% update enable use Prev stuff
handles.params.mask.usePrev=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_shift.
function CB_shift_Callback(hObject, eventdata, handles)
% hObject    handle to CB_shift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_shift
% ++++++
% update enable shift detection stuff
handles.params.mask.corrShift=get(hObject,'Value');
setfieldvalues(hObject, handles);


%|||||||||||||||||||||||
%|||| METHOD TAB |||||||
%|||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_D_th1_Callback(hObject, eventdata, handles)
% hObject    handle to S_D_th1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.D_th1=get(hObject,'Value');
set(handles.E_D_th1,'String',num2str(handles.params.mask.D_th1));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_D_th1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_D_th1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_D_th1_Callback(hObject, eventdata, handles)
% hObject    handle to E_D_th1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_D_th1 as text
%        str2double(get(hObject,'String')) returns contents of E_D_th1 as a double
[handles.params.mask.D_th1, ok] = getEditValue(handles.params.mask.D_th1,hObject,0,1);
if ~ok
    set(hObject,'String',num2str(handles.params.mask.D_th1));
else
    set(handles.S_D_th1,'Value',handles.params.mask.D_th1);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_D_th1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_D_th1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_D_th2_Callback(hObject, eventdata, handles)
% hObject    handle to S_D_th2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.D_th2=get(hObject,'Value');
set(handles.E_D_th2,'String',num2str(handles.params.mask.D_th2));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_D_th2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_D_th2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_D_th2_Callback(hObject, eventdata, handles)
% hObject    handle to E_D_th2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_D_th2 as text
%        str2double(get(hObject,'String')) returns contents of E_D_th2 as a double
[handles.params.mask.D_th2, ok] = getEditValue(handles.params.mask.D_th2,hObject,0,1);
if ~ok
    set(hObject,'String',num2str(handles.params.mask.D_th2));
else
    set(handles.S_D_th2,'Value',handles.params.mask.D_th2);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_D_th2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_D_th2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_D_cl1_Callback(hObject, eventdata, handles)
% hObject    handle to S_D_cl1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.D_cl1=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.D_cl1);
set(handles.E_D_cl1,'String',num2str(handles.params.mask.D_cl1));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_D_cl1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_D_cl1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_D_cl1_Callback(hObject, eventdata, handles)
% hObject    handle to E_D_cl1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_D_cl1 as text
%        str2double(get(hObject,'String')) returns contents of E_D_cl1 as a double
[handles.params.mask.D_cl1 ok] = getEditInt(handles.params.mask.D_cl1,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.D_cl1));
else
    set(handles.S_D_cl1,'Value',handles.params.mask.D_cl1);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_D_cl1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_D_cl1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_D_a_Callback(hObject, eventdata, handles)
% hObject    handle to S_D_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.D_a=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.D_a);
set(handles.E_D_a,'String',num2str(handles.params.mask.D_a));
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function S_D_a_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_D_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function E_D_a_Callback(hObject, eventdata, handles)
% hObject    handle to E_D_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_D_a as text
%        str2double(get(hObject,'String')) returns contents of E_D_a as a double
[handles.params.mask.D_a ok] = getEditInt(handles.params.mask.D_a,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.D_a));
else
    set(handles.S_D_a,'Value',handles.params.mask.D_a);
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function E_D_a_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_D_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_D_cl2_Callback(hObject, eventdata, handles)
% hObject    handle to S_D_cl2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.D_cl2=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.D_cl2);
set(handles.E_D_cl2,'String',num2str(handles.params.mask.D_cl2));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_D_cl2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_D_cl2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_D_cl2_Callback(hObject, eventdata, handles)
% hObject    handle to E_D_cl2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_D_cl2 as text
%        str2double(get(hObject,'String')) returns contents of E_D_cl2 as a double
[handles.params.mask.D_cl2 ok] = getEditInt(handles.params.mask.D_cl2,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.D_cl2));
else
    set(handles.S_D_cl2,'Value',handles.params.mask.D_cl2);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_D_cl2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_D_cl2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_D_dil_Callback(hObject, eventdata, handles)
% hObject    handle to S_D_dil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.D_dil=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.D_dil);
set(handles.E_D_dil,'String',num2str(handles.params.mask.D_dil));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_D_dil_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_D_dil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_D_dil_Callback(hObject, eventdata, handles)
% hObject    handle to E_D_dil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_D_dil as text
%        str2double(get(hObject,'String')) returns contents of E_D_dil as a double
[handles.params.mask.D_dil ok] = getEditInt(handles.params.mask.D_dil,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.D_dil));
else
    set(handles.S_D_dil,'Value',handles.params.mask.D_dil);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_D_dil_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_D_dil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||| FILTER TAB |||||||
%|||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_fName1_Callback(hObject, eventdata, handles)
% hObject    handle to E_fName1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_fName1 as text
%        str2double(get(hObject,'String')) returns contents of E_fName1 as a double
%+++++
% change filter method
%  - check wether the filter works with current parameter set
%  - otherwise reset
%+++++
filt=get(hObject,'string');
try
    %see if filter name valid
    f=fspecial(filt);
    try
        %see if params valid
        switch length(handles.params.mask.f1_params);
            case 0
                f=fspecial(filt);
            case 1
                f=fspecial(filt,handles.params.mask.f1_params{1});
            case 2
                f=fspecial(filt,handles.params.mask.f1_params{1},...
                                handles.params.mask.f1_params{2});
            otherwise
                throw(exception);
        end
    catch
        handles.params.mask.f1_params={};
        set(handles.E_fParams1,'String',cell2str(handles.params.mask.f1_params));
    end
    %
catch
    errordlg('Invalid Filter Name');
    set(hObject,'String',handles.params.mask.f1_name);
    return
end
handles.params.mask.f1_name=filt;
handles.params.mask.f1=f;
guidata(hObject, handles);
    
                       
%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_fName1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_fName1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_fParams1_Callback(hObject, eventdata, handles)
% hObject    handle to E_fParams1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_fParams1 as text
%        str2double(get(hObject,'String')) returns contents of E_fParams1 as a double
try
    params=eval(get(hObject,'string'));
    switch length(params)
        case 0
            f=fspecial(handles.params.mask.f1_name);
        case 1
            f=fspecial(handles.params.mask.f1_name,params{1});
        case 2
            f=fspecial(handles.params.mask.f1_name,params{1},params{2});
        otherwise
            throw(exception);
    end
catch
    errordlg('Invalid parameters for Filter')
    set(hObject,'String',cell2str(handles.params.mask.f1_params));
    return
end
handles.params.mask.f1_params=params;
set(hObject,'String',cell2str(handles.params.mask.f1_params));
handles.params.mask.f1=f;
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_fParams1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_fParams1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_fName2_Callback(hObject, eventdata, handles)
% hObject    handle to E_fName2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_fName2 as text
%        str2double(get(hObject,'String')) returns contents of E_fName2 as a double
filt=get(hObject,'string');
try
    %see if filter name valid
    f=fspecial(filt);
    try
        %see if params valid
        switch length(handles.params.mask.f2_params);
            case 0
                f=fspecial(filt);
            case 1
                f=fspecial(filt,handles.params.mask.f2_params{1});
            case 2
                f=fspecial(filt,handles.params.mask.f2_params{1},...
                                handles.params.mask.f2_params{2});
            otherwise
                throw(exception);
        end
    catch
        handles.params.mask.f2_params={};
        set(handles.E_fParams2,'String',cell2str(handles.params.mask.f2_params));
    end
    %
catch
    errordlg('Invalid Filter Name');
    set(hObject,'String',handles.params.mask.f2_name);
    return
end
handles.params.mask.f2_name=filt;
handles.params.mask.f2=f;
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_fName2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_fName2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_fParams2_Callback(hObject, eventdata, handles)
% hObject    handle to E_fParams2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_fParams2 as text
%        str2double(get(hObject,'String')) returns contents of E_fParams2 as a double
try
    params=eval(get(hObject,'string'));
    switch length(params)
        case 0
            f=fspecial(handles.params.mask.f2_name);
        case 1
            f=fspecial(handles.params.mask.f2_name,params{1});
        case 2
            f=fspecial(handles.params.mask.f2_name,params{1},params{2});
        otherwise
            throw(exception);
    end
catch
    errordlg('Invalid parameters for Filter')
    set(hObject,'String',cell2str(handles.params.mask.f2_params));
    return
end
handles.params.mask.f2_params=params;
set(hObject,'String',cell2str(handles.params.mask.f2_params));
handles.params.mask.f2=f;
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_fParams2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_fParams2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||| PREVIOUS TAB |||||
%|||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_d1_Callback(hObject, eventdata, handles)
% hObject    handle to S_d1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.d1=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.d1);
set(handles.E_d1,'String',num2str(handles.params.mask.d1));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_d1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_d1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_d1_Callback(hObject, eventdata, handles)
% hObject    handle to E_d1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_d1 as text
%        str2double(get(hObject,'String')) returns contents of E_d1 as a double
[handles.params.mask.d1 ok] = getEditInt(handles.params.mask.d1,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.d1));
else
    set(handles.S_d1,'Value',handles.params.mask.d1);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_d1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_d1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_d2_Callback(hObject, eventdata, handles)
% hObject    handle to S_d2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.d2=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.d2);
set(handles.E_d2,'String',num2str(handles.params.mask.d2));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_d2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_d2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_d2_Callback(hObject, eventdata, handles)
% hObject    handle to E_d2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_d2 as text
%        str2double(get(hObject,'String')) returns contents of E_d2 as a double
[handles.params.mask.d2 ok] = getEditInt(handles.params.mask.d2,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.d2));
else
    set(handles.S_d2,'Value',handles.params.mask.d2);
end
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_d2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_d2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_d3_Callback(hObject, eventdata, handles)
% hObject    handle to S_d3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.d3=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.mask.d3);
set(handles.E_d3,'String',num2str(handles.params.mask.d3));
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_d3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_d3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_d3_Callback(hObject, eventdata, handles)
% hObject    handle to E_d3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_d3 as text
%        str2double(get(hObject,'String')) returns contents of E_d3 as a double
[handles.params.mask.d3 ok] = getEditInt(handles.params.mask.d3,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.mask.d3));
else
    set(handles.S_d3,'Value',handles.params.mask.d3);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_d3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_d3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||| SHIFT TAB ||||||||
%|||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_ssth_Callback(hObject, eventdata, handles)
% hObject    handle to S_ssth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.ssth=get(hObject,'Value');
set(handles.E_ssth,'String',num2str(handles.params.mask.ssth));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_ssth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_ssth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_ssth_Callback(hObject, eventdata, handles)
% hObject    handle to E_ssth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_ssth as text
%        str2double(get(hObject,'String')) returns contents of E_ssth as a double
[handles.params.mask.ssth, ok] = getEditValue(handles.params.mask.ssth,hObject,0,1);
if ~ok
    set(hObject,'String',num2str(handles.params.mask.ssth));
else
    set(handles.S_ssth,'Value',handles.params.mask.ssth);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_ssth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_ssth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_sfth_Callback(hObject, eventdata, handles)
% hObject    handle to S_sfth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.sfth=get(hObject,'Value');
set(handles.E_sfth,'String',num2str(handles.params.mask.sfth));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_sfth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_sfth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_sfth_Callback(hObject, eventdata, handles)
% hObject    handle to E_sfth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_sfth as text
%        str2double(get(hObject,'String')) returns contents of E_sfth as a double
[handles.params.mask.sfth, ok] = getEditValue(handles.params.mask.sfth,hObject,0,1);
if ~ok
    set(hObject,'String',num2str(handles.params.mask.sfth));
else
    set(handles.S_sfth,'Value',handles.params.mask.sfth);
end
guidata(hObject,handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_sfth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_sfth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_frac_Callback(hObject, eventdata, handles)
% hObject    handle to S_frac (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.mask.frac=get(hObject,'Value');
set(handles.E_frac,'String',num2str(handles.params.mask.frac));
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_frac_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_frac (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_frac_Callback(hObject, eventdata, handles)
% hObject    handle to E_frac (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_frac as text
%        str2double(get(hObject,'String')) returns contents of E_frac as a double
[handles.params.mask.frac, ok] = getEditValue(handles.params.mask.frac,hObject,0,1);
if ~ok
    set(hObject,'String',num2str(handles.params.mask.frac));
else
    set(handles.E_frac,'Value',handles.params.mask.frac);
end
guidata(hObject,handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_frac_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_frac (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| STATISTICS PANEL |||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_sectors_Callback(hObject, eventdata, handles)
% hObject    handle to S_sectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
handles.params.stats.sectors=round(get(hObject,'Value'));
set(hObject,'Value',handles.params.stats.sectors);
set(handles.E_sectors,'String',num2str(handles.params.stats.sectors));
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_sectors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_sectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_sectors_Callback(hObject, eventdata, handles)
% hObject    handle to E_sectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_sectors as text
%        str2double(get(hObject,'String')) returns contents of E_sectors as a double
[handles.params.stats.sectors ok] = getEditInt(handles.params.stats.sectors,hObject,4,64);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.sectors));
else
    set(handles.S_sectors,'Value',handles.params.stats.sectors);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_sectors_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_sectors (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_confReg.
function CB_confReg_Callback(hObject, eventdata, handles)
% hObject    handle to CB_confReg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_confReg
handles.params.stats.confReg=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_regExt_Callback(hObject, eventdata, handles)
% hObject    handle to E_regExt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_regExt as text
%        str2double(get(hObject,'String')) returns contents of E_regExt as a double
[handles.params.stats.regExt ok] = getEditValue(handles.params.stats.regExt,hObject,0,5000);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.regExt));
else
    set(handles.E_distBinOL,'Value',handles.params.stats.regExt);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_regExt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_regExt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_space.
function CB_space_Callback(hObject, eventdata, handles)
% hObject    handle to CB_space (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_space
handles.params.stats.space=get(hObject,'Value');
setfieldvalues(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_distBinSS_Callback(hObject, eventdata, handles)
% hObject    handle to E_distBinSS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_distBinSS as text
%        str2double(get(hObject,'String')) returns contents of E_distBinSS as a double
[handles.params.stats.distBinSS ok] = getEditValue(handles.params.stats.distBinSS,hObject,0,200);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.distBinSS));
else
    set(handles.E_distBinSS,'Value',handles.params.stats.distBinSS);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_distBinSS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_distBinSS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_distBinOL_Callback(hObject, eventdata, handles)
% hObject    handle to E_distBinOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_distBinOL as text
%        str2double(get(hObject,'String')) returns contents of E_distBinOL as a double
[handles.params.stats.distBinOL ok] = getEditValue(handles.params.stats.distBinOL,hObject,0,1);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.distBinOL));
else
    set(handles.E_distBinOL,'Value',handles.params.stats.distBinOL);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_distBinOL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_distBinOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_corrDist_Callback(hObject, eventdata, handles)
% hObject    handle to E_corrDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_corrDist as text
%        str2double(get(hObject,'String')) returns contents of E_corrDist as a double
[handles.params.stats.corrDist ok] = getEditValue(handles.params.stats.corrDist,hObject,0,500);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.corrDist));
else
    set(handles.E_corrDist,'Value',handles.params.stats.corrDist);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_corrDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_corrDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_tempBinSS_Callback(hObject, eventdata, handles)
% hObject    handle to E_tempBinSS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_tempBinSS as text
%        str2double(get(hObject,'String')) returns contents of E_tempBinSS as a double
[handles.params.stats.tempBinSS ok] = getEditValue(handles.params.stats.tempBinSS,hObject,0,20);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.tempBinSS));
else
    set(handles.E_tempBinSS,'Value',handles.params.stats.tempBinSS);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_tempBinSS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_tempBinSS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_tempBinOL_Callback(hObject, eventdata, handles)
% hObject    handle to E_tempBinOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_tempBinOL as text
%        str2double(get(hObject,'String')) returns contents of E_tempBinOL as a double
[handles.params.stats.tempBinOL ok] = getEditValue(handles.params.stats.tempBinOL,hObject,0,1);
if ~ok,
    set(hObject,'String',num2str(handles.params.stats.tempBinOL));
else
    set(handles.E_tempBinOL,'Value',handles.params.stats.tempBinOL);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_tempBinOL_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_tempBinOL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


