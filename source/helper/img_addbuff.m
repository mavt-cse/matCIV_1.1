function res=img_addbuff(img,bS,how)
% add a buffer region around the image
% to prevent periodic contributions from curvelet transform
%
% img: image
% bufferSize: size of buffer in pixel
% how: 1: fill with zeros
%      2: fill with max value
%      3: mirrors
%      4: fill with gray
%      5: fill with boundary value

s=size(img);
res=ones(s+2*bS,class(img));
s2=size(res);
switch how
    case 1
        res=res*0;
        res((bS+1):(bS+s(1)),(bS+1:bS+s(2)))=img;
    case 2
        res = res*max(max(img));
        res((bS+1):(bS+s(1)),(bS+1:bS+s(2)))=img;
    case 3
        res=res*0;
        res((bS+1):(bS+s(1)),(bS+1:bS+s(2)))=img;
        
        res(1:bS,(bS+1:bS+s(2)))=flipud(img(1:bS,:));            %north
        res((s2(1)-bS+1):s2(1),(bS+1:bS+s(2)))=flipud(img((s(1)-bS+1:s(1)),:)); %south

        res(:,1:bS)=fliplr(res(:,(bS+1):2*bS));                         %west
        res(:,s2(2)-bS+1:s2(2))=fliplr(res(:,(s2(2)-2*bS+1):s2(2)-bS)); %east
    case 4
        res=res*0.2;
        res((bS+1):(bS+s(1)),(bS+1:bS+s(2)))=img;
        
    otherwise
        disp('no method specified, fill with 0')
        res((bS+1):(bS+s(1)),(bS+1:bS+s(2)))=img;
end
        
        
        





        