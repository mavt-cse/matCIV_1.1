function params = CIV_getDefaultParams(folder)

% SCRATCH2L_GETDEFAULTPARAMS:
%   gets default parameters for SCRATCH2L
%
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS:
%   looks for CIV_params.mat in current directory
%   if no CIV_params.mat is found, uses default params
%   
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS(DEFAULT):
%   with DEFAULT=1 forces use of default parameters

if nargin == 1
    try
        P = load(fullfile(folder,'CIV_params.mat'));
    catch ME, % catch and ignore all errors
    end
    if exist('P','var') && isfield(P,'dt')
        general=P;
        general.folderBI='BI';
        piv=CIV_getPIVParams(fullfile(folder,general.folderPIV));
        mask=CIV_getMASKParams(fullfile(folder,general.folderMASK));
        stats=CIV_getSTATSParams(fullfile(folder));
        
        % STAT parameters
        % @@@attention, this overwrites what was stored...
        % just here as its been added later
%         stats = struct(...
%             'sectors',32, ...       % angular bins
%             'space',1, ...          % do spatial analysis
%             'distBinSS',80, ...     % distance bin size
%             'distBinOL',0.75, ...   % distance bin overlap
%             'temp',1, ...           % do temporal analysis
%             'tempBinSS',6, ...      % temporal bin size
%             'tempBinOL',0.5, ...    % temporal bin overlap
%             'confReg',1, ...        % confine region where statistics are taken
%             'regExt',300, ...       % region distance to front
%             'stat',1);

        % all in one struct
        params=struct('general',general,... % general params
            'piv',piv, ...                  % piv params
            'mask',mask, ...                % masking params
            'stats',stats);                % statistical analysis params    
       return
    end
end

% set default values
% General parameters
general = struct(...
    'folder','DATA', ...    % folder containing data
    'folderPIV','PIV',...   % folder containing PIV data
    'folderMASK','MASK',... % folder containing MASK data
    'folderBI','BI',...     % folder containing BATCH Information data
    'dt',15, ...            % frame rate [min]
    'resolution',0.323, ... % pixel resolution [um/pixels]
    'scaleFactor',0.3, ...  % resizing factor
    'pad',30);              % padding size

% PIV parameters
piv=CIV_getPIVParams();

% MASK parameters
mask=CIV_getMASKParams();

% STAT parameters
stats=CIV_getSTATSParams();


% stats = struct(...
%     'sectors',32, ...       % angular bins
%     'space',1, ...          % do spatial analysis
%     'distBinSS',80, ...     % distance bin size
%     'distBinOL',0.75, ...   % distance bin overlap
%     'temp',1, ...           % do temporal analysis
%     'tempBinSS',6, ...      % temporal bin size
%     'tempBinOL',0.5, ...    % temporal bin overlap
%     'confReg',1, ...        % confine region where statistics are taken
%     'regExt',300, ...       % region distance to front
%     'stat',1);


% all in one struct
params=struct('general',general,... % general params
    'piv',piv, ...                  % piv params
    'mask',mask, ...                % masking params
    'stats',stats);                % statistical analysis params             