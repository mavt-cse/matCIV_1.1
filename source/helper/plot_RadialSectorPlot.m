function res=plot_RadialSectorPlot(sectors,values,color,width,mx,ol)
%plots consecutive angular sectors in polar plot
%  sectors = sector start, secttor divisions, last sector end
%  values = value vector length = length(sectors)-1
%  color = 'r' as in 'LineColor'..
%  width = as in 'LineWidth'
%  mx = maximum of polar plot to scale the thing
%  res = function handel: res=pp(..)
%  ol : outline: if 1, only outline is plotted

if(nargin==5)
    ol=0;
end


%sectors=[0,pi/4,2*pi/4,3*pi/4,2*pi];
%values=[0.8,0.8,0.8,0.8];

%mx=1;
substps=20;

if(length(values)~=length(sectors)-1)
    disp('wrong')
end

%loc=zeros(1,length(sectors)*substps+2*length(sectors)+1,1);
%val=zeros(1,length(sectors)*substps+2*length(sectors)+1,1);
ls=length(sectors)-1;
loc=zeros(1,ls*substps+2*ls+1,1);
val=zeros(1,ls*substps+2*ls+1,1);


cur_loc=sectors(1);
cur_i=0;
for s=1:length(sectors)-1
    step=(sectors(s+1)-sectors(s))/substps;
    %0
    if(~ol)
        cur_i=cur_i+1;
        loc(cur_i)=cur_loc;
        val(cur_i)=0;
    else
        cur_i=cur_i+1;
        loc(cur_i)=cur_loc;
        val(cur_i)=values(s);
    end
    %up
    cur_i=cur_i+1;
    loc(cur_i)=cur_loc;
    val(cur_i)=values(s);
    
    for ss=1:substps;
        cur_i=cur_i+1;
        cur_loc=cur_loc+step;
        loc(cur_i)=cur_loc;
        val(cur_i)=values(s);
    end
end
if(ol)
    val(end)=values(1);
end
pp(loc(:),val(:),'MaxValue',mx,'CentreValue',0,'LineColor',color,'LineWidth',width);
