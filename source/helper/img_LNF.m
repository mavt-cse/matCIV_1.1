function [res,M,AM,AS]=img_LNF(A,ws,which)
tic
switch(which)
    case 1
        [res,M,AM,AS]=img_LNF1(A,ws);
    case 2
        [res,M,AM,AS]=img_LNF2(A,ws);
    case 3
        [res,M,AM,AS]=img_LNF3(A,ws);
end
disp('*** img_NLF ***');
toc
%%
function [res,M,AM,AS]=img_LNF1(A,ws)
M=localMean(A,ws);
AM=A-M;
AS=(localMean((AM).^2,ws)).^(1/2);
res=img_rescale(AM./AS);

%%
function [res,M,AM,AS]=img_LNF2(A,ws)
M=medfilt2(A,[ws,ws],'symmetric');
AM=A-M;
%AS=(medfilt2((AM).^2,[ws,ws],'symmetric')).^(1/2);
hood=getnhood(strel('disk', floor(ws/2)));
AS=stdfilt(A,hood);
ind=find(AS==0);
res=img_rescale(AM./AS);
res(ind)=0;

%%
function [res,M,AM,AS]=img_LNF3(A,ws)
hood=getnhood(strel('disk',floor(ws/2)));
M=imfilter(A,hood/sum(hood(:)),'symmetric');
AM=A-M;
AS=stdfilt(A,hood);
ind=find(AS==0);
res=img_rescale(AM./AS);
%res=AM./AS;
res(ind)=0;

%%
function res=localMean(A,ws)
whs=floor(ws/2);
res=zeros(size(A));
for i=1:size(A,1)
    %determi window location in 1st Dim
    if(i>whs)
        if(i<size(A,1)-whs)
            spani=(i-whs):(i+whs);
        else
            spani=(size(A,1)-2*whs-1):(size(A,1));
        end
    else
        spani=1:(2*whs+1);
    end
    for j=1:size(A,2)
        %determi window location in 1st Dim
        if(j>whs)
            if(j<size(A,2)-whs)
                spanj=(j-whs):(j+whs);
            else
                spanj=(size(A,2)-2*whs-1):(size(A,2));
            end
        else
            spanj=1:(2*whs+1);
        end
        % sum up window contributions
        for fi=spani
            for fj=spanj
                res(i,j)=res(i,j)+A(fi,fj);
            end
        end
    end
end

res=res/(whs*2+1)^2;