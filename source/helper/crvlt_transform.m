function C=crvlt_transform(I,bufferSize)

im2C=img_addbuff(I,bufferSize,3);
C=fdct_wrapping(im2C,1);     
