function R=img_angleRGB(A,M,R)

if(nargin==1)
    M=ones(size(A));
end
if(nargin==3)
    M=M.*R;
end
% RGB=zeros(size(A,1),size(A,2),3);
% RGB(:,:,1)=(cos(A)*0.5+0.5).*M;
% RGB(:,:,2)=(cos(A+2*pi/3)*0.5+0.5).*M;
% RGB(:,:,3)=(cos(A+4*pi/3)*0.5+0.5).*M;

cr6=pi/3;
cr3=2*pi/3;
cr2=pi;
P=20;
PF1=cr3-cr6/P;
PF2=cr6-cr6/P;
crP=2*pi/P;

RGB(:,:,1)=min(max((PF1-abs(A))/PF2,0),1).*M;
B=A-cr3;
ind=find(B<-pi);
B(ind)=B(ind)+2*pi;
RGB(:,:,2)=min(max((PF1-abs(B))/PF2,0),1).*M;
B=A+cr3;
ind=find(B>pi);
B(ind)=B(ind)-2*pi;
RGB(:,:,3)=min(max((PF1-abs(B))/PF2,0),1).*M;

R=RGB;