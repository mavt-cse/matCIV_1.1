function res = CIV_storeData(handles)

% SCRATCH2L_STOREDATA
%   stores data for current frame
%
%   HANDLES: SCRATCH2L gui handles
%   stores only data, no parameters
%   stores only for checked boxes, eg. PIV, Mask, ...




%assemble file name
generalData=sprintf2(handles.batchInfo.pattern,handles.curFrame,'-GEN','.mat');
pivData=sprintf2(handles.batchInfo.pattern,handles.curFrame,'-PIV','.mat');
maskData=sprintf2(handles.batchInfo.pattern,handles.curFrame,'-MASK','.mat');
trackData=sprintf2(handles.batchInfo.pattern,handles.curFrame,'-TRACK','.mat');

% data paths
pthGEN=fullfile(handles.batchInfo.folderpath,handles.params.general.folder);
pthPIV=fullfile(pthGEN,handles.params.general.folderPIV);
pthMASK=fullfile(pthGEN,handles.params.general.folderMASK);

% store general data
%check for data path
if ~exist(pthGEN,'file')
    mkdir(pthGEN);
end
dg=handles.data.general;
save(fullfile(pthGEN,generalData),'-struct','dg');

% store piv data
storePIV=0;
try
    storePIV=(get(handles.CB_piv,'Value') && handles.data.piv.done )||...
        (get(handles.CB_shift,'Value') && handles.data.piv.shiftDone);
catch
    storePIV=(handles.CBV_piv && handles.data.piv.done )||...
        (handles.CBV_shift && handles.data.piv.shiftDone);
end

if(storePIV)
    if ~exist(pthPIV,'file')
        mkdir(pthPIV);
    end
    dp = handles.data.piv;
    save(fullfile(pthPIV,pivData),'-struct','dp');
end

% store mask data
storeMASK=0;
try
    storeMASK=get(handles.CB_mask,'Value') && (handles.data.mask.done || handles.data.mask.initMaskSet);
catch
    storeMASK=handles.CBV_mask && (handles.data.mask.done || handles.data.mask.initMaskSet);
end

if(storeMASK)
    if ~exist(pthMASK,'file')
        mkdir(pthMASK);
    end
    dm = handles.data.mask;
    save(fullfile(pthMASK,maskData),'-struct','dm');
end

res=1;