function params = CIV_getPIVParams(folder)

% SCRATCH2L_GETDEFAULTPARAMS:
%   gets default parameters for SCRATCH2L
%
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS:
%   looks for CIV_params.mat in current directory
%   if no CIV_params.mat is found, uses default params
%   
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS(DEFAULT):
%   with DEFAULT=1 forces use of default parameters

if nargin == 1
    try
        P = load(fullfile(folder,'CIV_paramsPIV.mat'));
    catch ME, % catch and ignore all errors
    end
    if exist('P','var') && isfield(P,'lnf')
        if ~isfield(P,'crvlt')
            P.crvlt=0;
            P.crvlt_pctg=1;
            P.crvlt_levels=[1 2 3 4 5 6 7];
            P.crvlt_abs=0;
        end
        params=P;
        
        return
    end
end

% PIV parameters
params = struct(...
    'lnf',1, ...            % do local normalization filter
    'crvlt',0, ...          % do curvelet filter
    'units',1, ...          % 0: um/min 1: pix/frame
    'lnf_winsize',70, ...   % local normalization filter window size
    'crvlt_pctg',100, ...     % percentage of curvelets to keep
    'crvlt_levels',[1 2 3 4 5 6 7], ... %curvelet levels to concider
    'crvlt_abs',0, ...      % take absolute or real part of transform
    'winsize',[ 128 128; 64 64; 32 32 ], ...   % piv window size [matPiv]
    'overlap',0.75, ...      % window overlap ratio [matPiv]
    'method','multin',...   % piv method [matPiv]
    'comap',[],...          % coordinate map [matPiv]
    'sft',1,...       % shift Correction
    'sftTh',2.5,...      % shift Detection threshold  
    'snrF',0,...            % signal-to-noise filtering
    'snrTh',1.3,...       % signal-to-noise threshold
    'glbF',1,...            % global Filter
    'glbTh',4,...         % global Filter threshold
    'locF',1,...            % local Filter
    'locTh',2.5,...       % local Filter threshold
    'locMtd','median',...  % local Filter method
    'locKs',3);            % local Filter kernel size