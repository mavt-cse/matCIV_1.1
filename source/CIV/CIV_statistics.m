function [stats ok] =CIV_statistics(hObject, eventdata, h,statStart,statStop)

% SCRATCH2L_STATISTICS
%   accumulate statistics for statStart to statEnd
%
%   Assume 1 or 2 initial ROIs
%   - accumulate statistics for these
%   - when they join, continue to gathers statistics for both
%       1,2: left(top)/right(bottom)
%       3: all
%
%  normalize orientation against scratch orientation
%   - assume scratch oreintation is 0;
%
%   consider spatial / temporal bining

try
    C.expFig=1;
    h1=figure;
    axis;
    export_fig(['tst','.pdf']);
    system('rm tst.pdf');
    close(h1);
catch
    C.expFig=0;
end

C.GS=5;     % global statistic quantities: S,O,OAbs,MINS,MAXS
C.CONF=0;   % confluent mask

%statistic settings
P=h.params.stats;

%create statistics folder
pthGEN=fullfile(h.batchInfo.folderpath,h.params.general.folder);
pthPIV=fullfile(pthGEN,h.params.general.folderPIV);
pthMASK=fullfile(pthGEN,h.params.general.folderMASK);
pthSTATS=fullfile(pthGEN,strcat('STAT-',...
    h.params.general.folderPIV,'-',...
    h.params.general.folderMASK));
if(P.confReg)
    pthSTATS=sprintf('%s_CONF%d',pthSTATS,P.regExt);
end
if ~exist(pthSTATS,'file')
    mkdir(pthSTATS);
end
if ~exist(fullfile(pthSTATS,'data'),'file')
    mkdir(fullfile(pthSTATS,'data'));
    mkdir(fullfile(pthSTATS,'images'));
    mkdir(fullfile(pthSTATS,'figures'));
end

%store statistics parameters to stats folder
save(fullfile(pthSTATS,'data','CIV_paramsSTATS.mat'),'-struct','P');


%++++++++++++++++++
% INIT VARIABLES
%++++++++++++++++++
%scaling Factor v=[pixels/min]
fac=1.0;
fac2=1.0;
fac3=1.0;
if(~h.params.piv.units==0)
    fac=h.params.general.resolution/h.params.general.scaleFactor/h.params.general.dt;
    fac2=h.params.general.resolution/h.params.general.scaleFactor;
    fac3=min(h.params.piv.winsize(:))*(1-h.params.piv.overlap)*...
        h.params.general.resolution/h.params.general.scaleFactor;
end
%init sectors
secSize=2*pi/P.sectors;
sectors=0:secSize:2*pi;
ok=0;

%quadrants in counter clockwise order
isec1=1:P.sectors/4;
isec2=P.sectors/4+1:2*P.sectors/4;
isec3=2*P.sectors/4+1:3*P.sectors/4;
isec4=3*P.sectors/4+1:P.sectors;
secRad=sectors(1:end-1)+secSize/2;

%++++++++++++++++++
% ALLOCATE DATA
%++++++++++++++++++
%init arrays
%frame averages
ALL=NaN(1,C.GS);
ALLt=ALL;
ALLF=NaN(statStop,2,C.GS);
AVD=NaN(1,P.sectors);
AVDt=AVD;
AVDF=NaN(statStop,2,P.sectors);
NSEW=NaN(1,4);
NSEWt=NSEW;
NSEWF=NaN(statStop,2,4);
DISTF=NaN(statStop,1);
EXF=ones(statStop,1);
SHIFTF=NaN(statStop,3);




%++++++++++++++++++
% START
%++++++++++++++++++
% init first frame
curData=CIV_loadData(statStart,h.batchInfo,h.params);
if ~checkData(curData,statStart)
    return;
end
%++++++++++++++++++
% find initial mask
%++++++++++++++++++
M=[];
if(curData.mask.initMaskSet)
    M=curData.mask.IM;
else
    M=curData.mask.M;
end
label=bwlabel(M);
l=unique(label);
if(length(l)==1)
    if(l(1)==1)
        %confluent mask
        C.CONF=1;
        M1=M;
        M2=M;
        width=size(M,1);
        stats.o1alpha=0;
        stats.o1rho=0;
        stats.o2alpha=0;
        stats.o2rho=0;
        %makes no sense to confine region in confluent layer
        h.params.stats.confReg=0;
    else
        %no mask: abort
        errordlg(sprintf('No Mask Present'));
        return;
    end
else
    M1=zeros(size(M));
    M2=zeros(size(M));
    M1(find(label==l(2)))=1;
    if(length(l)==3)
        M2(find(label==l(3)))=1;
    end
end
%++++++++++++++++++
% find initial scratch orientation
%++++++++++++++++++
%for M1
if ~C.CONF
    dist=bwdist(M1);
    [Gy,Gx]=gradient(dist);
    im1=find(M1);
    gy=sum(Gy(im1))/length(im1);
    gx=sum(Gx(im1))/length(im1);
    [stats.o1alpha, stats.o1rho]=cart2pol(gy,gx);
    %width in scratch direction
    if(abs(gy) > abs(gx))
        width=size(M,1);
    else
        width=size(M,2);
    end
    %for M2
    m2valid=0;
    if sum(M2(:))>0,
        m2valid=1;
        dist=bwdist(M2);
        [Gy,Gx]=gradient(dist);
        im2=find(M2);
        gy=sum(Gy(im2))/length(im2);
        gx=sum(Gx(im2))/length(im2);
        [stats.o2alpha, stats.o2rho]=cart2pol(gy,gx);
    else
        stats.o2alpha=NaN;
        stats.o2rho=NaN;
    end
    %plot and store it
  
    h2=figure;
    imshow(imadjust(curData.general.I_orig));
    hold on;
    scl=5;
    [s1,s2]=size(curData.general.I_orig);
    [s1,s2]=size(curData.general.I_orig);
    midx=round(s2/2);
    midy=round(s1/2);
    [a1x,a1y]=img_arrow(stats.o1alpha,scl,midx,midy);
    [a2x,a2y]=img_arrow(stats.o2alpha,scl,midx,midy);
    plot(a1x,a1y,'Color',color_my3(2,1,1),'LineWidth',5);
    plot(a2x,a2y,'Color',color_my3(2,1,2),'LineWidth',5);
    hold off;
      
    outfileBase=fullfile(pthSTATS,'%s',sprintf('InitialScratchOrientation.%%s'));
    if C.expFig
        export_fig(sprintf2(outfileBase,'images','pdf'));
    else
        saveas(h2,sprintf2(outfileBase,'images','tif'));
    end
    saveas(h2,sprintf2(outfileBase,'figures','fig'));
    close(h2);
    pause(0.001);
end

%++++++++++++++++++
% GO THROUGH FRAMES
%++++++++++++++++++
%reset start data

f=max(h.batchInfo.minFrame,statStart);
join=min(statStop+1,h.batchInfo.maxFrame);
while f<join
    % show in gui (if gui up and running)
    try
        set(h.T_processing,'String',sprintf('statistics on Frame %i',f));
        drawnow();
    catch
        disp(sprintf('processing Frame %i',f));
    end
    
    
    %load next data
    curData=CIV_loadData(f,h.batchInfo,h.params);
    if ~checkData(curData,f)
        return;
    end
    
    % reset for excluded data
    if(curData.general.exclude || f<statStart)
        disp('exluded');
        %exclude from statistics
        EXF(f)=1;
        for msk=1:2
            
            ADPBF{f}{msk}=[];
            SPBF{f}{msk}=[];
            OSPBF{f}{msk}=[];
            OPBF{f}{msk}=[];
        end
        f=f+1;
        continue;
    end
    
    % confine mask
    M=curData.mask.M;
    MM=M;
    DM=[];
    if(P.confReg)
        DM=bwdist(1-MM);
        ind=find(DM>h.params.stats.regExt);
        %catch confluent layer
        if length(ind)==length(M(:))
            ind=[];
        end
        MM(ind)=0;
    end
    
    % find submasks
    % based on distance to old masks
    if (f<join & ~C.CONF)
        % only if we are looking for submasks
        
        %distance maps to old maps
        d1=bwdist(M1);
        d2=bwdist(M2);
        M1=zeros(size(M));
        M2=zeros(size(M));
        
        label=bwlabel(M);
        l=unique(label);
        
        
        %assign components to masks
        % first comp is background
        for i=2:length(l)
            ind=find(label==l(i));
            dist1=min(min(d1(ind)));
            dist2=min(min(d2(ind)));
            if(dist1==dist2)
                if(dist1<=1)
                    M1(ind)=1;
                    M2(ind)=1;
                    join=f;
                else
                    s1=sum(sum(d1(ind)));
                    s2=sum(sum(d2(ind)));
                    if(s1<s2)
                        M1(ind)=1;
                    else
                        M2(ind)=1;
                    end
                end
            elseif(dist1<dist2)
                M1(ind)=1;
            else
                M2(ind)=1;
            end
        end
        if(join==f)
            M1=zeros(size(M));
            M2=zeros(size(M));
        end
    else
        M1=zeros(size(M));
        M2=zeros(size(M));
    end
    
    
    %+++++++++++++
    % calculations
    %+++++++++++++
    %shifting
    SHIFTF(f,1)=curData.piv.shift;
    SHIFTF(f,2)=curData.piv.us;
    SHIFTF(f,3)=curData.piv.vs;
    
    %extract data
    u=curData.piv.lu*fac;
    v=curData.piv.lv*fac;
    xi=curData.piv.xi;
    yi=curData.piv.yi;
    
    % angle and magnitude
    [alpha rho]=cart2pol(u,v);
    alpha=rem(-alpha+2*pi,2*pi);
    
    % if not joined
    if(f~=join)
        %for masks
        for msk=1:2
            %mask
            expr=sprintf('MM=M%d;',msk);
            eval(expr);
            m=MM(yi+(xi-1)*size(M1,1));
            % confine region
            if(P.confReg)
                ind=find(DM>P.regExt);
                expr=sprintf('MM(ind)=0;',msk);
                eval(expr);
            end
            % generate confined mask
            cm=MM(yi+(xi-1)*size(M1,1));
            icm=find(cm);
            sicm=max(length(icm),1);
            
            % orient with scratch
            expr=sprintf('alphaM=rem(-alpha+4*pi-stats.o%dalpha,2*pi);',msk);
            eval(expr);
            
            
            %++++++++++++++
            % SPEED AND ORIENTATION
            %++++++++++++++
            ALLt=ALLt.*NaN;
            ALLt(1)=sum(rho(icm))/sicm;              %layer speed
            ALLt(2)=sum(cos(alphaM(icm)))/sicm;      %orientation
            ALLt(3)=sum(abs(cos(alphaM(icm))))/sicm;  %absolute orientation
            ALLt(4)=0;                               %min speed
            ALLt(5)=0;                               %max speed
            if(~isempty(icm))
                ALLt(4)=min(rho(icm));
                ALLt(5)=max(rho(icm));
            end
            
            %++++++++++++++
            % AVD
            %++++++++++++++
            AVDt=AVDt.*NaN;
            for s=1:P.sectors
                ia=intersect(intersect(find(alphaM>=sectors(s)),find(alphaM<sectors(s+1))),icm);
                sria=sum(rho(ia));
                AVDt(s)=sria*P.sectors/sicm;
            end
            
            %++++++++++++++
            % INAXIS VELCITY CONTRIBUTION
            % NSEW
            %++++++++++++++
            NSEWt=NSEWt.*NaN;
            AVDC=cos(secRad).*AVDt;
            AVDS=sin(secRad).*AVDt;
            NSEWt(1)=sum(AVDC([isec1,isec4]))/P.sectors; %uop
            NSEWt(2)=sum(AVDC([isec2,isec3]))/P.sectors; %uon
            NSEWt(3)=sum(AVDS([isec1,isec2]))/P.sectors; %upp
            NSEWt(4)=sum(AVDS([isec3,isec4]))/P.sectors; %upn
            
            
            %collect data
            ALLF(f,msk,:)=ALLt;
            AVDF(f,msk,:)=AVDt;
            NSEWF(f,msk,:)=NSEWt;
            DISTF(f)=(sum(1-M(:))/width)*fac2;
            
            %exclude from statistics
            EXF(f)=curData.general.exclude;
            
            if P.space
                %++++++++++++++
                % SPATIAL
                %++++++++++++++
                %radial correlation per bin
                [ADPBF{f}{msk}]=img_angulardiff(alphaM,m,round(P.corrDist/fac3),P.distBinSS/fac3,P.distBinOL);
                %spead per bin
                SPBF{f}{msk}=img_distbin(rho,m,P.distBinSS/fac3,P.distBinOL);
                % orthogonal speed per bin
                [vx,vy]=pol2cart(alphaM,rho);
                OSPBF{f}{msk}=img_distbin(vx,m,P.distBinSS/fac3,P.distBinOL);
                % orientation per bin
                OPBF{f}{msk}=img_distbin(cos(alphaM),m,P.distBinSS/fac3,P.distBinOL);
            else
                ADPBF{f}{msk}=[];
                SPBF{f}{msk}=[];
                OSPBF{f}{msk}=[];
                OPBF{f}{msk}=[];
            end
        end
        f=f+1;
    end
end



%++++++++++++++++++
% CALCULATE AVERAGES
%++++++++++++++++++
iStrt=find(1-EXF(1:join-1));
%all
mALL=squeeze(nanmean(ALLF,1));
steALL=squeeze(nanstd(ALLF,1)/sqrt(sum(1-EXF)));
%AVD
mAVD=squeeze(nanmean(AVDF,1));
steAVD=squeeze(nanstd(AVDF,1)/sqrt(sum(1-EXF)));
%NSEW
mNSEW=squeeze(nanmean(NSEWF,1));
steNSEW=squeeze(nanstd(NSEWF,1)/sqrt(sum(1-EXF)));
% scratch wound closing velocity:
lls=DISTF(iStrt)'/[iStrt'*h.params.general.dt;ones(1,length(iStrt))];
woundVel=-lls(1);
startDist=lls(2);
%++++++++++++++++++
% CALCULATE TIME AVERAGES
%++++++++++++++++++
tbins=floor((join-P.tempBinSS)/(P.tempBinSS*(1-P.tempBinOL)))+1;
mAVDT=NaN(tbins,2,P.sectors);
mNSEWT=NaN(tbins,2,4);
steNSEWT=NaN(tbins,2,4);
for msk=1:2
    for t=1:tbins
        istr=round((t-1)*P.tempBinSS*(1-P.tempBinOL)+1);
        istp=istr+P.tempBinSS-1;
        idata=intersect(istr:istp,iStrt);
        if ~isempty(idata)
            mAVDT(t,msk,:)=squeeze(nanmean(AVDF(idata,msk,:)));
            mNSEWT(t,msk,:)=nanmean(NSEWF(idata,msk,:));
            steNSEWT(t,msk,:)=nanstd(NSEWF(idata,msk,:))/sqrt(length(idata));
        end
    end
end
        
%++++++++++++++++++
% CALCULATE TIME SPACE AVERAGES
%++++++++++++++++++
dbinsAD=[];
dbinsS=[];
ADTS=[];
STS=[];
OSTS=[];
OTS=[];
if P.space
    for msk=1:2
        dbinsAD(msk)=size(ADPBF{end}{msk},2);
        dbinsS(msk)=size(SPBF{end}{msk},2);
        
        ADTS{msk}=NaN(tbins,dbinsAD(msk));
        STS{msk}=NaN(tbins,dbinsS(msk));
        OSTS{msk}=NaN(tbins,dbinsS(msk));
        OTS{msk}=NaN(tbins,dbinsS(msk));
        
        for t=1:tbins
            istr=round((t-1)*P.tempBinSS*(1-P.tempBinOL)+1);
            istp=istr+P.tempBinSS-1;
            idata=intersect(istr:istp,iStrt);
            ADtmp=NaN(length(idata),dbinsAD(msk));
            Stmp=NaN(length(idata),dbinsS(msk));
            OStmp=Stmp;
            Otmp=Stmp;
            for t1=1:length(idata)
                dbinsADt=min(size(ADPBF{idata(t1)}{msk},2),dbinsAD(msk));
                for s=1:dbinsADt
                    ADtmp(t1,s)=ADPBF{idata(t1)}{msk}(end,s);
                end
                dbinsSt=min(size(SPBF{idata(t1)}{msk},2),dbinsS(msk));
                for s=1:dbinsSt
                    Stmp(t1,s)=SPBF{idata(t1)}{msk}(s);
                    OStmp(t1,s)=OSPBF{idata(t1)}{msk}(s);
                    Otmp(t1,s)=OPBF{idata(t1)}{msk}(s);
                end
            end    
            ADTS{msk}(t,:)=nanmean(ADtmp,1);
            STS{msk}(t,:)=nanmean(Stmp,1);
            OSTS{msk}(t,:)=nanmean(OStmp,1);
            OTS{msk}(t,:)=nanmean(Otmp,1);
        end
    end
end


%++++++++++++++++++
% store statistics
%++++++++++++++++++
stats.ALLF=ALLF;
stats.mALL=mALL;
stats.steALL=steALL;
stats.AVDF=AVDF;
stats.mAVD=mAVD;
stats.steAVD=steAVD;
stats.NSEWF=NSEWF;
stats.mNSEW=mNSEW;
stats.steNSEW=steNSEW;
stats.EXF=EXF;
stats.SHIFTF=SHIFTF;
stats.DISTF=DISTF;
stats.woundVel=woundVel;
stats.startDist=startDist;
stats.P=P;
stats.sectors=sectors;
stats.secSize=secSize;
stats.start=statStart;
stats.join=join;
stats.stop=statStop;
stats.ADPBF=ADPBF;
stats.SPBF=SPBF;
stats.OSPBF=OSPBF;
stats.OPBF=OPBF;
stats.params=h.params;
stats.batchInfo=h.batchInfo;
%time binning
stats.tbins=tbins;
stats.mAVDT=mAVDT;
stats.mNSEWT=mNSEWT;
stats.steNSEWT=steNSEWT;
%time space binning
stats.dbinsAD=dbinsAD;
stats.dbinsS=dbinsS;
stats.ADTS=ADTS;
stats.STS=STS;
stats.OSTS=OSTS;
stats.OTS=OTS;

%to CIV_stats.mat file
try
    set(h.T_processing,'String','Storing Statistics');
    drawnow();
catch
    disp('Storing Statistics');
end
save(fullfile(pthSTATS,'data','CIV_stats.mat'),'-struct','stats');

% write data to ascii file
try
    set(h.T_processing,'String','Writing Statistics');
    drawnow();
catch
    disp('Writing Statistics');
end
err =writeAscii(fullfile(pthSTATS,'data','CIV_stats.txt'),stats,h);
if err > 0
    errordlg('Error writing to file..');
end

% plot data
try
    set(h.T_processing,'String','Plotting Statistics');
    drawnow();
catch
    disp('Plotting Statistics');
end
ok =CIV_plotStats(pthSTATS,stats,h);
if ~ok  errordlg('Error plotting..');
end
ok=1;

% check if piv and mask data is around
function ok = checkData(curData,frame)
ok=1;
if(~curData.piv.done || ~curData.mask.done)
    ok=0;
    errordlg(sprintf('Not all data present for selected range, currently at %i!',frame));
end

% write data to ascii file
function [err,msg] = writeAscii(file,stats,h)

msg='nothing';
f = fopen(file,'wt');
err = 0;

if f == -1,
    err = 1;
    msg = 'Failed to open file.';
    return
end

%++++++++++++++
% BATCH
%++++++++++++++
fprintf(f,sprintf('BATCH:\n'));
fprintf(f,sprintf('FolderPath\t%s\n%',strrep(h.batchInfo.folderpath,'\','\\')));
fprintf(f,sprintf('FilePattern\t%s\t\n%',strrep(h.batchInfo.pattern,'%','%%')));
fprintf(f,sprintf('StartFrame\t%d\n%',stats.start));
fprintf(f,sprintf('JoinFrame\t%d\n%',stats.join));
fprintf(f,sprintf('StopFrame\t%d\n%',stats.stop));
fprintf(f,'\n');
fprintf(f,'\n');
%++++++++++++++
% STATISTICS PARAMS
%++++++++++++++
fprintf(f,sprintf('STATISTICS PARAMS:\n'));
fprintf(f,sprintf('Sectors\t%d\n%',stats.P.sectors));
%+++
fprintf(f,sprintf('Confine Region\t%d\n%',stats.P.confReg));
fprintf(f,sprintf('Region Extent\t%d\n%',stats.P.regExt));
fprintf(f,sprintf('Time Bin Size\t%d\n%',stats.P.tempBinSS));
fprintf(f,sprintf('Time Bin Overlap\t%d\n%',stats.P.tempBinOL));
fprintf(f,sprintf('Spatial Analysis\t%d\n%',stats.P.space));
fprintf(f,sprintf('Distance Bin Size\t%d\n%',stats.P.distBinSS));
fprintf(f,sprintf('Distance Bin Overlap\t%d\n%',stats.P.distBinOL));
fprintf(f,sprintf('Correlation Distane\t%d\n%',stats.P.corrDist));
fprintf(f,'\n');
fprintf(f,'\n');


%++++++++++++++
% SCRATCH ORIENTATIONS/Speed
%++++++++++++++
fprintf(f,sprintf('SCRATCH ORIENTATIONS/Speed:\n'));
fprintf(f,sprintf('%s\t%s\n','Mask','alpha'));
fprintf(f,sprintf('M1\t%f5.5\n',stats.o1alpha));
fprintf(f,sprintf('M2\t%f5.5\n',stats.o2alpha));
fprintf(f,sprintf('WoundClosingVel\t%f5.5\n',-stats.woundVel));
fprintf(f,'\n');
fprintf(f,'\n');

%++++++++++++++
% TIME AVERAGE QUANTITIES PIV
%++++++++++++++
fprintf(f,sprintf('TIME AVERAGE QUANTITIES PIV:\n'));
fprintf(f,sprintf('%s\t%s\t%s\t%s\t%s\t%s','Label','speed','orient','orientAbs','minSpeed','maxSpeed'));
for s=1:length(stats.sectors)-1
    fprintf(f,sprintf('\tAVD:%1.4f-%1.4f',stats.sectors(s),stats.sectors(s+1)));
end
fprintf(f,'\n');

% avg M1
fprintf(f,'MEAN M1');
for i=1:length(stats.mALL)
    fprintf(f,sprintf('\t%5.5f',stats.mALL(1,i)));
end
for i=1:length(stats.mAVD)
    fprintf(f,sprintf('\t%5.5f',stats.mAVD(1,i)));
end
% ste Ma
fprintf(f,'\n');
fprintf(f,'STE M1');
for i=1:length(stats.steALL)
    fprintf(f,sprintf('\t%5.5f',stats.steALL(1,i)));
end
for i=1:length(stats.steAVD)
    fprintf(f,sprintf('\t%5.5f',stats.steAVD(1,i)));
end
fprintf(f,'\n');

% avg M1
fprintf(f,'MEAN M2');
for i=1:length(stats.mALL)
    fprintf(f,sprintf('\t%5.5f',stats.mALL(2,i)));
end
for i=1:length(stats.mAVD)
    fprintf(f,sprintf('\t%5.5f',stats.mAVD(2,i)));
end
% ste Ma
fprintf(f,'\n');
fprintf(f,'STE M2');
for i=1:length(stats.steALL)
    fprintf(f,sprintf('\t%5.5f',stats.steALL(2,i)));
end
for i=1:length(stats.steAVD)
    fprintf(f,sprintf('\t%5.5f',stats.steAVD(2,i)));
end
fprintf(f,'\n');
fprintf(f,'\n');
fprintf(f,'\n');

%++++++++++++++
% FRAME AVERAGE QUANTITIES
%++++++++++++++
% write Header
fprintf(f,sprintf('FRAME AVERAGE QUANTITIES:\n'));
fprintf(f,sprintf('%s\t%s\t%s\t%s\t%s\t%s','Frame','Dist','Exclude','shift','us','vs'));
fprintf(f,sprintf('\t%s\t%s\t%s\t%s\t%s','speed M1','orient M1','orientAbsM1','minSpeed M1','maxSpeed M1'));
for s=1:length(stats.sectors)-1
    fprintf(f,sprintf('\tAVD M1:%1.4f-%1.4f',stats.sectors(s),stats.sectors(s+1)));
end
fprintf(f,sprintf('\t%s\t%s\t%s\t%s\t%s','speed M2','orient M2','orientAbsM2','minSpeed M2','maxSpeed M2'));
for s=1:length(stats.sectors)-1
    fprintf(f,sprintf('\tAVD M2:%1.4f-%1.4f',stats.sectors(s),stats.sectors(s+1)));
end
fprintf(f,'\n');
% write Data
for k=1:stats.stop
    fprintf(f,sprintf('%d',k));
    fprintf(f,sprintf('\t%5.5f',stats.DISTF(k)));
    fprintf(f,sprintf('\t%d',stats.EXF(k)));
    fprintf(f,sprintf('\t%d',stats.SHIFTF(k,1)));
    fprintf(f,sprintf('\t%d',stats.SHIFTF(k,2)));
    fprintf(f,sprintf('\t%d',stats.SHIFTF(k,3)));
    
    for i=1:size(stats.ALLF,3)
        fprintf(f,sprintf('\t%5.5f',stats.AVDF(k,1,i)));
    end
    for i=1:size(stats.AVDF,3)
        fprintf(f,sprintf('\t%5.5f',stats.AVDF(k,1,i)));
    end
    for i=1:size(stats.ALLF,3)
        fprintf(f,sprintf('\t%5.5f',stats.AVDF(k,2,i)));
    end
    for i=1:size(stats.AVDF,3)
        fprintf(f,sprintf('\t%5.5f',stats.AVDF(k,2,i)));
    end
    fprintf(f,'\n');
end
fprintf(f,'\n');
fprintf(f,'\n');

%++++++++++++++
% SIMULATION PARAMETERS
%++++++++++++++
fprintf(f,sprintf('PARAMETERS:\n'));
fprintf(f,sprintf('General:\n'));
fprintf(f,sprintf('GENPATH\t%s\n',h.params.general.folder));
fprintf(f,sprintf('PIVPATH\t%s\n',strrep(fullfile(h.params.general.folder,h.params.general.folderPIV),'\','\\')));
fprintf(f,sprintf('MASKPATH\t%s\n',strrep(fullfile(h.params.general.folder,h.params.general.folderMASK),'\','\\')));
%general
fprintf(f,sprintf('Frame Interval\t%5.5f\n',h.params.general.dt));
fprintf(f,sprintf('Resolution\t%5.5f\n',h.params.general.resolution));
fprintf(f,sprintf('Scaling Factor\t%5.5f\n',h.params.general.scaleFactor));
%piv
fprintf(f,sprintf('PIV:\n'));
fprintf(f,sprintf('LNF used\t%d\n',h.params.piv.lnf));
fprintf(f,sprintf('LNF Wsize\t%d\n',h.params.piv.lnf_winsize));
fprintf(f,sprintf('Wsize\t%s\n',mat2str(h.params.piv.winsize)));
fprintf(f,sprintf('Overlap\t%5.5f\n',h.params.piv.overlap));
fprintf(f,sprintf('Method\t%s\n',h.params.piv.method));
%++
fprintf(f,sprintf('Shift Correction \t%s\n',h.params.piv.sft));
fprintf(f,sprintf('Shift Detection Threshold \t%s\n',h.params.piv.sftTh));
fprintf(f,sprintf('Signal to Noise Ratio Filter \t%s\n',h.params.piv.snrF));
fprintf(f,sprintf('Signal to Noise Ratio Filter Threshold\t%s\n',h.params.piv.snrTh));
fprintf(f,sprintf('Global Filter \t%s\n',h.params.piv.glbF));
fprintf(f,sprintf('Global Filter Threshold\t%s\n',h.params.piv.glbTh));
fprintf(f,sprintf('Local Filter \t%s\n',h.params.piv.locF));
fprintf(f,sprintf('Local Filter Threshold\t%s\n',h.params.piv.locTh));
fprintf(f,sprintf('Local Filter KernelSize\t%s\n',h.params.piv.locKs));
fprintf(f,sprintf('Local Filter Method\t%s\n',h.params.piv.locMtd));
%++
%mask
fprintf(f,sprintf('MASK:\n'));
fprintf(f,sprintf('Th1\t%5.5f\n',h.params.mask.D_th1));
fprintf(f,sprintf('Th2\t%5.5f\n',h.params.mask.D_th2));
fprintf(f,sprintf('Close1\t%d\n',h.params.mask.D_cl1));
fprintf(f,sprintf('Area\t%d\n',h.params.mask.D_a));
fprintf(f,sprintf('Close2\t%d\n',h.params.mask.D_cl2));
fprintf(f,sprintf('Dilation\t%d\n',h.params.mask.D_dil));

fprintf(f,sprintf('F1 used\t%d\n',h.params.mask.useFilter1));
fprintf(f,sprintf('F1 name\t%s\n',h.params.mask.f1_name));
fprintf(f,sprintf('F1 params\t%s\n',cell2str(h.params.mask.f1_params)));
fprintf(f,sprintf('F2 used\t%d\n',h.params.mask.useFilter2));
fprintf(f,sprintf('F2 name\t%s\n',h.params.mask.f2_name));
fprintf(f,sprintf('F2 params\t%s\n',cell2str(h.params.mask.f2_params)));
fprintf(f,sprintf('Prev Mask used\t%d\n',h.params.mask.usePrev));
fprintf(f,sprintf('Maximal Speed1\t%d\n',h.params.mask.d1));
fprintf(f,sprintf('Shift Detection used\t%d\n',h.params.mask.corrShift));
fprintf(f,sprintf('Correction Factor\t%5.5f\n',h.params.mask.frac));

[msg,errno]=ferror(f);
if errno
    err=2;
    fclose(f);
    return
end
fclose(f);
