function R=color_my2(maxv,minv,val)
rv=max(maxv-minv,1);
v=val-minv;
R=[v/rv,1-v/rv,0.6];
return