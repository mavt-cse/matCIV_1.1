function res=EXTCIV_collapseBatchStatistics(DATA,outfolder)

% post process and collaps statistics
% cleaned up version to work with export_fig
% include export_fig in CIV_setpath

if nargin<2
    outfolder='COLLAPSEDSTATS';
end

% +++++++++++++++++
%   PLOTTING
% +++++++++++++++++
try
    C.expFig=1;
    h1=figure;
    axis;
    export_fig(['tst','.pdf']);
    system('rm tst.pdf');
    close(h1);
catch
    C.expFig=0;
end


% +++++++++++++++++
% make output folder
% +++++++++++++++++
mkdir(fullfile(DATA.basePath,outfolder));
mkdir(fullfile(DATA.basePath,outfolder,'images'));
mkdir(fullfile(DATA.basePath,outfolder,'figures'));
mkdir(fullfile(DATA.basePath,outfolder,'data'));

% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  INIT
% ||    Assumptions:
% ||      - stats parameters same for all experiments
% ||
% |||||||||||||||||||||||
% |||||||||||||||||||||||

% +++++++++++++++++
%   INIT global parameters that hold for all experiments
%   - load from first experiment
% +++++++++++++++++
dataFolder='DATA'; %adapt if changed
P_G=load(fullfile(DATA.basePath,DATA.EXP{1}.folder,DATA.EXP{1}.pos{1},dataFolder,'CIV_params.mat'));
P_STS=load(fullfile(DATA.basePath,DATA.EXP{1}.folder,DATA.EXP{1}.pos{1},...
    dataFolder,'CIV_paramsSTATS.mat'));
statsFolder=strcat('STAT-',...
    P_G.folderPIV,'-',...
    P_G.folderMASK);
if(P_STS.confReg)
    statsFolder=sprintf('%s_CONF%d',statsFolder,P_STS.regExt);
end

%reload statistics parameters directly from stats folder
P_STS=load(fullfile(DATA.basePath,DATA.EXP{1}.folder,DATA.EXP{1}.pos{1},...
    dataFolder,statsFolder,'data','CIV_paramsSTATS.mat'));


% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  LOAD STATS OF SINGLE EXPERIMENTS
% ||
% |||||||||||||||||||||||
% |||||||||||||||||||||||

% experiments per conditon
for c=1:DATA.P.NC
    nData(c)=0;
end

d=1;
for e=1:length(DATA.EXP);
    for p=1:length(DATA.EXP{e}.pos);
        EXP{d}.data=load(fullfile(DATA.basePath,DATA.EXP{e}.folder,...
            DATA.EXP{e}.pos{p},dataFolder,statsFolder,'data','CIV_stats.mat'));
        EXP{d}.condP=DATA.EXP{e}.condP{p};
        EXP{d}.condM(1)=DATA.EXP{e}.condM{p}(1);
        EXP{d}.condM(2)=DATA.EXP{e}.condM{p}(2);
        c1=(EXP{d}.condP-1)*DATA.P.NCM+EXP{d}.condM(1);
        c2=(EXP{d}.condP-1)*DATA.P.NCM+EXP{d}.condM(2);
        nData(c1)=nData(c1)+1;
        nData(c2)=nData(c2)+1;
        d=d+1;
    end
end


% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  FIND THINGS
% ||   - maximal time
% ||   - maximal distance into the layer
% ||
% |||||||||||||||||||||||
% |||||||||||||||||||||||

C.ntb=0;
C.ndb=0;
C.ndbAC=0;
for e=1:length(EXP)   
    C.ntb=max(C.ntb,EXP{e}.data.tbins);
    C.ndb=max(C.ndb,min(EXP{e}.data.dbinsS));
    C.ndbAC=max(C.ndb,min(EXP{e}.data.dbinsAD));
end
%number of time bins
C.ntb=min(C.ntb,round((DATA.P.end-(P_STS.tempBinSS*(1-P_STS.tempBinOL)))/...
    (P_STS.tempBinSS*(1-P_STS.tempBinOL))));

C.ndb=min(C.ndb,DATA.P.ndb);
C.ndbAC=min(C.ndb,DATA.P.ndbAC);


%%
% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  Wound closing speed
% ||   - group by metric / not by condition
% ||   - crop in time
% |||||||||||||||||||||||
% |||||||||||||||||||||||
DO_VEL=1;
if DO_VEL

    % ++++++++++++
    %   collect data
    % ++++++++++++
    CWCS=zeros(sum(nData)/2,1); %condition
    WCS=zeros(sum(nData)/2,1); % wound closing velocity
    CLSO=zeros(sum(nData),1); %condition
    LSO=zeros(sum(nData),3);  % layer velocity, layer orientation, absolute layer orientation
    
    i=1;
    for e=1:length(EXP)
        
        %indices
        i1=2*(i-1)+1;
        i2=2*(i-1)+2;
        
        CWCS(i)=EXP{e}.condP; %condition of position
        %only consider cropped frames
        iStrt=find(1-EXP{e}.data.EXF(max(1,DATA.P.start-EXP{e}.data.start+1):...
            min(EXP{e}.data.join-EXP{e}.data.start,DATA.P.end-EXP{e}.data.start+1))); %croped indices
        lls=EXP{e}.data.DISTF(iStrt)'/[iStrt'*P_G.dt;ones(1,length(iStrt))];
        WCS(i)=-lls(1);  %wound closing speed
        
        %mask 1
        CLSO(i1)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(1);    %condition
        LSO(i1,1)=sum(EXP{e}.data.ALLF(iStrt,1,1))/length(iStrt);        %layer velocity
        LSO(i1,2)=sum(EXP{e}.data.ALLF(iStrt,1,2))/length(iStrt);        %orientation
        LSO(i1,3)=sum(EXP{e}.data.ALLF(iStrt,1,3))/length(iStrt);        %abs(orientation)
        
        %mask 2
        CLSO(i2)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(2);
        LSO(i2,1)=sum(EXP{e}.data.ALLF(iStrt,2,1))/length(iStrt);
        LSO(i2,2)=sum(EXP{e}.data.ALLF(iStrt,2,2))/length(iStrt);
        LSO(i2,3)=sum(EXP{e}.data.ALLF(iStrt,2,3))/length(iStrt);
        
        i=i+1;
    end
    
    % ++++++++++++
    %   mean and standard error
    % ++++++++++++
    %   - front velocity
    mWCS=zeros(DATA.P.NCP,1);
    steWCS=mWCS;
    for c=1:DATA.P.NCP
        ind=find(CWCS(:)==c);
        mWCS(c,:)=mean(WCS(ind),1); %mean
        steWCS(c,:)=std(WCS(ind),0,1)/sqrt(length(ind)); %standard error of mean
    end
    %   - layer velocity
    %   - orientation
    %   - abs orientation
    mLSO=zeros(DATA.P.NC,3);
    steLSO=mLSO;
    for c=1:DATA.P.NC
        ind=find(CLSO(:)==c);
        mLSO(c,:)=mean(LSO(ind,:),1);
        steLSO(c,:)=std(LSO(ind,:),0,1)/sqrt(length(ind));
    end
    
    % ++++++++++++
    %   calculate significance
    % ++++++++++++
    %   - front velocity
    tWCS=zeros(DATA.P.NCP,DATA.P.NCP);
    pWCS=tWCS;
    for c=1:DATA.P.NCP
        indC=find(CWCS==c);
        for c2=c+1:DATA.P.NCP
            indC2=find(CWCS==c2);
            [tWCS(c,c2),pWCS(c,c2)]=ttest2(WCS(indC),WCS(indC2),0.05,'both','unequal');
        end
    end
    %   - layer velocity
    %   - orientation
    %   - speed
    tLSO=zeros(DATA.P.NC,DATA.P.NC,3);
    pLSO=tLSO;
    for c=1:DATA.P.NC
        indC=find(CLSO==c);
        for c2=c+1:DATA.P.NC
            indC2=find(CLSO==c2);
            [tLSO(c,c2,1),pLSO(c,c2,1)]=ttest2(LSO(indC,1),LSO(indC2,1),0.05,'both','unequal');
            [tLSO(c,c2,2),pLSO(c,c2,2)]=ttest2(LSO(indC,2),LSO(indC2,2),0.05,'both','unequal');
            [tLSO(c,c2,3),pLSO(c,c2,3)]=ttest2(LSO(indC,3),LSO(indC2,3),0.05,'both','unequal');
        end
    end
    
    
    % ++++++++++++
    %   plot front velocity
    %   - grouped by metric
    % ++++++++++++
    h=figure('Name','Wound Closing Velocity');
    ms=3; %marker size
    spcD=0.6;
    spc=spcD/(DATA.P.NCP-1);
    spcS=0.7;
    if spc==0
        spcS=1;
    end
    
    %plot data
    h1=plot(spcS+(CWCS-1)*spc,WCS/2,'ok','MarkerSize',ms);
    hold on
    
    %plot mean and error
    % front velocity
    
    H=[];
    for c=1:DATA.P.NCP
        errorbar(spcS+(c-1)*spc,mWCS(c)/2,steWCS(c)/2,'o','Color',color_my2(DATA.P.NCP,1,c));
        expr=sprintf('h%d=errorbar(spcS+(c-1)*spc,mWCS(c)/2,steWCS(c)/2,''o'',''Color'',color_my2(DATA.P.NCP,1,c));',c+1);
        eval(expr);
        expr=sprintf('H=[H,h%d];',c+1);
        eval(expr);
    end
    %make plot nice
    %axis([0.5,4.5,-0.02,max(LSO)*1.2]);
    %axis tight
    %lable things
    title('Wound Front Speed');
    ylabel('speed [\mum/min]');
    xlabel('experimental condition');
    % tics
    set(gca,'XTick',[1])
    set(gca,'XTickLabel',{''})
    %store
    set(gca, 'Color', 'none');
    set(gcf, 'Color', 'none');
    leg=DATA.P.CONDP;
    leg{end+1}='data';
    legend(H,leg,'Location','NorthWest');
    outfileBase=fullfile(DATA.basePath,outfolder,'%s','FS.%s');
    if C.expFig
        export_fig(sprintf2(outfileBase,'images','pdf'));
    else
        saveas(h,sprintf2(outfileBase,'images','tif'));
    end
    saveas(h,sprintf2(outfileBase,'figures','fig'));
    close(h)
    
    
    % ++++++++++++
    %   plot mean layer velocity and orientation
    %   - grouped by metric
    % ++++++++++++
    h=figure('Name','Cell Layer Speed and Orientation');
    ms=3; %marker size
    spcD=0.6;
    spc=spcD/(DATA.P.NC-1);
    spcS=0.7;
    if spc==0
        spcS=1;
    end
    %%
    %plot data
    plot(spcS+(CLSO-1)*spc,LSO(:,1),'ok','MarkerSize',ms)
    hold on
    plot(1+spcS+(CLSO-1)*spc,LSO(:,2),'ok','MarkerSize',ms)
    h1=plot(2+spcS+(CLSO-1)*spc,LSO(:,3),'ok','MarkerSize',ms);
    
    %plot mean and error
    % layer velocity and orientation
    H=[];
    for c=1:DATA.P.NC
        errorbar(spcS+(c-1)*spc,mLSO(c,1),steLSO(c,1)/2,'o','Color',color_my2(DATA.P.NC,1,c));
        errorbar(1+spcS+(c-1)*spc,mLSO(c,2),steLSO(c,2)/2,'o','Color',color_my2(DATA.P.NC,1,c));
        expr=sprintf('h%d=errorbar(2+spcS+(c-1)*spc,mLSO(c,3),steLSO(c,3)/2,''o'',''Color'',color_my2(DATA.P.NC,1,c));',c+1);
        eval(expr);
        expr=sprintf('H=[H,h%d];',c+1);
        eval(expr);
    end
    H(end+1)=h1;
    %make plot nice
    %axis([0.5,4.5,-0.02,max(LSO)*1.2]);
    %lable things
    title('Cell Layer Speed and Orientation');
    ylabel({'speed [\mum/min]';'orientation [cos(\theta)]'});
    xlabel('experimental condition');
    % tics
    set(gca,'XTick',[1 2 3])
    set(gca,'XTickLabel',{'layer speed','orientation','abs(orientation)'})
    %store
    set(gca, 'Color', 'none');
    set(gcf, 'Color', 'none');
    leg=DATA.P.COND;
    leg{end+1}='data';
    legend(H,leg,'Location','NorthWest');
    
    outfileBase=fullfile(DATA.basePath,outfolder,'%s','LSO.%s');
    if C.expFig
        export_fig(sprintf2(outfileBase,'images','pdf'));
    else
        saveas(h,sprintf2(outfileBase,'images','tif'));
    end
    saveas(h,sprintf2(outfileBase,'figures','fig'));
    close(h)
end

%%
% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  Angular Velocity Distribution
% ||
% |||||||||||||||||||||||
% |||||||||||||||||||||||

%+++++++++++
DO_AVD=1;
if DO_AVD
    
    % assume all have same number of sectors
    sectors=EXP{1}.data.sectors;
    secSize=EXP{1}.data.secSize;
    nSecs=length(sectors)-1;
    
    % all deflections to the same side
    CAVD=NaN(sum(nData),1);           %condition
    AVD=NaN(sum(nData),nSecs);        %overall average
    AVDT=NaN(sum(nData),C.ntb,nSecs); %time binning
    
    i=1;
    for e=1:length(EXP),
            
        i1=2*(i-1)+1;
        i2=2*(i-1)+2;
        %only consider cropped frames
        iStrt=find(1-EXP{e}.data.EXF(max(1,DATA.P.start-EXP{e}.data.start+1):...
            min(EXP{e}.data.join-EXP{e}.data.start,DATA.P.end-EXP{e}.data.start+1)));
        
        % +++++++++++
        %   mask 1
        % +++++++++++
        CAVD(i1)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(1);    %condition
        %check flip
        D=EXP{e}.data.mAVD(1,:);
        sra=sum(D(1:(end/2)));
        srb=sum(D((end/2+1):end));
        f1=0;
        if(sra<srb)
            %flip it
            f1=1;
        end
        %@@@ use nanmean
        if f1
            
            AVD(i1,:)=nanmean(EXP{e}.data.AVDF(iStrt,1,end:-1:1),1);
        else
            AVD(i1,:)=nanmean(EXP{e}.data.AVDF(iStrt,1,:),1);
        end
        
        % +++++++++++
        %   mask 2
        % +++++++++++
        CAVD(i2)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(2);    %condition
        %check flip
        D=EXP{e}.data.mAVD(2,:);
        sra=sum(D(1:(end/2)));
        srb=sum(D((end/2+1):end));
        if(sra<srb)
            %flip it
            f2=1;
        end
        if f2
            AVD(i2,:)=nanmean(EXP{e}.data.AVDF(iStrt,2,end:-1:1),1);
        else
            AVD(i2,:)=nanmean(EXP{e}.data.AVDF(iStrt,2,:),1);
        end        
        
        % +++++++++++
        %   time binning
        % +++++++++++
        ntb=min(C.ntb,EXP{e}.data.tbins);
        if f1
            AVDT(i1,1:ntb,:)=EXP{e}.data.mAVDT(1:ntb,1,end:-1:1);
        else
            AVDT(i1,1:ntb,:)=EXP{e}.data.mAVDT(1:ntb,1,:);
        end
        if f2
            AVDT(i2,1:ntb,:)=EXP{e}.data.mAVDT(1:ntb,2,end:-1:1);
        else
            AVDT(i2,1:ntb,:)=EXP{e}.data.mAVDT(1:ntb,2,:);
        end
            
        i=i+1;
    end
    
    %%
    %calculate mean, standard deviation and standard error
    mAVD=zeros(DATA.P.NC,nSecs);
    steAVD=mAVD;
    mAVDT=zeros(DATA.P.NC,C.ntb,nSecs);
    
    for c=1:DATA.P.NC
        ind=find(CAVD==c);
        mAVD(c,:)=nanmean(AVD(ind,:),1);
        steAVD(c,:)=nanstd(AVD(ind,:),0,1)/sqrt(length(ind));
        mAVDT(c,:,:)=squeeze(nanmean(AVDT(ind,:,:,1),1));
    end
    
    % ++++++++++++
    %   plot overlay condition AVD
    % ++++++++++++
    mxv=max(mAVD(:)+steAVD(:));
    
    for c=1:DATA.P.NC
        f1=figure('Name',sprintf('AVD_C%s',DATA.P.COND{c}));
        
        %+error
        pp(sectors+secSize/2,[mAVD(c,:)+steAVD(c,:) mAVD(c,1)+steAVD(c,1)],...
            'MaxValue',mxv*1.05,'CentreValue',0,...
            'LineColor',color_my2(DATA.P.NC,1,c),'LineWidth',0.7);
        hold on;
        %-error
        pp(sectors+secSize/2,max([mAVD(c,:)-steAVD(c,:) mAVD(c,1)-steAVD(c,1)],0),...
            'MaxValue',mxv*1.05,'CentreValue',0,...
            'LineColor',color_my2(DATA.P.NC,1,c),'LineWidth',0.7);
        %mean
        pp(sectors+secSize/2,[mAVD(c,:) mAVD(c,1)],...
            'MaxValue',mxv*1.05,'CentreValue',0,...
            'LineColor',color_my2(DATA.P.NC,1,c),'LineWidth',1.3);
        axis equal;
        set(gca, 'Color', 'none');
        set(gcf, 'Color', 'none');
        
        outfileBase=fullfile(DATA.basePath,outfolder,'%s',sprintf('AVD_%s.%%s',DATA.P.COND{c}));
        if C.expFig
            export_fig(sprintf2(outfileBase,'images','pdf'));
        else
            saveas(h,sprintf2(outfileBase,'images','tif'));
        end
        saveas(h,sprintf2(outfileBase,'figures','fig'));
        close(f1);
    end
    
    
    % ++++++++++++
    %   plot overlay every condition AVD
    % ++++++++++++
    mxv=max(mAVD(:)+steAVD(:));
    f1=figure('Name',sprintf('AVD'));
    for c=1:DATA.P.NC
        
        %+error
        pp(sectors+secSize/2,[mAVD(c,:)+steAVD(c,:) mAVD(c,1)+steAVD(c,1)],...
            'MaxValue',mxv*1.05,'CentreValue',0,...
            'LineColor',color_my2(DATA.P.NC,1,c),'LineWidth',0.7);
        hold on;
        %-error
        pp(sectors+secSize/2,max([mAVD(c,:)-steAVD(c,:) mAVD(c,1)-steAVD(c,1)],0),...
            'MaxValue',mxv*1.05,'CentreValue',0,...
            'LineColor',color_my2(DATA.P.NC,1,c),'LineWidth',0.7);
        %mean
        pp(sectors+secSize/2,[mAVD(c,:) mAVD(c,1)],...
            'MaxValue',mxv*1.05,'CentreValue',0,...
            'LineColor',color_my2(DATA.P.NC,1,c),'LineWidth',1.3);
        hold on
    end
    axis equal;
    set(gca, 'Color', 'none');
    set(gcf, 'Color', 'none');
    
    outfileBase=fullfile(DATA.basePath,outfolder,'%s','AVD.%s');
    if C.expFig
        export_fig(sprintf2(outfileBase,'images','pdf'));
    else
        saveas(h,sprintf2(outfileBase,'images','tif'));
    end
    saveas(h,sprintf2(outfileBase,'figures','fig'));
    close(f1);
    
    % ++++++++++++
    %   plot evolution AVD
    % ++++++++++++
    mxv=max(mAVDT(:));
    for c=1:DATA.P.NC
        f1=figure('Name',sprintf('AVDT_%s',DATA.P.COND{c}));
        
        for t=1:C.ntb
            
            pp(sectors+secSize/2,[squeeze(mAVDT(c,t,:))' squeeze(mAVDT(c,t,1))'],...
                'MaxValue',mxv*1.05,'CentreValue',0,...
                'LineColor',color_my3(C.ntb,1,t),'LineWidth',1.3);
            hold on
        end
        axis equal;
        set(gca, 'Color', 'none');
        set(gcf, 'Color', 'none');
        
        outfileBase=fullfile(DATA.basePath,outfolder,'%s',sprintf('TMP_AVD_%s.%%s',DATA.P.COND{c}));
        if C.expFig
            export_fig(sprintf2(outfileBase,'images','pdf'));
        else
            saveas(h,sprintf2(outfileBase,'images','tif'));
        end
        saveas(h,sprintf2(outfileBase,'figures','fig'));
        close(f1);
    end
end


%%
% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  N-S-E-W Velocity components
% ||  Total Average
% ||  Time Binning
% ||  - north/south/east/west components of velocity
% ||
% |||||||||||||||||||||||
% |||||||||||||||||||||||
DO_NSEW=1;
if DO_NSEW
    
    % assume all have same number of sectors
    sectors=EXP{1}.data.sectors;
    secSize=EXP{1}.data.secSize;
    nSecs=length(sectors)-1;
    secRad=sectors(1:end-1)+secSize/2;
    
    CNSEW=NaN(sum(nData),1);
    NSEW=NaN(sum(nData),4);
    NSEWT=NaN(sum(nData),C.ntb,4);
    
    %quadrants in counter clockwise order
    ir1=1:nSecs/4;
    ir2=nSecs/4+1:2*nSecs/4;
    ir3=2*nSecs/4+1:3*nSecs/4;
    ir4=3*nSecs/4+1:nSecs;
    
    i=1;
    for e=1:length(EXP),
        
        i1=2*(i-1)+1;
        i2=2*(i-1)+2;
        %only consider cropped frames
        iStrt=find(1-EXP{e}.data.EXF(max(1,DATA.P.start-EXP{e}.data.start+1):...
            min(EXP{e}.data.join-EXP{e}.data.start,DATA.P.end-EXP{e}.data.start+1)));
        
        % +++++++++++
        %   mask 1
        % +++++++++++
        CNSEW(i1)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(1);    %condition
        %check flip
        f1=0;
        D=EXP{e}.data.mAVD(1,:);
        sra=sum(D(1:(end/2)));
        srb=sum(D((end/2+1):end));
        if(sra<srb)
            %flip it
            f1=1;
        end
        
        NSEW(i1,:)=nanmean(EXP{e}.data.NSEWF(iStrt,1,:));
        if f1
            tmp=-NSEW(i1,3);
            NSEW(i1,3)=-NSEW(i1,4);
            NSEW(i1,4)=tmp;
        end
        
        
        % +++++++++++
        %   mask 2
        % +++++++++++
        CNSEW(i2)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(2);    %condition
        %check flip
        f2=0;
        D=EXP{e}.data.mAVD(2,:);
        sra=sum(D(1:(end/2)));
        srb=sum(D((end/2+1):end));
        if(sra<srb)
            %flip it
            f2=1;
        end
        
        NSEW(i2,:)=nanmean(EXP{e}.data.NSEWF(iStrt,2,:));
        if f1
            tmp=-NSEW(i2,3);
            NSEW(i2,3)=-NSEW(i2,4);
            NSEW(i2,4)=tmp;
        end
        
        % +++++++++++
        %   time binning
        % +++++++++++
        ntb=min(C.ntb,EXP{e}.data.tbins);
        NSEWT(i1,1:ntb,:)=EXP{e}.data.mNSEWT(1:ntb,1,:);
        NSEWT(i2,1:ntb,:)=EXP{e}.data.mNSEWT(1:ntb,2,:);
        if f1
            %flip
            NSEWT(i1,1:ntb,3)=-EXP{e}.data.mNSEWT(1:ntb,1,4);
            NSEWT(i1,1:ntb,4)=-EXP{e}.data.mNSEWT(1:ntb,1,3);
        end
        if f2
            NSEWT(i2,1:ntb,3)=-EXP{e}.data.mNSEWT(1:ntb,2,4);
            NSEWT(i2,1:ntb,4)=-EXP{e}.data.mNSEWT(1:ntb,2,3);
        end
        i=i+1;
    end
    
    % ++++++++++++
    % calculate mean and std
    % ++++++++++++
    mNSEW=NaN(DATA.P.NC,4);
    steNSEW=mNSEW;
    
    mNSEWT=NaN(DATA.P.NC,C.ntb,4);
    steNSEWT=mNSEWT;
    
    for c=1:DATA.P.NC
        ind=find(CNSEW==c);
        mNSEW(c,:)=nanmean(NSEW(ind,:),1);
        steNSEW(c,:)=nanstd(NSEW(ind,:),0,1)/sqrt(length(ind));
        mNSEWT(c,:,:)=squeeze(nanmean(NSEWT(ind,:,:),1));
        steNSEWT(c,:,:)=squeeze(nanstd(NSEWT(ind,:,:),0,1))/sqrt(length(ind));
    end
    
    % ++++++++++++
    %   plot overlay conditions NSEW
    % ++++++++++++
    h=figure('Name','Axial Velocity Contribution');
    for c=1:DATA.P.NC
        errorbar([mNSEW(c,1:2),0 0],[0 0 ,mNSEW(c,3:4)],steNSEW(c,1:4),'o','Color',color_my2(DATA.P.NC,1,c),'LineWidth',1.5);
        hold on
    end
    %make plot nice
    axis equal;
    axis([1.1*min(mNSEW(:,2)) 1.1*max(mNSEW(:,1)) 1.1*min(mNSEW(:,4)) 1.1*max(mNSEW(:,3))]);
    grid on;
    title('Axial Velocity Contribution');
    ylabel('speed [\mum/min]');
    xlabel('speed [\mum/min]');
    set(gca, 'Color', 'none');
    set(gcf, 'Color', 'none');
    
    outfileBase=fullfile(DATA.basePath,outfolder,'%s','NSEW.%s');
    if C.expFig
        export_fig(sprintf2(outfileBase,'images','pdf'));
    else
        saveas(h,sprintf2(outfileBase,'images','tif'));
    end
    saveas(h,sprintf2(outfileBase,'figures','fig'));
    close(h);
    
    % ++++++++++++
    %   plot evolution NSEW
    % ++++++++++++
    for c=1:DATA.P.NC
        h=figure('Name','Axial Velocity Contribution Evolution');
        for t=1:C.ntb
            errorbar([squeeze(mNSEWT(c,t,1:2))',0 0],[0 0 ,squeeze(mNSEWT(c,t,3:4))'],squeeze(steNSEWT(c,t,1:4))',...
                'o','Color',color_my3(C.ntb,1,t),'LineWidth',1.5);
            hold on;
        end
        
        %make plot nice
        axis equal;
        axis([1.1*min(min(mNSEWT(:,:,2))) 1.1*max(max(mNSEWT(:,:,1))) ...
              1.1*min(min(mNSEWT(:,:,4))) 1.1*max(max(mNSEWT(:,:,3)))]);
        grid on;
        title('Axial Velocity Contribution Evolution');
        ylabel('speed [\mum/min]');
        xlabel('speed [\mum/min]');
        set(gca, 'Color', 'none');
        set(gcf, 'Color', 'none');
        
        outfileBase=fullfile(DATA.basePath,outfolder,'%s',sprintf('TMP_NSEW_%s.%%s',DATA.P.COND{c}));
        if C.expFig
            export_fig(sprintf2(outfileBase,'images','pdf'));
        else
            saveas(h,sprintf2(outfileBase,'images','tif'));
        end
        saveas(h,sprintf2(outfileBase,'figures','fig'));
        close(h);
    end
end

%%
% |||||||||||||||||||||||
% |||||||||||||||||||||||
% ||
% ||  Angular correlation at distncas max distance
% ||
% |||||||||||||||||||||||
% |||||||||||||||||||||||
DO_ANG=1;
if DO_ANG
    
    % ++++++++++++
    %   collect data
    % ++++++++++++
    CACTS=NaN(sum(nData),1);                        % conditon
    
    ACTS=NaN(C.ndbAC,C.ntb,sum(nData));             % angular difference in space and time
    ACDFTS=ACTS;                                    % diff in angular difference to front
    ACDNTS=NaN(C.ndbAC-1,C.ntb,sum(nData));         % diff in angular difference to next bin
    
    i=1;
    for e=1:length(EXP),        
        %indices for up/down mask
        i1=2*(i-1)+1;
        i2=2*(i-1)+2;
        
        %condition
        CACTS(i1)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(1);
        CACTS(i2)=(EXP{e}.condP-1)*DATA.P.NCM+EXP{e}.condM(2);
        
        % +++++++++++
        %   time binning
        % +++++++++++
        
        ntb=min(C.ntb,EXP{e}.data.tbins);
        ndbAC=min(EXP{e}.data.dbinsAD,C.ndbAC);
        
        %angular correlation
        ACTS(1:ndbAC(1),1:ntb,i1)=1./EXP{e}.data.ADTS{1}(1:ntb,1:ndbAC(1))';
        ACTS(1:ndbAC(2),1:ntb,i2)=1./EXP{e}.data.ADTS{2}(1:ntb,1:ndbAC(2))';
        
        %Angular Correlation Difference to Front
        ACDFTS(1:ndbAC(1),1:ntb,i1)=1./EXP{e}.data.ADTS{1}(1:ntb,1:ndbAC(1))'-...
                          1./(ones(ndbAC(1),1)*EXP{e}.data.ADTS{1}(1:ntb,1)');
        ACDFTS(1:ndbAC(2),1:ntb,i2)=1./EXP{e}.data.ADTS{2}(1:ntb,1:ndbAC(2))'-...
                          1./(ones(ndbAC(2),1)*EXP{e}.data.ADTS{2}(1:ntb,1)');
        
        %Angular Correlation Difference to Last bin
        ACDNTS(1:ndbAC(1)-1,1:ntb,i1)=1./EXP{e}.data.ADTS{1}(1:ntb,1:ndbAC(1)-1)'-...
                                      1./EXP{e}.data.ADTS{1}(1:ntb,2:ndbAC(1))';
        ACDNTS(1:ndbAC(2)-1,1:ntb,i2)=1./EXP{e}.data.ADTS{2}(1:ntb,1:ndbAC(2)-1)'-...
                                      1./EXP{e}.data.ADTS{2}(1:ntb,2:ndbAC(2))';
        i=i+1;
    end
    
    %%
    % ++++++++++++
    % calculate mean and std
    % ++++++++++++
    mACTS=NaN(C.ndbAC,C.ntb,DATA.P.NC);
    steACTS=mACTS;
    
    mACDFTS=mACTS;
    steACDFTS=mACTS;
    
    mACDNTS=NaN(C.ndbAC-1,C.ntb,DATA.P.NC);
    steACDNTS=mACDNTS;
    
    for c=1:DATA.P.NC
        ind=find(CACTS==c);
        mACTS(:,:,c)=nanmean(ACTS(:,:,ind),3);
        steACTS(:,:,c)=nanstd(ACTS(:,:,ind),0,3)/sqrt(length(ind));
        
        mACDFTS(:,:,c)=nanmean(ACDFTS(:,:,ind),3);
        steACDFTS(:,:,c)=nanstd(ACDFTS(:,:,ind),0,3)/sqrt(length(ind));
        
        mACDNTS(:,:,c)=nanmean(ACDNTS(:,:,ind),3);
        steACDNTS(:,:,c)=nanstd(ACDNTS(:,:,ind),0,3)/sqrt(length(ind));
    end
    
    %%
    %to plot time
    for t=1:C.ntb
       ticklabels{t}=sprintf('%3.3f',((P_STS.tempBinSS-1)/2+(t-1)*(1-P_STS.tempBinOL)...
           *(P_STS.tempBinSS))*(P_G.dt/60));
    end
    
    %%
    %++++++++++++++++++++
    % plot correlation mean
    %   - versus time
    %   - at difference distances
    % plot per condition
    %++++++++++++++++++++
    for c=1:DATA.P.NC
        
        h=figure;
        for d=1:C.ndbAC
            ind=find(mACTS(d,:,c)==mACTS(d,:,c));
            errorbar(ind,mACTS(d,ind,c),steACTS(d,ind,c),'.-','Color',color_my3(C.ndbAC,1,d),'LineWidth',1.5);
            hold on
        end
        % plot base line
        plot(1:C.ntb,(1:C.ntb).*0+2/pi,'-.k');
        
        % make figure nice
        ind=find(mACTS);
        axis( [0.9 C.ntb+0.1 0.95*min(min(mACTS(ind)-steACTS(ind)),2/pi) 1.1*max(mACTS(ind)+steACTS(ind))])
        title(sprintf('angle correlation (at %3.2f \\mum)',P_STS.corrDist))
        set(gca,'XTick',1:C.ntb);
        set(gca,'XTickLabel',ticklabels);
        xlabel(sprintf('time +- %2.3f [h]',(P_STS.tempBinSS-1)/2*(P_G.dt/60)));
        ylabel('angle correlation [1/\Delta \theta)]');
        set(gca, 'Color', 'none');
        set(gcf, 'Color', 'none');
        
        outfileBase=fullfile(DATA.basePath,outfolder,'%s',sprintf('SPTMP_AC_%s.%%s',DATA.P.COND{c}));
        if C.expFig
            export_fig(sprintf2(outfileBase,'images','pdf'));
        else
            saveas(h,sprintf2(outfileBase,'images','tif'));
        end
        saveas(h,sprintf2(outfileBase,'figures','fig'));
        close(h);
    end
    
    %%
    %++++++++++++++++++++
    % plot correlation difference mean
    %   - versus time
    %   - at difference distances
    % plot per condition
    %++++++++++++++++++++
    for c=1:DATA.P.NC
        
        h=figure;
        for d=2:C.ndbAC
            ind=find(mACDFTS(d,:,c)==mACDFTS(d,:,c));
            if(length(ind)>0)
                errorbar(ind,mACDFTS(d,ind,c),steACDFTS(d,ind,c),'.-','Color',color_my3(C.ndbAC,1,d),'LineWidth',1.5);
                hold on
            end
        end
        % plot base line
        plot(1:C.ntb,(1:C.ntb).*0,'-.k');
        
        % make figure nice
        ind=find(mACDFTS);
        axis( [0.9 C.ntb+0.1 1.05*min(mACDFTS(ind)-steACDFTS(ind)) max(1.1*max(mACDFTS(ind)+steACTS(ind)),0.05)])
        title(sprintf('diff in angle corr. (at %3.2f \\mum) to first bin',P_STS.corrDist))
        set(gca,'XTick',1:C.ntb);
        set(gca,'XTickLabel',ticklabels);
        xlabel(sprintf('time +- %2.3f [h]',(P_STS.tempBinSS-1)/2*(P_G.dt/60)));
        ylabel('angle correlation [1/\Delta \theta)]');
        set(gca, 'Color', 'none');
        set(gcf, 'Color', 'none');
        
        outfileBase=fullfile(DATA.basePath,outfolder,'%s',sprintf('SPTMP_ACDF_%s.%%s',DATA.P.COND{c}));
        if C.expFig
            export_fig(sprintf2(outfileBase,'images','pdf'));
        else
            saveas(h,sprintf2(outfileBase,'images','tif'));
        end
        saveas(h,sprintf2(outfileBase,'figures','fig'));
        close(h);
    end
    %%
    %++++++++++++++++++++
    % plot correlation difference mean to next bin
    %   - versus time
    %   - at difference distances
    % plot per condition
    %++++++++++++++++++++
    
    for c=1:DATA.P.NC
        
        h=figure;
        for d=1:C.ndbAC-1
            ind=find(mACDNTS(d,:,c)==mACDNTS(d,:,c));
            if(length(ind)>0)
                errorbar(ind,mACDNTS(d,ind,c),steACDNTS(d,ind,c),'.-','Color',color_my3(C.ndbAC,1,d),'LineWidth',1.5);
                hold on
            end
        end
        % plot base line
        plot(1:C.ntb,(1:C.ntb).*0,'-.k');
        
        % make figure nice
        ind=find(mACDNTS);
        axis( [0.9 C.ntb+0.1 1.05*min(mACDNTS(ind)-steACDNTS(ind)) max(1.1*max(mACDNTS(ind)+steACDNTS(ind)),0.05)])
        title(sprintf('diff in angle corr. (at %3.2f \\mum) to next bin',P_STS.corrDist))
        set(gca,'XTick',1:C.ntb);
        set(gca,'XTickLabel',ticklabels);
        xlabel(sprintf('time +- %2.3f [h]',(P_STS.tempBinSS-1)/2*(P_G.dt/60)));
        ylabel('angle correlation [1/\Delta \theta)]');
        set(gca, 'Color', 'none');
        set(gcf, 'Color', 'none');
        
        outfileBase=fullfile(DATA.basePath,outfolder,'%s',sprintf('SPTMP_ACDN_%s.%%s',DATA.P.COND{c}));
        if C.expFig
            export_fig(sprintf2(outfileBase,'images','pdf'));
        else
            saveas(h,sprintf2(outfileBase,'images','tif'));
        end
        saveas(h,sprintf2(outfileBase,'figures','fig'));
        close(h);
    end
    
    
    %%
    %++++++++++++++++++++
    % ttest of angular correlation at distance d compared to front
    %   - versus time
    %   - at difference distances
    % plot per condition
    %++++++++++++++++++++
    
    %calculate significance level
    hACTS=zeros(DATA.P.NC,C.ntb,C.ndbAC-1);
    pACTS=hACTS;
    for c=1:DATA.P.NC
        ic=find(CACTS==c);
        for t=1:C.ntb
            for db=2:C.ndbAC
                dc1=squeeze(ACTS(1,t,ic));
                dc1=dc1(find(dc1));
                dc2=squeeze(ACTS(db,t,ic));
                dc2=dc2(find(dc2));
                [hACTS(c,t,db),pACTS(c,t,db)]=ttest2(dc1,dc2);
            end
        end
    end
    hACTS(find(hACTS~=hACTS))=0;
    pACTS(find(pACTS~=pACTS))=1;
    
    %plot this
    for c=1:DATA.P.NC
        h=figure;
        I=squeeze(pACTS(c,:,2:end));
        rgb=zeros(size(I,2),size(I,1),3);
        indC=find(I<=0.05);
        indNC=find(I>0.05);
        mC=zeros(size(I));
        mNC=zeros(size(I));
        mC(indC)=1;
        mNC(indNC)=1;
        rgb(:,:,1)=min(max(mC*0.6     + mNC.*(1-(I-0.05/0.95)) ,0),1)';
        rgb(:,:,2)=min(max(mC.*I/0.01     + mNC.*(1-(I-0.05/0.95)) ,0),1)';
        rgb(:,:,3)=min(max(mC.*(1-(I/0.01))      + mNC.*(1-(I-0.05/0.95)) ,0),1)';
        rgb=imresize(rgb,30,'nearest');
        imshow(rgb);
        outfile=fullfile(DATA.basePath,outfolder,'images',sprintf('SPTMP_SIGP_ACDF_%s.tif',DATA.P.COND{c}));
        imwrite(rgb,outfile);
        close(h);
    end
    %%
    %plot colormap
    h=figure;
    I=(0:0.001:1)'*ones(1,30);
    indC=find(I<=0.05);
    mC=zeros(size(I),30);
    mC(indC)=1;
    indNC=find(I>0.05);
    mNC=zeros(size(I),30);
    mNC(indNC)=1;
    rgb=zeros(size(mNC,1),size(mNC,2),3);
    rgb(:,:,1)=min(max(mC*0.6     + mNC.*(1-(I-0.05/0.95)) ,0),1);
    rgb(:,:,2)=min(max(mC.*I/0.01     + mNC.*(1-(I-0.05/0.95)) ,0),1);
    rgb(:,:,3)=min(max(mC.*(1-(I/0.01))      + mNC.*(1-(I-0.05/0.95)) ,0),1);
    imshow(rgb);
    outfile=fullfile(DATA.basePath,outfolder,'images',sprintf('SIGP_COLORMAP.tif'));
    imwrite(rgb,outfile);
    close(h);
end


% ++++++++++++++++++
% STORE THINGS
% ++++++++++++++++++
save(fullfile(DATA.basePath,outfolder,'data','CIV_statsCollapsed.mat'),...
    'mWCS','steWCS','pWCS',...
    'mLSO','steLSO','pLSO',...
    'mAVD','steAVD','mAVDT',...
    'mNSEW','steNSEW','mNSEWT','steNSEWT',...
    'mACTS','steACTS','mACDFTS','steACDFTS','mACDNTS','steACDNTS',...
    'hACTS','pACTS',...
    'C','DATA','P_G','P_STS');
