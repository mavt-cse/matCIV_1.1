function R = img_distbin(A,MSK,dbs,dbol,MSK2)
% do distance based binning of quantity A
% A = quantity to bin over
% MSK = mask
% bs = bin size
% bol = bin overlap
% calculate for regions with respect to interface
% MSK2 = second mask to refine components to consider
%
% R mean value per bin
%
% NOTE: 
% - correlations are calculated inside half circle away from the mask front
% - correlations are calculated up to mcl, extending over the bin borders
% - last bin center is at least mcl+bs/2 away from end of domain
% + best if bs*(1-bol) is integer

[As1,As2]=size(A);


%if not confluent
if(min(MSK(:))==0)
    %distance to front
    D1=bwdist(1-MSK);
    
    %mxdist: furthest point from mask front to be considered
    % find far-end boundary of mask
    s1=sum(MSK,1)/As1;
    s2=sum(MSK,2)/As2;
    [tmp,c]=max([s2(1),s2(end),s1(1),s1(end)]);
    mxdist=0;
    switch c
        case 1
            %NORTH
            %mxdist=min(DIST(1,:));
            mxdist=mean(D1(1,:));
        case 2
            %SOUTH
            %mxdist=min(DIST(end,:));
            mxdist=mean(D1(end,:));
        case 3
            %EAST
            %mxdist=min(DIST(:,1));
            mxdist=mean(D1(:,1));
        case 4
            %WEST
            %mxdist=min(DIST(:,end));
            mxdist=mean(D1(:,end));
    end
    %number of distance bins
    dbinN=floor((mxdist-dbs)/(dbs*(1-dbol)))+1;
    dbinL=(0:dbinN-1)*dbs*(1-dbol);
    dbinH=dbinL+dbs;
else
    dbinN=1;
    dbinL=0;
    dbinH=1;
    D1=zeros(As1,As2);
end

R=zeros(1,dbinN);
%TEST=zeros(As1,As2);
if nargin < 5
    for db=1:dbinN;
        indDB=intersect(find(D1>dbinL(db)),find(D1<=dbinH(db)));
        R(db)=mean(A(indDB));
        %TEST(indDB)=db;
    end
else
    for db=1:dbinN;
        indDB=intersect(intersect(find(D1>dbinL(db)),find(D1<=dbinH(db))),find(MSK2));
        R(db)=mean(A(indDB));
        %TEST(indDB)=db;
    end
end