function R=color_my3(maxv,minv,val)
% purple - yellow
rv=max(maxv-minv,1);
v=val-minv;
if size(val,1)==1
    R=[0.6,v/rv,1-v/rv];
elseif size(val,2)>1
  R=zeros([size(val),3]);
  R(:,:,1)=0.6;
  R(:,:,2)=v/rv;
  R(:,:,3)=1-v/rv;
end
return