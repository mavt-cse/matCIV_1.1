function DATA=EXTCIV_prepareBatchDataTMPLT

%++++++++++++++++++++++++++++++++++++++++
% Script to run prepare the DATA variable passed to the
%   EXTCIV_batchProcessing(DATA,DO_PIV,DO_MASK)
%
% Copy and modif this script to prepare your own data for batch processing
%
% PRECONDITION ALL
% - see conditions for EXTCIV_batchProcessing.m
% - see conditions for EXTCIV_collapseBatchStatistics.m
%
% PARAMETERS TO ADJUST:
%   P.start: first sequence file of each experiment to consider
%   P.end:   lates sequence file of each experiment to consider
%           
%   P.ndb:   maximal number of distance bins to consider
%
%   P.ndbAC: maximal number of distance bins considered in the spatial
%            angular correlation analysis
%
%   P.CONDP: labels of considered experimental conditions per position
%
%   P.CONDM: labels of considered experimental conditions per mask
%            - used if different conditions hold for the 2 masks in
%              one image  (e.g. flow)
%
%++++++++++++++++++++++++++++++++++++++++

%++++++++++++++++++++++++++++++++++++++++
%++++ DEFINE STATISTICAL ANALYSE PARAMETER ++++%

%CROPPING (stats-start to stats-end)
% ADJUST THE FOLLOWING 2 LINES
P.start=1;
P.end=8;

%Maximal distance bins
% ADJUST THE FOLLOWING 2 LINES
P.ndb=10;
P.ndbAC=10;

%CONDITIONS PER POSITION
% ADJUST THE FOLLOWING LINE!!
P.CONDP={'CP1','CP2'};  %conditions per position
P.NCP=length(P.CONDP);  %number of position conditions
% for easier assignment
for p=1:P.NCP;
    expr=sprintf('CONDP%d=%d;',p,p);
    eval(expr);
end

%CONDITIONS PER MASK
% ADJUST THE FOLLOWING LINE!!
%P.CONDM={'UP','DOWN'};  %conditons per mask
P.CONDM={'M'};
P.NCM=length(P.CONDM);  %number of mask conditions
for m=1:P.NCM
    expr=sprintf('CONDM%d=%d;',m,m);
    eval(expr);
end

%TOTAL CONDITIONS
P.NC=P.NCP*P.NCM;
for c1=1:P.NCP
    for c2=1:P.NCM
        P.COND{(c1-1)*P.NCM+c2}=strcat(P.CONDP{c1},P.CONDM{c2});
    end
end

DATA.P=P;


%++++++++++++++++++++++++++++++++++++++++
%++++ DEFINE EXPERIMENTS TO ANALYSE ++++%
%++++++++++++++++++++++++++++++++++++++++
% SET FOLDER PATHS
% - assumed folder layout:
%   - folderpath/experiment/position
% - e.g.
%   -/home/user/experiments/exp_11_11_4/xy1c1
%   -/home/user/experiments/exp_11_11_4/xy2c1
%   -/home/user/experiments/exp_11_11_21/xy1c1
%   -/home/user/experiments/exp_11_11_21/xy2c1
%   -/home/user/experiments/exp_11_11_21/xy3c1
%   ====== CODE ==========
%   DATA.folderpath: '/home/user/experiments/'
%   clear exp;
%   exp.folder='exp_11_11_4';
%   exp.pos={'xy1c1','xy2c1'};
%   exp.condP{CONDP1,CONDP1};
%   exp.condM={[CONDM1,CONDM2],[CONDM1,CONDM2]}
%   DATA.exp{1}=exp;
%   clear exp;
%   exp.folder='exp_11_11_21';
%   exp.pos: {'xy1c1','xy2c1','xy3c1'};
%   exp.condP{CONDP2,CONDP2,CONDP2};
%   exp.condM={[CONDM1,CONDM2],[CONDM1,CONDM2],[CONDM1,CONDM2]}
%   DATA.exp{2}=exp;
%   ======================
%
%  the example assumes:
%   - experiments in folder exp_11_11_4 are of condition CONDP1
%   - experiments in folder exp_11_11_21 are of condition CONDP2
%   - for all experiments the 1st mask is of conditon CONDM1
%   - for all experiments the 2nd mask is of conditon CONDM2
%++++++++++++++++++++++++++++++++++++++++

%example for test data set
%path to folder containing experiment
%DATA.basePath='/path/to/testdata/TESTDATA';
DATA.basePath='/Users/mildef/phd/projects/woundhealing/imageanalysis/mat-CIV/TESTDATA';
% 1st experiment
clear exp;
exp.folder='EXP1';
exp.pos={'pos1','pos2'};
exp.condP={CONDP1,CONDP1};
exp.condM={[CONDM1,CONDM1],[CONDM1,CONDM1]};
DATA.EXP{1}=exp;
% 2nd experiment
clear exp;
exp.folder='EXP2';
exp.pos={'pos3','pos4'};
exp.condP={CONDP2,CONDP2};
exp.condM={[CONDM1,CONDM1],[CONDM1,CONDM1]};
DATA.EXP{2}=exp;
% ... experiments
%++++ DONE DEFINING EXPERIMENTS     ++++%
%++++++++++++++++++++++++++++++++++++++++