function h =CIV_stepMask(hObject, eventdata, h)

% SCRATCH2L_STEPMASK
%   mask the current frame
%
%
% todo: 
%   - decide which method
%       - intensity based
%       - doppler based
%   ...

%prepare things
PM=h.params.mask;

%check if current frame setup for masking
if ~h.data.mask.ready
    h.data.mask.I = h.data.general.I_orig;
    h.data.mask.hst = imhist(h.data.general.I_orig);
    h.data.mask.ready=1;
end

%load prev frame
if(PM.usePrev && h.data.mask.initMaskSet)
    h.prevdata.mask.done=1;
    h.prevdata.piv.shift=0;
    h.prevdata.mask.M=h.data.mask.IM;
    h.prevdata_ready=1;
elseif (PM.usePrev && ~h.prevdata_ready && h.curFrame ~= h.batchInfo.minFrame),
    h.prevdata=CIV_loadData(h.curFrame-1,h.batchInfo,h.params);
    h.prevdata_ready=1;
end

%

%load next frame
if ~h.nextdata_ready,
    h.nextdata=CIV_loadData(h.curFrame+1,h.batchInfo,h.params);
    h.nextdata_ready=1;
end


us=0;
vs=0;
shift=0;
shiftFrac=1;
switch(h.params.mask.mod)
    case 0 % difference based

        I_this=h.data.general.I_orig;
        % equilibrate histogram of next frame to match current
        I_prev=histeq(h.prevdata.general.I_orig,h.data.mask.hst);
        if(PM.corrShift && h.prevdata.piv.shift)
            bs=30;
            [s1 s2]=size(I_this);
            I_prev_tmp=circshift(padarray(I_prev,[bs bs],'replicate'), ...
                    [h.prevdata.piv.vs, h.prevdata.piv.us]);
            I_prev=I_prev_tmp(bs+1:s1+bs,bs+1:s2+bs);
        end

        I_next=histeq(h.nextdata.general.I_orig,h.data.mask.hst);
        
        if(h.curFrame == h.batchInfo.minFrame || h.prevdata.general.exclude)
            I_prev=I_next;
            if(PM.corrShift && shift)
                bs=30;
                [s1 s2]=size(I_this);
                I_prev_tmp=circshift(padarray(I_prev,[bs bs],'replicate'), ...
                    [-h.prevdata.mask.vs, -h.prevdata.mask.us]);
                I_prev=I_prev_tmp(bs+1:s1+bs,bs+1:s2+bs);
            end
        end
        
        % filter 
        if(PM.useFilter1)
            I_this=imfilter(I_this,PM.f1);
            I_prev=imfilter(I_prev,PM.f1);
        end

        I_diff=img_rescale(max(I_prev,I_this)-I_prev,PM.D_th1);
        %I_diff=max(I_prev,I_this)-I_prev;
        % filter
        if(PM.useFilter2)
            I_diff=imfilter(I_diff,PM.f2);
        end
        
        % threshold difference image
        I_tmp1=img_threshold(I_diff,PM.D_th2);

        % close image (to connect close components)
        I_tmp2=imclose(I_tmp1,strel('disk',PM.D_cl1));

        % remove small components
        cc = bwconncomp(I_tmp2);
        stats=regionprops(cc, 'Area');
        idx = find([stats.Area] > PM.D_a);
        I_tmp3=ismember(labelmatrix(cc),idx);

        % close and dilate mask
        I_m = imdilate(imclose(I_tmp3,strel('disk',PM.D_cl2)), ...
            strel('disk',PM.D_dil));        
        
        I_m_shift=I_m;

        if (h.prevdata.mask.done && PM.usePrev),
            % correct shift
            i1=[];
            i3=[];
            I_m_prev=h.prevdata.mask.M;
            
            if(h.prevdata.piv.shift)
                % shift detected in previous frame
                % shift the previous mask with detected shift
                bs=30;
                [s1 s2]=size(I_m);
                I_m_tmp=circshift(padarray(I_m_prev,[bs bs],'replicate'), ...
                    [h.prevdata.piv.vs, h.prevdata.piv.us]);
                I_m_prev=I_m_tmp(bs+1:s1+bs,bs+1:s2+bs);
            end
            
            % calculate distances between prev and cur mask
            dist=bwdist(I_m_prev);
            dist1=dist.*I_m;
            dist2=bwdist(-I_m_prev+1);
            
            % apply different thresholds if shift detected
            % -> more conservative
            fac=1;
            if(h.prevdata.piv.shift)
                fac=h.params.mask.frac;
            end
            if fac
                i1=find(dist1>PM.d1*fac);
                i3=find(dist2>PM.d3*fac);
                I_m(i1)=0;
                I_m(i3)=1;
            else
                I_m=h.prevdata.mask.M;
            end
        end
        %to use without initialization
        % idea: if first frame and no initial mask around
        %   - only keep large regions (>1/10 domain)
        if(h.curFrame == h.batchInfo.minFrame & ~h.data.mask.initMaskSet)
            % remove small components
            cc = bwconncomp(I_m);
            stats=regionprops(cc, 'Area');
            idx = find([stats.Area] > length(I_m(:))/10);
            I_m=ismember(labelmatrix(cc),idx);
        end
        
        % store masks
        h.data.mask.I=I_diff;
        h.data.mask.M=I_m;
        h.data.mask.MS=max(I_m_shift-I_m,0);
        h.data.mask.T1=I_tmp1;
        h.data.mask.T2=I_tmp2;
        h.data.mask.T3=I_tmp3;
        h.data.mask.us=us;
        h.data.mask.vs=vs;
        h.data.mask.shift=shift;
        h.data.mask.done=1;
        
    case 1 % intensity based
         disp('Intensity based masking not supported');
            
                    
    otherwise % nothing
        disp('No masking method selected');
        
end