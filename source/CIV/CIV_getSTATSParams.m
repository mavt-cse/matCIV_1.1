function params = CIV_getSTATSParams(folder)

% SCRATCH2L_GETDEFAULTPARAMS:
%   gets default parameters for SCRATCH2L
%
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS:
%   looks for CIV_params.mat in current directory
%   if no CIV_params.mat is found, uses default params
%   
%   PARAMS=SCRATCH2L_GETDEFAULTPARAMS(DEFAULT):
%   with DEFAULT=1 forces use of default parameters

if nargin == 1
    try
        P = load(fullfile(folder,'CIV_paramsSTATS.mat'));
    catch ME, % catch and ignore all errors
    end
    if exist('P','var') && isfield(P,'corrDist') %always latest added field
        params=P;
        return
    end
end

% STATS parameters
params = struct(...
    'sectors',32, ...       % angular bins
    'space',1, ...          % do spatial analysis
    'distBinSS',40, ...     % distance bin size
    'distBinOL',0.5, ...   % distance bin overlap
    'corrDist',150, ...     % correlation distance
    'tempBinSS',6, ...      % temporal bin size
    'tempBinOL',0.5, ...    % temporal bin overlap
    'confReg',1, ...        % confine region where statistics are taken
    'regExt',250, ...       % region distance to front
    'stat',1);