function h =CIV_stepPiv(hObject, eventdata, h)

% SCRATCH2L_STEPPIV
%   conduct one step of piv analysis
%   if shift detected:
%       - find shift
%       - shift curframe
%       - redo piv
%
%
% todo: 
%   - load next frame
%   - do piv for current frame

%check if current frame setup for piv
%if ~h.data.piv.ready
   h.data.piv.I=imadjust(h.data.general.I_orig);
   if(h.params.piv.lnf)
       if (~h.data.general.lnf_done || 1)
           h.data.general.I_lnf=imadjust(img_LNF(h.data.general.I_orig,h.params.piv.lnf_winsize,3));
           h.data.general.lnf_done=1;
       end
       h.data.piv.I=h.data.general.I_lnf;
   elseif(h.params.piv.crvlt)
       bufferSize=round(min(size(h.data.general.I_orig))*0.1);
       if ~h.data.general.crvlt_transform
           disp('crvlt transform...');
           h.data.general.C_crvlt=crvlt_transform(h.data.general.I_orig,bufferSize);
           h.data.general.crvlt_transform=1;
       end
       if (~h.data.general.crvlt_done || 1)
           disp('crvlt filter...');
           h.data.general.I_crvlt=imadjust(crvlt_filter(h.data.general.C_crvlt,...
               h.params.piv.crvlt_pctg/100,h.params.piv.crvlt_levels,h.params.piv.crvlt_abs,bufferSize));
       end
       h.data.general.crvlt_done=1;
       h.data.piv.I=h.data.general.I_crvlt;
   end
   h.data.piv.ready=1;
%end

% load next frame, if necessary
if ~h.nextdata_ready
    h.nextdata=CIV_loadData(h.curFrame+1,h.batchInfo,h.params);
    h.nextdata_ready=1;
end

% prepare for piv
%if ~h.nextdata.piv.ready
    h.nextdata.piv.I=imadjust(h.nextdata.general.I_orig);
    if(h.params.piv.lnf)
        if (~h.nextdata.general.lnf_done || 1)
            h.nextdata.general.I_lnf=imadjust(img_LNF(h.nextdata.general.I_orig,h.params.piv.lnf_winsize,3));
            h.nextdata.general.lnf_done=1;
        end
        h.nextdata.piv.I=h.nextdata.general.I_lnf;
    elseif(h.params.piv.crvlt)
       if ~h.nextdata.general.crvlt_transform
           disp('crvlt transform...');
           h.nextdata.general.C_crvlt=crvlt_transform(h.nextdata.general.I_orig,bufferSize);
           h.nextdata.general.crvlt_transform=1;
       end
       if (~h.nextdata.general.crvlt_done || 1)
           disp('crvlt filter...');
           h.nextdata.general.I_crvlt=imadjust(crvlt_filter(h.nextdata.general.C_crvlt,...
               h.params.piv.crvlt_pctg/100,h.params.piv.crvlt_levels,h.params.piv.crvlt_abs,bufferSize));
       end
       h.nextdata.general.crvlt_done=1;
       h.nextdata.piv.I=h.nextdata.general.I_crvlt;
   end
    
    h.nextdata.piv.ready=1;
%end

if(h.params.piv.units==0)
    dt=handles.general.dt/(handles.general.resolution*handles.general.scaleFactor);
else
    dt=1;
end
if(strcmp(h.params.piv.method,'single') || ...
   strcmp(h.params.piv.method,'mqd'))
    winsize=min(h.params.piv.winsize(:));
else
    winsize=h.params.piv.winsize;
end


[xi,yi,iu,iv,snr,pkh]=matpiv(h.data.piv.I,h.nextdata.piv.I,...
    winsize,dt,h.params.piv.overlap,h.params.piv.method,h.params.piv.comap);

%[u,v]=naninterp_mod(iu,iv,'linear',[],xi,yi);

%hst=imhist(h.data.general.I_orig);
 
%[shift us vs]=CIV_detectShift_old2(u,v,h.data.general.I_orig,h.nextdata.general.I_orig);
 
%[lu,lv]=localfilt(xi,yi,u-us,v-vs,2,'median',5);
%[lu,lv]=naninterp_mod(lu,lv,'linear',[],xi,yi);

h.data.piv.xi=xi;
h.data.piv.yi=yi;
h.data.piv.u=iu;
h.data.piv.v=iv;
h.data.piv.snr=snr;
h.data.piv.pkh=pkh;
%h.data.piv.lu=lu;
%h.data.piv.lv=lv;
%h.data.piv.shift=shift;
%h.data.piv.shiftDone=1;
%h.data.piv.us=us;
%h.data.piv.vs=vs;
h.data.piv.done=1;