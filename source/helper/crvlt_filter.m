function I2=crvlt_filter(C,pctg,keepLevels,takeabs,bufferSize)
% Get threshold value
cfs =[];
for s=1:length(C)
  for w=1:length(C{s})
    cfs = [cfs; abs(C{s}{w}(:))];
  end
end
cfs = sort(cfs); cfs = cfs(end:-1:1);
nb = round(pctg*length(cfs));
cutoff = cfs(nb);

% Set small coefficients to zero
send = length(C);
for s=1:send
    if find(keepLevels==s)
        for w=1:length(C{s})
            value = abs(C{s}{w});
            C{s}{w} = C{s}{w} .* (value>cutoff);
        end
    else
        for w=1:length(C{s});
            C{s}{w} = C{s}{w} .* 0.0;
        end
    end 
        
end

if takeabs
    I2=img_removebuff(abs(ifdct_wrapping(C,1)),bufferSize);
else
    I2=img_removebuff(real(ifdct_wrapping(C,1)),bufferSize);
end