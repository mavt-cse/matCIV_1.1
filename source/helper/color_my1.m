function R=color_my1(maxv,minv,val)
rv=max(maxv-minv,1);
v=val-minv;
R=[v/rv,1-v/rv,0.6+abs(2*v/rv-1)*0.4];
return