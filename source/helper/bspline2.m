function y=bspline2(x)
%calculates bspline interpolation
%b spline centered at 0 with support -1.5:1.5?
y=zeros(size(x));
int1=find(abs(x)<=1/2);
int2=find(abs(x)>1/2 & abs(x)<3/2);
y(int1)=-x(int1).^2+0.75;
y(int2)=0.5*(abs(x(int2))-1.5).^2;
