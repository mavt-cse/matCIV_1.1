function varargout = CIV(varargin)
% CIV MATLAB code for CIV.fig
%      CIV, by itself, creates a new CIV or raises the existing
%      singleton*.
%
%      H = CIV returns the handle to a new CIV or the handle to
%      the existing singleton*.
%
%      CIV('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CIV.M with the given input arguments.
%
%      CIV('Property','Value',...) creates a new CIV or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CIV_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CIV_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CIV

% Last Modified by GUIDE v2.5 13-Apr-2012 11:22:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CIV_OpeningFcn, ...
                   'gui_OutputFcn',  @CIV_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| GUI CREATION/DELETION ||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes just before CIV is made visible.
function CIV_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CIV (see VARARGIN)

%setpath
CIV_setpath;

% init view settings
handles.viewI=0;
handles.maxViewI=3;
handles.viewL=0;
handles.maxViewL=9;
handles.axisImg=[];
% init folder params
batchInfo=struct('folderpath','',...
    'datapath','',...
    'statspath','',...
    'pattern','',...
    'ext','',...
    'regexp','',...
    'minFrame',0,...
    'maxFrame',1,...
    'sX',1024,...
    'sY',1024);
handles.batchInfo=batchInfo;

handles.Z=img_rescale(imresize(imread(fullfile('source','images','bee.tif')),[1024 1024]));

% load parameters
handles.params=CIV_getDefaultParams();

%init data gerneral
sXscl=ceil(batchInfo.sX*handles.params.general.scaleFactor);
sYscl=ceil(batchInfo.sY*handles.params.general.scaleFactor);
data.general.lnf_done=0;
data.general.crvlt_transform=0;
data.general.crvlt_filter=0;
data.general.crvlt_done=0;
data.general.I_orig=img_rescale(imresize(handles.Z,[sYscl,sXscl]));
data.general.I_lnf=data.general.I_orig;
data.general.I_crvlt=data.general.I_orig;
data.general.C_crvlt=[];
data.general.exclude=0;
data.general.shift.ready=0;
data.general.shift.done=0;
data.general.shift.detected=0;
data.general.shift.us=0;
data.general.shift.vs=0;
%init data piv
data.piv.ready=0;
data.piv.done=0;
data.piv.u=[];
data.piv.v=data.piv.u;
data.piv.lu=data.piv.u;
data.piv.lv=data.piv.u;
data.piv.snr=0;
data.piv.pkh=0;
data.piv.xi=[];
data.piv.yi=[];
data.piv.us=0;
data.piv.uv=0;
data.piv.shift=0;
data.piv.shiftDone=0;
%init data mask
data.mask.ready=0;
data.mask.done=0;
data.mask.initMaskSet=0;
data.mask.hst=[];
data.mask.us=0;
data.mask.vs=0;
data.mask.I=data.general.I_orig;
data.mask.M=data.mask.I;
data.mask.MS=data.mask.I;
data.mask.T1=data.mask.I;
data.mask.T2=data.mask.I;
data.mask.T3=data.mask.I;
data.mask.IM=data.mask.I;
%stats
data.stats.avg=[];
data.stats.start=0;
data.stats.join=0;
data.stats.stop=0;
data.stats.o1alpha=0;
data.stats.o1rho=0;
data.stats.o2alpha=0;
data.stats.o2alpha=0;
%track
data.track=[];

%next/prev
handles.data=data;
handles.nextdata=data;
handles.nextdata_ready=0;
handles.prevdata=data;
handles.prevdata_ready=0;
    
% init things
handles.tabs=[handles.PB_layers handles.UIP_layers; ...
    handles.PB_cells handles.UIP_cells];
handles.curTab=1;
PB_tab_Callback(handles.PB_layers,[],handles);
initStat(hObject,eventdata,handles);

%interrupts
handles.stoprun=0;

%init handle to axis image
handles.show.img=[];
handles.show.imgRGB=[];
[handles.axisImg handles.hQuiver]=resetAxis(hObject,eventdata,handles,0);
%showAxis(hObject, eventdata, handles);

% Choose default command line output for CIV
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CIV wait for user response (see UIRESUME)
% uiwait(handles.CIV);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Outputs from this function are returned to the command line.
function varargout = CIV_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| MENU CALLBACKS |||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --------------------------------------------------------------------
function menu_file_Callback(hObject, eventdata, handles)
% hObject    handle to menu_file (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --------------------------------------------------------------------
function menu_settings_Callback(hObject, eventdata, handles)
% hObject    handle to menu_settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++
% open dialog to adjust settings
%+++
CIV_settings(@update_params,@store_params,@delete_data,handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function handles=update_params(handles,params)

pthGEN=fullfile(handles.batchInfo.folderpath,handles.params.general.folder);
pthPIV=fullfile(pthGEN,handles.params.general.folderPIV);
pthMASK=fullfile(pthGEN,handles.params.general.folderMASK);
% update the parameters
% update axis if necessary
if(handles.params.piv.lnf ~= params.piv.lnf | ...
   handles.params.piv.lnf_winsize ~= params.piv.lnf_winsize | ...
   ~strcmp(mat2str(handles.params.piv.winsize),mat2str(params.piv.winsize)) |...
   handles.params.piv.overlap ~= params.piv.overlap |...
   ~strcmp(handles.params.piv.method,params.piv.method) |...
   handles.params.piv.crvlt~= params.piv.crvlt |...
   handles.params.piv.crvlt_pctg ~= params.piv.crvlt_pctg |...
   isequal(handles.params.piv.crvlt_levels,params.piv.crvlt_levels) |...
   handles.params.piv.crvlt_abs ~= params.piv.crvlt_abs)
    handles.data.piv.done=0;
    handles.data.piv.ready=0;handles.cRes=1;
    handles.nextdata.piv.ready=0;
    handles.data.general.lnf_done=0;
    handles.nextdata.general.lnf_done=0;
end
if(handles.params.general.scaleFactor ~= params.general.scaleFactor) 
     handles.params=params;
%     delete(fullfile(pthGEN,'*-GEN.mat'));
%     delete(fullfile(pthMASK,'*-MASK.mat'));
%     delete(fullfile(pthPIV,'*-PIV.mat'));
%     
     handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
     [handles.axisImg handles.hQuiver]=resetAxis(handles.CIV, [],handles,0);
     [handles.show.imgI handles.show.imgRGB]=showAxis(handles.CIV, [], handles);
     showPiv(handles.CIV, [], handles);
 
 else
    handles.params=params;
end

%update curFrame
if handles.curFrame ~= str2num(get(handles.T_curFrame,'String'))
    handles.curFrame=str2num(get(handles.T_curFrame,'String'));
    handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
    handles.prevdata_ready=0;
    handles.nextdata_ready=0;
end
guidata(handles.CIV, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function store_params(handles)
% store the parameters to file
% general
pthGEN=fullfile(handles.batchInfo.folderpath,handles.params.general.folder);
pthPIV=fullfile(pthGEN,handles.params.general.folderPIV);
pthMASK=fullfile(pthGEN,handles.params.general.folderMASK);
pGEN=handles.params.general;
pPIV=handles.params.piv;
pMASK=handles.params.mask;
pSTATS=handles.params.stats;
%GENERAL
if ~exist(pthGEN,'file')
    mkdir(pthGEN);
end
save(fullfile(pthGEN,'CIV_params.mat'),'-struct','pGEN');
% PIV
if ~exist(pthPIV,'file')
    mkdir(pthPIV);
end
save(fullfile(pthPIV,'CIV_paramsPIV.mat'),'-struct','pPIV');
% MASK
if ~exist(pthMASK,'file')
    mkdir(pthMASK);
end
save(fullfile(pthMASK,'CIV_paramsMASK.mat'),'-struct','pMASK');
% STATS
save(fullfile(pthGEN,'CIV_paramsSTATS.mat'),'-struct','pSTATS');

guidata(handles.CIV,handles);


% --- Executes on button press in PB_clearAll.
function ans=delete_data(handles, params)
% hObject    handle to PB_clearAllMask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++
% delete stored data
%+++++
pthGEN=fullfile(handles.batchInfo.folderpath,params.general.folder);
pthMASK=fullfile(pthGEN,params.general.folderMASK);
pthPIV=fullfile(pthGEN,params.general.folderPIV);
ans=questdlg('Do you really want to delete all data','WARNING','ok','cancel','ok');
if(strcmp(ans,'ok'))
    delete(fullfile(pthGEN,'*-GEN.mat'));
    delete(fullfile(pthMASK,'*-MASK.mat'));
    delete(fullfile(pthPIV,'*-PIV.mat'));
end


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| LOAD / STORE |||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_load.
function PB_load_Callback(hObject, eventdata, handles)
% hObject    handle to PB_load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++++++++++
% - load data:
%   - per folder:
%       - load parameters
%       - load filename pattern
%   - load current image
%+++++++++++++
% store current dat
if(get(handles.CB_store,'Value'))
    set(handles.T_processing,'String','storing data..');
    set(handles.T_processing,'Visible','on');
    drawnow();
    CIV_storeData(handles);
end
% update processing string
set(handles.T_processing,'String','loading data..');
set(handles.T_processing,'Visible','on');
drawnow();
% 
enterpath=pwd;
if(~isempty(handles.batchInfo.folderpath))
    enterpath=handles.batchInfo.folderpath;
end
tag=get(hObject,'Tag');
keepFocus=1;
switch tag
    case 'PB_load'
        % open GUI to select file
        [filename, folderpath]=uigetfile({'*.tif' 'tif Files (*.tif)'}, 'Open image', enterpath);
        if(~filename)
            set(handles.T_processing,'Visible','off');
            return;
        end
        [ok, batchInfo, params, curFrame] = CIV_loadBatchInfo(folderpath, filename, handles.batchInfo, handles.params);
        if(ok)
            if(~isequal(handles.batchInfo.folderpath,batchInfo.folderpath))
                % if new folder, store folder info, load params
            	handles.batchInfo=batchInfo;
                handles.params=params;
                keepFocus=0;
                % display folder
                set(handles.T_data,'string',sprintf2(fullfile(handles.batchInfo.folderpath,handles.batchInfo.pattern),0,'','.tif'));
                % reset data.show stuff
               % sYscl=ceil(handles.batchInfo.sY*params.general.scaleFactor);
               % sXscl=ceil(handles.batchInfo.sX*params.general.scaleFactor);
               % handles.show.img=imresize(handles.Z,[sYscl sXscl]);
               % handles.show.imgRGB=zeros(sYscl,sXscl,3);
               % handles.show.imgRGB(:,:,1)=handles.show.img;
                %reset Statistics controls
                initStat(hObject,eventdata, handles);
                [handles.axisImg handles.hQuiver]=resetAxis(hObject, eventdata,handles,keepFocus);
            end
           handles.curFrame=curFrame;
        end
        handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
        handles.prevdata_ready=0;
        handles.prevdata=handles.data;
        handles.nextdata_ready=0;
        handels.nextdata=handles.data;
    case 'PB_prev'
        handles.curFrame=handles.curFrame-1;
        handles.nextdata=handles.data;
        handles.nextdata_ready=1;
        if(handles.prevdata_ready)
            handles.data=handles.prevdata;
        else
            handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
        end
        handles.prevdata_ready=0;
    case 'PB_pprev'
        handles.curFrame=handles.curFrame-5;
        handles.nextdata_ready=0;
        handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
        handles.prevdata_ready=0;
    case 'PB_next'
        handles.curFrame=handles.curFrame+1;
        handles.prevdata=handles.data;
        handles.prevdata_ready=1;
        if(handles.nextdata_ready)
            handles.data=handles.nextdata;
        else
            handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
        end
        handles.nextdata_ready=0;
  case 'PB_nnext'
        handles.curFrame=handles.curFrame+5;
        handles.prevdata_ready=0;
        handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
        handles.nextdata_ready=0;
end
%[handles.axisImg handles.hQuiver]=resetAxis(hObject, eventdata,handles,keepFocus);
[handles.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata, handles);
showPiv(hObject, eventdata, handles);
%finalize
updatePannel(hObject,handles);
updateMessages(hObject,handles)
guidata(hObject, handles);
% update processing string
set(handles.T_processing,'Visible','off');
drawnow();


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_layers.
function PB_layers_Callback(hObject, eventdata, handles)
% hObject    handle to PB_layers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| DATA PROCESSING ||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_run.
function PB_run_Callback(hObject, eventdata, handles)
% hObject    handle to PB_run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.PB_stop,'Enable','on');
set(handles.PB_stop,'UserData',0);

while(handles.curFrame < handles.batchInfo.maxFrame)
    handles=PB_step_Callback(hObject, eventdata, handles);
    [handles.show.imgI handles.show.imgRGB]=showAxis(hObject,eventdata,handles);
    updatePannel(hObject,handles);
    
    if(get(handles.CB_store,'Value'))
        set(handles.T_processing,'String','storing data..');
        set(handles.T_processing,'Visible','on');
        drawnow();
        CIV_storeData(handles);
        set(handles.T_processing,'Visible','off');
    end
    % check for interrupt
    if(get(handles.PB_stop,'UserData'))
        set(handles.PB_stop,'Enable','off');
        return;
    end
    %advance one frame
    handles.prevdata=handles.data;
    handles.prevdata_ready=1;
    if(handles.nextdata_ready)
        handles.data=handles.nextdata;
    else
        handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
    end
    handles.nextdata_ready=0;
    handles.curFrame=handles.curFrame+1;
    %updatePannel(hObject,handles);
    
    %guidata(hObject, handles)
end
set(handles.PB_stop,'Enable','off');



%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_step.
function handles=PB_step_Callback(hObject, eventdata, handles)
% hObject    handle to PB_step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%++++++++++++++++
% - check which tab active
%   - check which boxes are checked
%   - do one step
%++++++++++++++++
if(handles.curTab==1)
    % cell layer analysis active

    if(get(handles.CB_piv,'Value'))
        % do piv analysis
        set(handles.T_processing,'String','processing PIV..');
        set(handles.T_processing,'Visible','on');
        drawnow();
        handles=CIV_stepPiv(hObject,eventdata,handles);
        set(handles.T_processing,'Visible','off');
    end
    if(get(handles.CB_shift,'Value')||...
        (~handles.data.piv.shiftDone&&get(handles.CB_mask,'Value')));
        % do shift detection
        set(handles.T_processing,'String','processing detect Shift..');
        set(handles.T_processing,'Visible','on');
        drawnow();
        handles=CIV_shiftNfilter(hObject,eventdata,handles);
        set(handles.T_processing,'Visible','off');
    end
    if(get(handles.CB_mask,'Value'))
        % do masking
        set(handles.T_processing,'String','processing Mask...');
        set(handles.T_processing,'Visible','on');
        drawnow();
        handles=CIV_stepMask(hObject,eventdata,handles);
        set(handles.T_processing,'Visible','off');
    end
    [handles.show.imgI handles.show.imgRGB]=showAxis(hObject,eventdata,handles);
    showPiv(hObject, eventdata, handles);
    updateMessages(hObject,handles)
else
    % cell tracking active
    
end

guidata(hObject, handles);
drawnow();

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_stop.
function PB_stop_Callback(hObject, eventdata, handles)
% hObject    handle to PB_stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.PB_stop,'Enable','off');
set(handles.PB_stop,'UserData',1);
guidata(hObject, handles);
drawnow();


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_store.
function CB_store_Callback(hObject, eventdata, handles)
% hObject    handle to CB_store (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_store
% +++++
% if checked, will store,overwrite data
% on step:
%   after step has been executed
% on run:
%   after every step of run
% +++++
if(get(handles.CB_store,'Value'))
    set(handles.CB_exclude,'Enable','on');
else
    set(handles.CB_exclude,'Enable','off');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_initMask.
function PB_initMask_Callback(hObject, eventdata, handles)
% hObject    handle to PB_initMask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++
% draw mask manually
% + extend image with boundary layer
% + draw polygon to mark mask
%+++
bs=40;
sXscl=ceil(handles.batchInfo.sX*handles.params.general.scaleFactor);
sYscl=ceil(handles.batchInfo.sY*handles.params.general.scaleFactor);
I=padarray(handles.show.imgRGB,[bs bs],0.8);
imagesc(I);
p=impoly();
c=get(p,'Children');
if(strcmp(get(c(3),'Tag'),'impoly vertex'))
    IM=createMask(p);
else
    IM=rasterLine(get(c(4),'XData'),get(c(4),'YData'),sYscl+2*bs,sXscl+2*bs);
end
p.delete
handles.data.mask.I=zeros(sYscl,sXscl);
handles.data.mask.M=zeros(sYscl,sXscl);
handles.data.mask.IM=max(handles.data.mask.IM,IM(bs+1:sYscl+bs,bs+1:sXscl+bs));
handles.data.mask.MS=handles.data.mask.I;
handles.data.mask.T1=handles.data.mask.I;
handles.data.mask.T2=handles.data.mask.I;
handles.data.mask.T3=handles.data.mask.I;
handles.data.mask.initMaskSet=1;
%draw stuff
[handles.axisImg handles.hQuiver]=resetAxis(hObject,eventdata,handles,0)
[handles.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata,handles);
updateMessages(hObject,handles)
guidata(hObject, handles)

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_clearMask.
function PB_clearMask_Callback(hObject, eventdata, handles)
% hObject    handle to PB_clearMask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
sXscl=ceil(handles.batchInfo.sX*handles.params.general.scaleFactor);
sYscl=ceil(handles.batchInfo.sY*handles.params.general.scaleFactor);
%handles.data.mask.I=zeros(sYscl,sXscl);
%handles.data.mask.M=handles.data.mask.I;
handles.data.mask.IM=handles.data.mask.I;
%handles.data.mask.MS=handles.data.mask.I;
%handles.data.mask.T1=handles.data.mask.I;
%handles.data.mask.T2=handles.data.mask.I;
%handles.data.mask.T3=handles.data.mask.I;
%handles.data.mask.done=0;
handles.data.mask.initMaskSet=0;
[handles.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata,handles);
updateMessages(hObject,handles)
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_exportStats.
function PB_exportData_Callback(hObject, eventdata, handles)
% hObject    handle to PB_exportData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++
% export current data to workspace
% exports into CIVdat
%   - params
%   - batchInfo
%   - data
%+++
CIVdat.params=handles.params;
CIVdat.batchInfo=handles.batchInfo;
CIVdat.data= handles.data;
assignin('base','CIVdat',CIVdat);
msgbox(' current frame data has been exported to variable ''CIVdat'' in workspace','Export');



%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_exportStats.
function PB_exportStats_Callback(hObject, eventdata, handles)
% hObject    handle to PB_exportStats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++
% from start to stop
% - accumulate statistics and export then
% - if some data is missing, abort and inform
%+++

set(handles.T_processing,'String','Exporting Stats...');
set(handles.T_processing,'Visible','on');
drawnow();
[stats ok] = CIV_statistics(hObject,eventdata,handles,...
                get(handles.S_statStart,'Value'),get(handles.S_statStop,'Value'));
set(handles.T_processing,'Visible','off');
if ok
    handles.data.stats=stats;
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_exportStats.
function PB_exportView_Callback(hObject, eventdata, handles)
% hObject    handle to PB_exportStats (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++
% from start to stop
% - load frame by frame and export the image to tif file
%+++    
set(handles.T_processing,'String','Exporting View...');
set(handles.T_processing,'Visible','on');
drawnow();
f1=figure;
path=fullfile(handles.batchInfo.folderpath,'IMAGES');
if ~exist(path)
    mkdir(path);
end
prevframe=handles.curFrame;
handles.curFrame=get(handles.S_statStart,'Value');
handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
for i=get(handles.S_statStart,'Value'):get(handles.S_statStop,'Value')
    %draw stuff in separate window.. sorry, dont know how to do this
    %otherwise
    %check alternative
    if 0
        [handles.show.imgI handles.show.imgRGB]=showAxis(hObject,eventdata,handles);
        f1;
        am=handles.axis_main;
        copyobj(am,f1);
        set(f1,'Units','characters');
        pos=get(am,'Position');
        set(f1,'Position',pos);
    else
        [handles.show.imgI handles.show.imgRGB]=showAxis(hObject,eventdata,handles);
        %draw image
        f1;
        axes('Position', [0, 0, 1, 1]);
        imshow(handles.show.imgRGB);
        %draw PIV
        if(get(handles.CB_showPiv,'Value') &&handles.data.piv.done)
            if 0
                xi=handles.data.piv.xi;
                yi=handles.data.piv.yi;
                [s1 ~]=size(handles.data.mask.M);
                m=handles.data.mask.M(yi+(xi-1)*s1);
                ind=find(m);
                figure;
                axes('Position', [0, 0, 1, 1]);
                imshow(handles.show.imgRGB);
                
                X=[xi(ind), yi(ind)];
                Y=[xi(ind)+handles.data.piv.lu(ind), yi(ind)+handles.data.piv.lv(ind)];
                hold on
                arrow(X,Y,'BaseAngle',60,'TipAngle',20,'Length',2,'Width',0.3,'LineWidth',0.1,'FaceColor',[0 0.95 0.6]);
                hold off
            else
                hold on;
                quiver(handles.data.piv.xi,handles.data.piv.yi, ...
                    handles.data.piv.lu, handles.data.piv.lv,'g');
                hold off;
            end
        end
    end
    %assemble filename
    filename=sprintf2(handles.batchInfo.pattern,handles.curFrame,'-vI%dvL%dP%d','.tif');
    filename=sprintf2(filename,handles.viewI, handles.viewL, get(handles.CB_showPiv,'Value'));
    %store image
    saveas(f1,fullfile(path,filename));
    %advance one frame
    handles.prevdata=handles.data;
    handles.prevdata_ready=1;
    if(handles.nextdata_ready)
        handles.data=handles.nextdata;
        handles.curFrame=handles.curFrame+1;
    else
        handles.curFrame=handles.curFrame+1;
        handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
    end
    handles.nextdata_ready=0;
   
end
close(f1);
%reset
handles.curFrame=prevframe;
handles.data=CIV_loadData(handles.curFrame,handles.batchInfo,handles.params);
[handles.show.imgI handles.show.imgRGB]=showAxis(hObject,eventdata,handles);
set(handles.T_processing,'Visible','off');


    

%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| DATA VIEW STUFF ||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [axisImg hQuiver]=resetAxis(hObject,eventdata,handles,keepFocus)
% init/reset mainAxis
% put focus on current figure
figure(handles.CIV);
% store focus
if(keepFocus)
    XLim=get(handles.axis_main,'XLim');
    YLim=get(handles.axis_main,'YLim');
end
% AXIS
sXscl=ceil(handles.batchInfo.sX*handles.params.general.scaleFactor);
sYscl=ceil(handles.batchInfo.sY*handles.params.general.scaleFactor);
rgb=zeros(sYscl,sXscl,3);
rgb(:,:,1)=img_rescale(imresize(handles.Z,[sYscl,sXscl]));
rgb(:,:,2)=rgb(:,:,1);
rgb(:,:,3)=rgb(:,:,1);
imagesc(rgb);
if(get(handles.CB_aspectRatio,'Value'))
    set(handles.axis_main,'DataAspectRatio',[1 1 1]);
end
axisImg=get(handles.axis_main,'Children');
%set(axisImg,'ButtonDownFcn',@(hObject,eventdata)CIV(...
%        'AX_BD_Callback',hObject,eventdata,guidata(hObject)));
% PIV DATA
hold on
quiver(handles.data.piv.xi,handles.data.piv.yi,...
        handles.data.piv.u, handles.data.piv.v,'g');
hold off
c=get(handles.axis_main,'Children');
hQuiver=c(1);
show=get(handles.CB_showPiv,'Value');
choices={'off','on'};
if(handles.data.piv.done)
    set(hQuiver,'Visible',choices{show+1});
end
% reset focus
if(keepFocus)
    set(handles.axis_main,'XLim',XLim);
    set(handles.axis_main,'YLim',YLim);
end
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [imgI imgRGB]=showAxis(hObject,eventdata,handles)
% display things in the main axis
switch(handles.viewI)
    case 0
        imgI=min(handles.data.general.I_orig*get(handles.S_bright,'Value'),1);
    case 1
        if handles.params.piv.crvlt
            imgI=min(handles.data.general.I_crvlt*get(handles.S_bright,'Value'),1);
        else
            imgI=min(handles.data.general.I_lnf*get(handles.S_bright,'Value'),1);
        end
    case 2
        imgI=min(handles.data.mask.I*get(handles.S_bright,'Value'),1);
end
handles.show.imgRGB=zeros(size(imgI,1),size(imgI,2),3);
switch(handles.viewL)
    case 0
        imgRGB(:,:,1)=imgI;
        imgRGB(:,:,2)=imgI;
        imgRGB(:,:,3)=imgI;
    case 1
        imgRGB(:,:,1)=imgI*0.7+handles.data.mask.M*0.3;
        imgRGB(:,:,2)=imgI*0.7+handles.data.mask.MS*0.1 ;
        imgRGB(:,:,3)=imgI*0.7;
    case 2
        imgRGB(:,:,1)=imgI*0.7+handles.data.mask.IM*0.3;
        imgRGB(:,:,2)=imgI*0.7;
        imgRGB(:,:,3)=imgI*0.7;
    case 3
        imgRGB(:,:,1)=imgI*0.7+handles.data.mask.T1*0.3;
        imgRGB(:,:,2)=imgI*0.7;
        imgRGB(:,:,3)=imgI*0.7;
    case 4
        imgRGB(:,:,1)=imgI*0.7+handles.data.mask.T2*0.3;
        imgRGB(:,:,2)=imgI*0.7;
        imgRGB(:,:,3)=imgI*0.7;
    case 5
        imgRGB(:,:,1)=imgI*0.7+handles.data.mask.T3*0.3;
        imgRGB(:,:,2)=imgI*0.7;
        imgRGB(:,:,3)=imgI*0.7;
    case 6
        % mark front region
        MF=handles.data.mask.M*0;
        if(handles.params.stats.confReg)
            dist=bwdist(1-handles.data.mask.M);
            ind=find(dist<(handles.params.general.scaleFactor*...
                handles.params.stats.regExt/handles.params.general.resolution));            
            MF(ind)=1;
            MF=MF.*handles.data.mask.M;
        end
        imgRGB(:,:,1)=imgI*0.7+handles.data.mask.M*0.3;
        imgRGB(:,:,2)=imgI*0.7;
        imgRGB(:,:,3)=imgI*0.7+MF*0.3;
    case 7
        fac=0.5;
        xlo=min(handles.data.piv.yi(:));
        xhi=max(handles.data.piv.yi(:));
        ylo=min(handles.data.piv.xi(:));
        yhi=max(handles.data.piv.xi(:));
        [s1 s2]=size(imgI);
        a=cart2pol(handles.data.piv.lu,handles.data.piv.lv);
        m=handles.data.mask.M(handles.data.piv.yi+(handles.data.piv.xi-1)*s1);
        rgb=img_angleRGB(a,m);
        
        rgb=min(max(imresize(rgb,[xhi-xlo+1,yhi-ylo+1]),0),1);
        
        imgRGB(:,:,1)=imgI*(1-fac);%+RGB(:,:,1)*fac;
        imgRGB(:,:,2)=imgI*(1-fac);%+RGB(:,:,2)*fac;
        imgRGB(:,:,3)=imgI*(1-fac);%+RGB(:,:,3)*fac;
        imgRGB(xlo:xhi,ylo:yhi,1)=imgRGB(xlo:xhi,ylo:yhi,1)+rgb(:,:,1)*fac;
        imgRGB(xlo:xhi,ylo:yhi,2)=imgRGB(xlo:xhi,ylo:yhi,2)+rgb(:,:,2)*fac;
        imgRGB(xlo:xhi,ylo:yhi,3)=imgRGB(xlo:xhi,ylo:yhi,3)+rgb(:,:,3)*fac;
    case 8
        fac=0.5;
        mxvel=0.5; %um/min;
        xlo=min(handles.data.piv.yi(:));
        xhi=max(handles.data.piv.yi(:));
        ylo=min(handles.data.piv.xi(:));
        yhi=max(handles.data.piv.xi(:));
        [s1 s2]=size(imgI);
        %get angles
        [a r]=cart2pol(handles.data.piv.lu,handles.data.piv.lv);
        %get mask
        m=handles.data.mask.M(handles.data.piv.yi+(handles.data.piv.xi-1)*s1);
        %rescale velocity
        
        r=r*(handles.params.general.resolution/handles.params.general.scaleFactor/handles.params.general.dt)/mxvel;
        rgb=img_angleRGB(a,m,r);
        
        rgb=min(max(imresize(rgb,[xhi-xlo+1,yhi-ylo+1]),0),1);
        
        imgRGB(:,:,1)=imgI*(1-fac);%+RGB(:,:,1)*fac;
        imgRGB(:,:,2)=imgI*(1-fac);%+RGB(:,:,2)*fac;
        imgRGB(:,:,3)=imgI*(1-fac);%+RGB(:,:,3)*fac;
        imgRGB(xlo:xhi,ylo:yhi,1)=imgRGB(xlo:xhi,ylo:yhi,1)+rgb(:,:,1)*fac;
        imgRGB(xlo:xhi,ylo:yhi,2)=imgRGB(xlo:xhi,ylo:yhi,2)+rgb(:,:,2)*fac;
        imgRGB(xlo:xhi,ylo:yhi,3)=imgRGB(xlo:xhi,ylo:yhi,3)+rgb(:,:,3)*fac;
end
set(handles.axisImg,'CData',imgRGB);
set(handles.T_viewImage,'String',sprintf('[%d]',handles.viewI+1));
set(handles.T_viewMask,'String',sprintf('[%d]',handles.viewL+1));
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
function showPiv(hObject,eventdata,handles)
%display piv results
if(get(handles.CB_showPiv,'Value') && handles.data.piv.done)
    set(handles.hQuiver,'Visible','on');
    set(handles.hQuiver,'XData',handles.data.piv.xi);
    set(handles.hQuiver,'YData',handles.data.piv.yi);
    if(0)
        % pure
        set(handles.hQuiver,'UData',handles.data.piv.u);
        set(handles.hQuiver,'VData',handles.data.piv.v);
    else
        %shift corrected
        set(handles.hQuiver,'UData',handles.data.piv.lu);
        set(handles.hQuiver,'VData',handles.data.piv.lv);
    end
else
    set(handles.hQuiver,'Visible','off');
end 
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_bright_Callback(hObject, eventdata, handles)
% hObject    handle to S_bright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
[handels.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata,handles);
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_bright_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_bright (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on scroll wheel click while the figure is in focus.
function CIV_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to CIV (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)
%++++
% switch displayed image
%++++
scroll=eventdata.VerticalScrollCount;
if(get(handles.RB_image,'Value'))
    view=mod(handles.viewI+scroll,handles.maxViewI);
    if(view~=handles.viewI)
        handles.viewI=view;
        guidata(hObject, handles)
        [handels.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata,handles);
    end
else
    view=mod(handles.viewL+scroll,handles.maxViewL);
    if(view~=handles.viewL)
        handles.viewL=view;
        guidata(hObject, handles)
        [handels.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata,handles);
    end
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_piv.
function CB_showPiv_Callback(hObject, eventdata, handles)
% hObject    handle to CB_piv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_piv
showPiv(hObject, eventdata, handles);
guidata(hObject, handles);


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_shift.
function CB_piv_Callback(hObject, eventdata, handles)
% hObject    handle to CB_shift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% if this is on, disable shift box and turn of
if(get(hObject,'Value'))
    set(handles.CB_shift,'Value',1);
    set(handles.CB_shift,'Enable','off');
else
    set(handles.CB_shift,'Value',0);
    set(handles.CB_shift,'Enable','on');
end

% Hint: get(hObject,'Value') returns toggle state of CB_shift

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_aspectRatio.
function CB_aspectRatio_Callback(hObject, eventdata, handles)
% hObject    handle to CB_aspectRatio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_aspectRatio
[handles.axisImg handles.hQuiver]=resetAxis(hObject,eventdata,handles,1);
[handels.show.imgI handles.show.imgRGB]=showAxis(hObject, eventdata,handles);
showPiv(hObject, eventdata, handles);
guidata(hObject, handles);

%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| GUI VIEW STUFF |||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in PB_layers.
function PB_tab_Callback(hObject, eventdata, handles)
% hObject    handle to PB_layers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++++++++++
% - switch on visibility of the correct tab
% - highlight the current tab
% hObject is the tab that has been clicked
%+++++++++++++
% lable clicked tab
set(handles.tabs(handles.curTab,1),'Enable','on');
set(handles.tabs(handles.curTab,2),'Visible','off');
handles.curTab = find(handles.tabs == hObject);
set(handles.tabs(handles.curTab,1),'Enable','off');
set(handles.tabs(handles.curTab,2),'Visible','on');
% Update handles structure
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function updatePannel(hObject,handles)
% enable/disable next/prev buttons
%update File Panel
if(handles.curFrame==handles.batchInfo.maxFrame)
    set(handles.PB_next,'Enable','off');
    set(handles.PB_run,'Enable','off');
    set(handles.PB_step,'Enable','off');
else
    set(handles.PB_next,'Enable','on');
    set(handles.PB_run,'Enable','on');
    set(handles.PB_step,'Enable','on');
end
if(handles.curFrame==handles.batchInfo.minFrame)
    set(handles.PB_prev,'Enable','off');
else
    set(handles.PB_prev,'Enable','on');
end

if(handles.curFrame>handles.batchInfo.maxFrame-5)
    set(handles.PB_nnext,'Enable','off');
else
    set(handles.PB_nnext,'Enable','on');
end
if(handles.curFrame<handles.batchInfo.minFrame+5)
    set(handles.PB_pprev,'Enable','off');
else
    set(handles.PB_pprev,'Enable','on');
end

set(handles.T_curFrame,'String',handles.curFrame);

% Update exclude box
set(handles.CB_exclude,'Value',handles.data.general.exclude);

% Update handles structure
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function updateMessages(hObject,handles)
c={'off','on'};
set(handles.T_pivDone,'Visible',c{handles.data.piv.done+1});
set(handles.T_maskingDone,'Visible',c{handles.data.mask.done+1});
set(handles.T_shiftDetected,'Visible',c{handles.data.piv.shift+1});
set(handles.T_initMaskSet,'Visible',c{handles.data.mask.initMaskSet+1});
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||| STATS ||||||||
%|||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function initStat(hObject, eventdata, handles)
set(handles.S_statStart,'Value',handles.batchInfo.minFrame);
set(handles.E_statStart,'String',num2str(handles.batchInfo.minFrame));
set(handles.S_statStop,'Value',handles.batchInfo.maxFrame-1);
set(handles.E_statStop,'String',num2str(handles.batchInfo.maxFrame-1));
set(handles.S_statStart,'Min',handles.batchInfo.minFrame);
set(handles.S_statStart,'Max',handles.batchInfo.maxFrame-0.9);
set(handles.S_statStop,'Min',handles.batchInfo.minFrame);
set(handles.S_statStop,'Max',handles.batchInfo.maxFrame-0.9);
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_statStart_Callback(hObject, eventdata, handles)
% hObject    handle to S_statStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
statStart=round(get(hObject,'Value'));
set(hObject,'Value',statStart);
set(handles.E_statStart,'String',num2str(statStart));
%reset slider ranges
set(handles.S_statStop,'Min',statStart);
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_statStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_statStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_statStart_Callback(hObject, eventdata, handles)
% hObject    handle to E_statStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_statStart as text
%        str2double(get(hObject,'String')) returns contents of E_statStart as a double
[statStart ok] = getEditInt(get(handles.S_statStart,'Value'),hObject, ...
    handles.batchInfo.minFrame,get(handles.S_statStop,'Value'));
if ~ok,
    set(hObject,'String',num2str(statStart));
else
    set(handles.S_statStart,'Value',statStart);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_statStart_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_statStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on slider movement.
function S_statStop_Callback(hObject, eventdata, handles)
% hObject    handle to S_statStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
statStop=round(get(hObject,'Value'));
set(hObject,'Value',statStop);
set(handles.E_statStop,'String',num2str(statStop));
%reset slider ranges
set(handles.S_statStart,'Max',statStop+0.1);
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function S_statStop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to S_statStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function E_statStop_Callback(hObject, eventdata, handles)
% hObject    handle to E_statStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of E_statStop as text
%        str2double(get(hObject,'String')) returns contents of E_statStop as a double
[statStop ok] = getEditInt(get(handles.S_statStop,'Value'),hObject, ...
    get(handles.S_statStart,'Value'),handles.batchInfo.maxFrame-1);
if ~ok,
    set(hObject,'String',num2str(statStop));
else
    set(handles.S_statStop,'Value',statStop);
end
guidata(hObject, handles);

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes during object creation, after setting all properties.
function E_statStop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to E_statStop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Executes on button press in CB_exclude.
function CB_exclude_Callback(hObject, eventdata, handles)
% hObject    handle to CB_exclude (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of CB_exclude
%++++++
% store wether to exclude this fram from the statistics
%++++++
handles.data.general.exclude=get(handles.CB_exclude,'Value');
guidata(hObject, handles);

%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||| Helper Function ||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||
%||||||||||||||||||||||||||||||||||||||||||||||

%|||||||||||||||||||||||
%|||||||||||||||||||||||
% --- Rasterieze line
function res=rasterLine(X,Y,xSize,ySize)
T=round(X);
X=round(Y);
Y=T;
res=zeros(xSize,ySize);
DX(1)=X(2)-X(1);
DX(2)=Y(2)-Y(1);

if(abs(DX(1))>abs(DX(2)));
    dy=DX(2)/DX(1);
    for x=0:sign(DX(1)):DX(1)
        y=unique([Y(1)+round(dy*(x-0.5)),Y(1)+round(dy*(x+0.5))]);
        res(X(1)+x,y)=1;
    end
else
    dx=DX(1)/DX(2);
    for y=0:sign(DX(2)):DX(2)
        x=unique([X(1)+round(dx*(y-0.5)),X(1)+round(dx*(y+0.5))]);
        res(x,Y(1)+y)=1;
    end
end

%|||||||||||||||||||||||
%|||||||||||||||||||||||
function [val, ok]=getEditInt(preval, E, min, max)
val=round(str2double(get(E,'String')));
ok=1;
if(isnan(val) || (val<min) || (val>max)),
    val=preval;
    ok=0;
    errordlg(sprintf('Enter value between %0.2f and %0.2f!', min, max));
end


% --- Executes on button press in PB_clearAllPIV.
function PB_clearAllPIV_Callback(hObject, eventdata, handles)
% hObject    handle to PB_clearAllPIV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++
% delete stored data
%+++++
pthGEN=fullfile(handles.batchInfo.folderpath,handles.params.general.folder);
pthPIV=fullfile(pthGEN,handles.params.general.folderPIV);
ans=questdlg('Do you really want to delete all stored PIV data','WARNING','ok','cancel','ok');
if(strcmp(ans,'ok'))
    delete(fullfile(pthPIV,'*-PIV.mat'));
end


% --- Executes on button press in PB_clearAllMask.
function PB_clearAllMask_Callback(hObject, eventdata, handles)
% hObject    handle to PB_clearAllMask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%+++++
% delete stored data
%+++++
pthGEN=fullfile(handles.batchInfo.folderpath,handles.params.general.folder);
pthMASK=fullfile(pthGEN,handles.params.general.folderMASK);
ans=questdlg('Do you really want to delete all stored MASK data','WARNING','ok','cancel','ok');
if(strcmp(ans,'ok'))
    delete(fullfile(pthMASK,'*-MASK.mat'));
end
