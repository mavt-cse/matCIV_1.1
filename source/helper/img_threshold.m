%threshold image
function res=img_threshold(im,thr)
    res=zeros(size(im));
    I=find(im>thr);
    res(I)=1;