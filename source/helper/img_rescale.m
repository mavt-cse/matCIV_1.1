%rescale the image intensity between 0 and 1
function res=img_rescale(im,perc);
% rescale image based on Max/Min value found in image
     

    if nargin<2
        perc=1;
    end
    minim=double(min(im(:)));
    maxim=perc*double(max(im(:)));
    res=(double(im)-minim)/(maxim-minim);
    

