function [ok, batchInfo, params, curFrame] =CIV_loadBatchInfo(folderpath, filename, batchInfo, params)

% SCRATCH2L_LOADFOLDERINFO
%   load folder information current frame id
%
%   OK: 1 if successfull, 0 otherwise
%   BATCHINFO: batch Info struct
%   CURFRAME: current frame id

ok=0;
%check if folder changed:
if(~isequal(folderpath,batchInfo.folderpath))
    datapath = fullfile(folderpath,params.general.folder);
    bipath = fullfile(folderpath,params.general.folderBI);
    %statspath = fullfile(folderpath,'STATS');
    if(exist(fullfile(bipath,'CIV_batchInfo.mat'),'file'));
        % if batch info exists, load it
        batchInfo=load(fullfile(bipath,'CIV_batchInfo.mat'));
        % in case folder has been moved
        batchInfo.folderpath = folderpath;
        params=CIV_getDefaultParams(fullfile(folderpath,params.general.folder));
    else
        % otherwise, assemble ig
        batchInfo.folderpath = folderpath;
        
        % find file pattern:
        % load filenames in folder
        % compare to first to last
        % extract pattern where they differ
        fileNamesRaw=dir(fullfile(folderpath,'*.tif'));
        fileNames={fileNamesRaw.name};
        diff=ones(1,length(fileNames(1)));
        for f=fileNames
            diff=diff.*(f{1}==filename);
        end
        %should get ones with a contiguous burst of 0's followed by ones
        label=bwlabel(diff);
        if (max(label)>2)
            return;
        end
        z=int2str(length(find(label==0)));
        [tmp,pattern,ext]=fileparts(strcat(filename(find(label==1)),'%0',z,'i',filename(find(label==2))));
        % store pattern
        batchInfo.pattern=strcat(pattern,'%s%s');
        % store extension
        batchInfo.ext=ext;
        % store regular expression
        batchInfo.regexp=strcat('(?<=',filename(find(label==1)),')\d+(?=',filename(find(label==2)),')');
        % find min/max frames
        frames=str2num(cell2mat(regexp(cell2mat(fileNames),batchInfo.regexp,'match')'));
        batchInfo.minFrame=min(frames);
        batchInfo.maxFrame=max(frames);
        if(length(fileNames)~=batchInfo.maxFrame-batchInfo.minFrame+1)
            return;
        end
        %load Params
        pthGEN=fullfile(folderpath,params.general.folder);
        params=CIV_getDefaultParams(pthGEN);
        %find image size
        tmp=imread(fullfile(folderpath,filename));
        batchInfo.sY=size(tmp,1);
        batchInfo.sX=size(tmp,2);
        %store it
        pthBI=fullfile(folderpath,params.general.folderBI);
        if(~exist(pthBI,'file'))
            mkdir(pthBI);
        end
        save(fullfile(pthBI,'CIV_batchInfo.mat'),'-struct','batchInfo');
    end
end
curFrame=batchInfo.minFrame;
try
    curFrame=str2num(cell2mat(regexp(filename,batchInfo.regexp,'match')));
catch
end
ok=1;