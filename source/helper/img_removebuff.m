function res=img_removebuff(img,bS);
% remove a buffer region around the image;
% to be used together with img_addbuff
%
% img: image
% bS:  size of buffer in pixel
%       - buffer around image on all faes of equal size

s=size(img);
res = img((bS+1):(s(1)-bS),(bS+1):(s(2)-bS));
