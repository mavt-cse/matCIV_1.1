function h=CIV_shiftNfilter(hObject, eventdata,h)

% load next frame, if necessary
if ~h.nextdata_ready
    h.nextdata=CIV_loadData(h.curFrame+1,h.batchInfo,h.params);
    h.nextdata_ready=1;
end

% use velocity field as indicator if around..
mv=0;
shift=0;
us=0;
vs=0;
[u,v]=naninterp_mod(h.data.piv.u,h.data.piv.v,'linear',[],h.data.piv.xi,h.data.piv.yi);
if h.data.piv.done
    mv=sqrt(mean(u(:))^2+mean(v(:))^2);
end
T=h.data.general.I_orig;
N=h.nextdata.general.I_orig;

%@@@ put back to 2.5
if(mv>h.params.piv.sftTh)
    shift=1;
    [s1 s2]=size(T);
    bd=round(s1/10);
    T2=T(1+bd:s1-bd,1+bd:s2-bd);
    cc = normxcorr2(T2,N);
    [max_c, imax] = max(abs(cc(:)));
    [ypeak, xpeak] = ind2sub(size(cc),imax(1));
    us=(xpeak-size(T2,2))-bd;
    vs=(ypeak-size(T2,1))-bd;
end

h.data.piv.shiftDone=1;
h.data.piv.shift=shift;
h.data.piv.us=us;
h.data.piv.vs=vs;
h.data.piv.mv=mv;
%Shift correction
if(h.params.piv.sft)
    lu=h.data.piv.u-us;
    lv=h.data.piv.v-vs;
else
    lu=h.data.piv.u;    
    lv=h.data.piv.v;
end
%Signal to Noise Ratio
if(h.params.piv.snrF)
    [lu,lv]=snrfilt(h,data.piv.xi,h.data.piv.yi,lu,lv,h.data.piv.snr,h.params.piv.snrTh);
end
%Global Filtering
if(h.params.piv.glbF)
    [lu,lv]=globfilt(h.data.piv.xi,h.data.piv.yi,lu,lv,h.params.piv.glbTh);
end
%Local Filtering
if(h.params.piv.locF)
    [lu,lv]=localfilt(h.data.piv.xi,h.data.piv.yi,lu,lv,h.params.piv.locTh,h.params.piv.locMtd,h.params.piv.locKs);
end
%NaN Interpolation
[lu,lv]=naninterp_mod(lu,lv,'linear',[],h.data.piv.xi,h.data.piv.yi);
h.data.piv.lu=lu;
h.data.piv.lv=lv;