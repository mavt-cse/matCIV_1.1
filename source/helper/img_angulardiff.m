function [R RR] = img_angulardiff(A,MSK,mcl,dbs,dbol)
% calculate spatial variation of angle
% A =[-pi,pi]
% MSK = mask
% mcl = maximal correlation length
% bs = bin size
% bol = bin overlap
% calculate for regions with respect to interface
%
% R radial correlation per bin
% RR spatial correlation per bin
%
% NOTE: 
% - correlations are calculated inside half circle away from the mask front
% - correlations are calculated up to mcl, extending over the bin borders
% - last bin center is at least mcl+bs/2 away from end of domain
% + best if bs*(1-bol) is integer
% - !! if confluent mask, one bin over everything

[As1,As2]=size(A);

%maxdist in pixel
% should be taken care of by caller
%md=min(mcl,floor(min(size(A))/2));
md=mcl;

% for all pixels
sTW=md*2+1; % size of target window
rTW=md;     % radius of target window


%if not confluent
if(min(MSK(:))==0)
    %distance to front
    D1=bwdist(1-MSK);
    
    %mxdist: furthest point from mask front to be considered
    % find far-end boundary of mask
    s1=mean(MSK,1);
    s2=mean(MSK,2);
    [tmp,c]=max([s2(1),s2(end),s1(1),s1(end)]);
    mxdist=0;
    % also define orientation based masking indecis
    indDir=[];
    switch c
        case 1
            %NORTH
            %mxdist=min(DIST(1,:));
            mxdist=mean(D1(1,:));
            %orientation based masking index
            tmp=zeros(sTW);
            tmp(1:ceil(end/2),:)=1;
            indDir=find(tmp);
        case 2
            %SOUTH
            %mxdist=min(DIST(end,:));
            mxdist=mean(D1(end,:));
            %orientation based masking index
            tmp=zeros(sTW);
            tmp(floor(end/2):end,:)=1;
            indDir=find(tmp);
        case 3
            %WEST
            %mxdist=min(DIST(:,1));
            mxdist=mean(D1(:,1));
            %orientation based masking index
            tmp=zeros(sTW);
            tmp(:,1:ceil(end/2))=1;
            indDir=find(tmp);
        case 4
            %EAST
            %mxdist=min(DIST(:,end));
            mxdist=mean(D1(:,end));
            %orientation based masking index
            tmp=zeros(sTW);
            tmp(:,floor(end/2):end)=1;
            indDir=find(tmp);
    end
    
    %number of distance bins
    dbinN=floor((mxdist-mcl-dbs)/(dbs*(1-dbol)))+1;
    dbinL=(0:dbinN-1)*dbs*(1-dbol);
    dbinH=dbinL+dbs;
else
    dbinN=1;
    dbinL=0;
    dbinH=1;
    D1=zeros(As1,As2);
    indDir=1:(sTW*sTW);
end

r=zeros(sTW*sTW,dbinN);
s=zeros(sTW*sTW,dbinN);

%TEST=zeros(As1,As2);
for i=1:As1
    for j=1:As2
        %only consider valid points
        if MSK((j-1)*As1+i)
            
            %find dist bin
            db=find(dbinL<=D1(i,j) &dbinH > D1(i,j));
            %if(size(db,2)~=0)
            %    TEST(i,j)=sum(db);
            %end
            %db=floor(DIST(i,j)/bs)+1;
            %if(db<=dbins) %only if not in last half bin
            if ~isempty(db)
                
                %determine target window over which to angular dist
                TWiLo=max(1,i-rTW);
                TWiHi=min(As1,i+rTW);
                TWjLo=max(1,j-rTW);
                TWjHi=min(As2,j+rTW);
                TW=A(TWiLo:TWiHi,TWjLo:TWjHi);
                
                indTW=find(MSK(TWiLo:TWiHi,TWjLo:TWjHi));
                
                %calculate delta theta
                AD=abs(TW(indTW)-A(i,j));
                AD=min(AD,2*pi-AD);
                
                %[s1TW s2TW]=size(TW);
                s1TW=size(TW,1);
                oiL=TWiLo-i+rTW;
                ojL=TWjLo-j+rTW;
                row=floor((indTW-1)/s1TW);
                indDW=ojL*sTW+(row+1)*oiL+row*(sTW-s1TW-oiL)+indTW;
                
                %find dist bin
                for idb=db
                    r(indDW,idb)=r(indDW,idb)+AD;
                    s(indDW,idb)=s(indDW,idb)+1;
                end
            end
        end 
    end
end

%for radial distance calculation
D2=zeros(sTW);
D2(ceil(end/2),ceil(end/2))=1;
D2=bwdist(D2);
rbinN=rTW;
rbs=rTW/rbinN;
for irb=1:rbinN
    iD2{irb}=intersect(intersect(find(D2>=(irb-1)*rbs),find(D2<(irb)*rbs)),indDir);
end
R=zeros(rbinN,dbinN);
for idb=1:dbinN
    for irb=1:rbinN
        R(irb,idb)=sum(r(iD2{irb},idb))/sum(s(iD2{irb},idb));
    end
end
RR=r./s;