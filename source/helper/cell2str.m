function str=cell2str(c)

%CELL2STR converts a cell to a string such that eval can transform the
% string back into the original cell
% cell can contain numbers, matrices, strings and cells

str='';
if iscell(c),
    str='{';
    for i=1:length(c)
       if iscell(c{i}),
           str=strcat(str,cell2str(c{i}));
       elseif ischar(c{i}),
           str=strcat(str,'''',c{i},'''');
       elseif isnumeric(c{i}),
           str=strcat(str,mat2str(c{i}));
       else
           str='error';
       end
       if i~=length(c),
           str=strcat(str,',');
       end
    end
    str=strcat(str,'}');
    return;
elseif isempty(c)
    str='{}';
else
    str='error';
end