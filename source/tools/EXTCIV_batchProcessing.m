function res=EXTCIV_batchProcessing(DATA,DO_PIV,DO_MASK,DO_STATS)

%++++++++++++++++++++++++++++++++++++++++
% Script to run the PIV, MASK and statistical analysis standalone:
%
% PRECONDITION ALL
% - execure CIV_setpath
%
% PRECONDITION PIV:
% - to run with default parameter set: none
% - to run with defined parameter set:
%   - the parameters for the PIV analysis have to be set using CIV
%       - can be done by opening the first file in CIV
%       - open parameters
%       - set parameters
%       - update parameters
%       - store parameters
%       - done
%
% PRECONDITION MASK:
% - to run with default parameter set:
%   - data needs to be very good to correclty guess initial mask
%   - to initialize the mask by hand:
%      - open first file
%       - init mask
%      - check Mask box
%       - check Store/Replace box
%       - advance to next frame (will store the current frame
%       - done
% - to run with defined parameter set:
%   - set initial mask (see above)
%   - the parameters for automated MASKING have to be set using CIV
%       - can be done by opening the first file in CIV
%       - open parameters
%       - set parameters
%       - update parameters
%       - store parameters
%       - done
%
%
% PARAMETERS:
%   DATA: struct containing experiment folder information
%       - see EXTCIV_prepareBatchDataTMPLT to generate the DATA
%       struct
%   DO_PIV:  if true, a PIV analysis will be conducted
%   DO_MASK: if true, autmoated masking will be conducted
%++++++++++++++++++++++++++++++++++++++++

if nargin<2
    disp('mode not specified.. just going through files doing nothing');
    %dont do anything if not specified
    DO_PIV=0;
    DO_MASK=0;
    DO_STATS=0;
end

fprintf('DATAPATH: %s\n',DATA.basePath);
for e=1:length(DATA.EXP)
    for p=1:length(DATA.EXP{e}.pos)
        fprintf('Processing: %s/%s\n',DATA.EXP{e}.folder,DATA.EXP{e}.pos{p});
        %reset things
        batchInfo.folderpath='';
        params.general.folder='DATA';
        params.general.folderBI='BI';
        params.general.folderMASK='MASK';
        params.general.folderPIV='PIV';
        %load params and batch info
        clear h;
        %[ok,h.batchInfo,h.params,h.curFrame]=CIV_loadBatchInfo(fullfile(folderpath{1},exp{e},pos{p}),'',batchInfo,params);
        %assemble filename
        expPath=fullfile(DATA.basePath,DATA.EXP{e}.folder,DATA.EXP{e}.pos{p});
        fileNamesRaw=dir(fullfile(expPath,'*.tif'));
        fileNames={fileNamesRaw.name};
        filename=fileNames{1};
        %load / create batch info
        [ok,h.batchInfo,h.params,h.curFrame]=CIV_loadBatchInfo(...
            fullfile(DATA.basePath,DATA.EXP{e}.folder,DATA.EXP{e}.pos{p}),filename,batchInfo,params);
        %reset some parameters
        h.nextdata_ready=0;
        h.CBV_piv=1;
        h.CBV_shift=1;
        h.CBV_mask=1;
        
        h.curFrame=h.batchInfo.minFrame;
        h.data=CIV_loadData(h.curFrame,h.batchInfo,h.params);
        h.prevdata=h.data;
        h.prevdata_ready=0;
        h.nextdata=h.data;
        h.nextdata_ready=0;
        if(DO_PIV | DO_MASK)
        for f=h.curFrame:h.batchInfo.maxFrame-1
            
            % do piv analysis
            if(DO_PIV)
                
                h=CIV_stepPiv([],[],h);
                h=CIV_shiftNfilter([],[],h);
            end
            
            % do masking
            if(DO_MASK)
                
                h=CIV_stepMask([],[],h);
            end
            
            %store data
            CIV_storeData(h);
            
            %advance one frame
            h.prevdata=h.data;
            h.prevdata_ready=1;
            if(h.nextdata_ready)
                h.data=h.nextdata;
            else
                h.data=CIV_loadData(h.curFrame,h.batchInfo,h.params);
            end
            h.nextdata_ready=0;
            h.curFrame=h.curFrame+1;
        end
        end
        
        % do statistics
        if(DO_STATS)
            CIV_statistics([],[],h,h.batchInfo.minFrame,h.batchInfo.maxFrame);
        end
    end
end
