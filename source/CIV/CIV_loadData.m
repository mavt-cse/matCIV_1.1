function data = CIV_loadData(curFrame,batchInfo,params)

% SCRATCH2L_LOADDATA:
%   loads data for selected frame
%
%   CURFRAME: id of current frame to load
%   batchInfo: folder information struct
%   PARAMS: params
%   DATA: data struct containing loaded data

%assemble file name
filename=sprintf2(batchInfo.pattern,curFrame,'',batchInfo.ext);
generalData=sprintf2(batchInfo.pattern,curFrame,'-GEN','.mat');
pivData=sprintf2(batchInfo.pattern,curFrame,'-PIV','.mat');
maskData=sprintf2(batchInfo.pattern,curFrame,'-MASK','.mat');
trackData=sprintf2(batchInfo.pattern,curFrame,'-TRACK','.mat');

% data paths
pthGEN=fullfile(batchInfo.folderpath,params.general.folder);
pthPIV=fullfile(pthGEN,params.general.folderPIV);
pthMASK=fullfile(pthGEN,params.general.folderMASK);


data.piv_ready=0;

sYscl=ceil(batchInfo.sY*params.general.scaleFactor);
sXscl=ceil(batchInfo.sX*params.general.scaleFactor);
Z=img_rescale(imresize(imread('source/images/bee.tif'),[sYscl sXscl]));

%check for general Data
if(exist(fullfile(pthGEN,generalData),'file'))
    %load general data
    data.general=load(fullfile(pthGEN,generalData));
    try
        g=data.general.exclude;
    catch
        data.general.exclude=0;
    end
    try
        g=data.general.crvlt_done;
    catch
        data.general.I_crvlt=Z;
        data.general.C_crvlt=[];
        data.general.crvlt_transform=0;
        data.general.crvlt_filter=0;
        data.general.crvlt_done=0;
    end
else
    %load image, init/reset general data
    tmp=img_rescale(imresize(imread(fullfile(batchInfo.folderpath,filename)),params.general.scaleFactor));
    if(size(tmp,3)==3)
        tmp=rgb2gray(tmp);
    end
    data.general.I_orig=tmp;
    data.general.I_lnf=Z;
    data.general.lnf_done=0;
    data.general.exclude=0;
    data.general.I_crvlt=Z;
    data.general.C_crvlt=[];
    data.general.crvlt_done=0;
    data.general.crvlt_transform=0;
    data.general.crvlt_filter=0;
end

%check for piv Data
if(exist(fullfile(pthPIV,pivData),'file'))
    %load piv data
    data.piv=load(fullfile(pthPIV,pivData));
    try
        data.piv.lu;
    catch
        data.piv.lu=data.piv.u;
        data.piv.lv=data.piv.v;
    end
else
    %init/reset piv data
    ws=min(min(params.piv.winsize));
    sYpiv=floor((sYscl-ws)/(ws*params.piv.overlap))+1;
    sXpiv=floor((sXscl-ws)/(ws*params.piv.overlap))+1;
    data.piv.xi=zeros(sYpiv,sXpiv);
    data.piv.yi=data.piv.xi;
    data.piv.u=data.piv.xi;
    data.piv.v=data.piv.xi;
    data.piv.lu=data.piv.xi;
    data.piv.lv=data.piv.xi;
    data.piv.shiftDone=0;
    data.piv.shift=0;
    data.piv.us=0;
    data.piv.vs=0;
    data.piv.I=Z;
    data.piv.done=0;
    data.piv.ready=0;
end

%check for mask Data
if(exist(fullfile(pthMASK,maskData),'file'))
    %load mask data
    data.mask=load(fullfile(pthMASK,maskData));
    %data.mask.T1=Z;
    %data.mask.T2=Z;
    %data.mask.T3=Z;
else
    %init/reset mask data
    data.mask.I=zeros(sYscl,sXscl);
    data.mask.M=zeros(sYscl,sXscl);
    data.mask.MS=zeros(sYscl,sXscl);
    data.mask.IM=zeros(sYscl,sXscl);
    data.mask.T1=Z;
    data.mask.T2=Z;
    data.mask.T3=Z;
    data.mask.us=0;
    data.mask.vs=0;
    data.mask.ready=0;
    data.mask.done=0;
    data.mask.shift=0;
    data.mask.initMaskSet=0;
end

%check for tracking Data
%if(exist(fullfile(batchInfo.datapath,trackData),'file'))
%    %load mask data
%    data.track=load(fullfile(batchInfo.datapath,trackData));
%else
%    %init/reset mask data
%end