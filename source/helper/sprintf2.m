function [str,errmsg] = sprintf2(formatSpec,varargin)
%SPRINTF2 Fix a path string for use with sprintf (for Windows)

[str,errmsg] = sprintf(strrep(formatSpec,'\','\\'), varargin{:});

end
