% set path to source directory
addpath(fullfile('source','CIV'));
addpath(fullfile('source','helper'));
addpath(fullfile('source','tools'));

%set path to matpiv
%ADJUST NEXT LINE!
MatPIVPath='/PATH-TO/MatPIV1.6.1';
addpath(MatPIVPath);
addpath(fullfile(MatPIVPath,'src'));
addpath(fullfile(MatPIVPath,'filters'));
addpath(fullfile(MatPIVPath,'postprocessing'));

%set path to curvelets
%ADJUST NEXT LINE!
CurveLabPath='/PATH-TO/CurveLab-2.1.1-64';
addpath(fullfile(CurveLabPath,'fdct_wrapping_cpp','mex'));

%export fig
%ADJUST NEXT LINE!
Exportfig='/PATH-TO/toolbox_export_fig';
addpath(Exportfig);